var express = require('express')
var path = require('path')
var ejs = require('ejs');
var bodyParser = require('body-parser');

/*==========================*/
//zhiliang
var cookieParser = require('cookie-parser');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
/*==========================*/

var routes = require('./app/routes/revision.server.routes')


var app = express()
app.use(bodyParser.json())
app.use(bodyParser.urlencoded())

// app.use(session({
//     secret: settings.cookieSecret,
//     key: settings.db,//cookie name
//     cookie: {maxAge: 1000 * 60 * 60 * 24 * 30},//30 days
//     store: new MongoStore({
//         url: 'mongodb://localhost/blog'
//     })
// }));

// //处理404错误
// app.use(function(req, res, next) {
//     var err = new Error('Not Found');
//     err.status = 404;
//     next(err);
// });
// // 处理500错误
// app.use(function(err, req, res, next) {
//     res.status(err.status || 500);
//     res.render('error', {
//         message: err.message,
//         error: {}
//     });
// });
// //
app.set('views', path.join(__dirname,'app','./views'));
app.engine('html', ejs.__express);
app.set('view engine', 'html');

app.use(express.static(path.join(__dirname, 'public')));
app.use('/',routes)
app.use('/main',routes)
app.use('/data',routes)
app.engine('.html', require('ejs').__express);
app.set('view engine', 'html');

app.listen(8080, function () {
	  console.log('Revision app listening on port 8080!')
	})
module.exports = app;
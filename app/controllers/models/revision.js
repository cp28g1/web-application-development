var mongoose = require('./db')
var mongoConnectUrl = 'mongodb://localhost:27017';
var MongoClient = require('mongodb').MongoClient;


var revSchema = new mongoose.Schema(
    {   title: String,
        timestamp:String,
        user:String,
        anon:String},
    {
        versionKey: false
    });

var Candel_Stick_Schema = new mongoose.Schema(
    {   time: Number,
        low:Number,
        high:Number,
        open:Number,
        close:Number,
        volume:Number
    },
    {
        collection:'historyRate'
    });


var Revision = mongoose.model('Revision', Candel_Stick_Schema, 'historyRate')
module.exports = Revision;

module.exports.ChangeToTwoDArray = function (data) {
    for (var i = 0; i < data.length; i++){
        if (data[i]=="["){
            data = data.replace(data[i],",");
        }
        else if (data[i]=="]"){
            data = data.replace(data[i],",");
        }
    }
    data = data.replace(",,","");
    data=data.substring(0,data.length-1);
    data=data.substring(0,data.length-1);
    data=data.split(",,,");
    for (var m = 0; m < data.length; m++){
        data[m]=data[m].split(",");
        for (var n = 0; n < data[m].length; n++){
            if (data[m][n].indexOf('.')){
                data[m][n]=parseFloat(data[m][n]);
            }
            else{
                data[m][n]=parseInt(data[m][n])
            }
        }
    }
    // console.log(data);
    return data;
}

module.exports.StringToArray = function (data) {
    data = data.replace(data[0],",");
    data = data.replace(data[data.length-1],",");

    data=data.split(",");

    return data;
}

TimeOut = 0;

module.exports.TwoDArrayToJsonArray = function(data) {

    // console.log(data)
    // var firstDate = new Date();
    // firstDate.setDate(firstDate.getDate() - 2000);
    // firstDate.setHours(0, 0, 0, 0);

    var jsonArr = new Array();

    for (var j=0; j < data.length; j++) {
        var value = Math.round(Math.random() * ( 30 ) + 100);
        var jsonObj = {};

        // var newDate = new Date(firstDate);
        // newDate.setHours(0, TimeOut, 0, 0);
        // console.log(TimeOut, newDate)
        // var time = data[j][0];
        // time = time *1000;
        // time = new Date(time);
        // jsonObj["date"] = newDate;
        jsonObj["date"] = data[j][0];
        jsonObj["low"] = data[j][1];
        jsonObj["high"] = data[j][2];
        jsonObj["open"] = data[j][3];
        jsonObj["close"] = data[j][4];
        jsonObj["volume"] = data[j][5];
        jsonObj["value"] = value;
        jsonArr.push(jsonObj)
    }
    // console.log(jsonArr);

    TimeOut= TimeOut+1;
    return jsonArr;
};

module.exports.TwoDArrayToJsonArrayForBitfinex = function(data) {
    var jsonArr = new Array();
    for (var j=0; j < data.length; j++) {
        var value = Math.round(Math.random() * ( 30 ) + 100);
        var jsonObj = {};

        jsonObj["date"] = data[j][0];
        jsonObj["open"] = data[j][1];
        jsonObj["close"] = data[j][2];
        jsonObj["high"] = data[j][3];
        jsonObj["low"] = data[j][4];
        jsonObj["volume"] = data[j][5];
        jsonObj["value"] = value;
        jsonArr.push(jsonObj)
    }
    return jsonArr;
};

module.exports.JsonArrayTwoDArray = function(result) {
    var Arrlist = [];
    for (var i = 0; i < result.length; i++) {
        Arrlist[i]=[];
        //Arrlist[i][0]=result[i].time;
        Arrlist[i][0]=result[i].low;
        Arrlist[i][1]=result[i].high;
        Arrlist[i][2]=result[i].open;
        Arrlist[i][3]=result[i].close;
        Arrlist[i][4]=result[i].volume;
    }
    // console.log(Arrlist);
    return Arrlist;
};

module.exports.JsonArrayForDepth = function(result) {
    var depthresult = new Array();

    for (var i = 0; i < result.length; i++) {
        var jsonObj = {};
        jsonObj["date"] = Date((result[i].time)*1000);
        jsonObj["value"] = result[i].open;
        jsonObj["volume"] = result[i].volume;
        depthresult.push(jsonObj)
    }
    console.log(depthresult)
    return depthresult
}

module.exports.insertSingleData = function (mongoConnectUrl, coll, data, callback) {
    MongoClient.connect(mongoConnectUrl, function(err, client){
        if(err)
            return console.log(err);

        var db = client.db('Bitcoin');
        var collection = db.collection(coll);

        collection.insert(data, function(err, result){
            client.close();
            //callback(result);
        });
        console.dir("InsertSingleData OK!");
    });
};

module.exports.insertSingleData_GBP = function (mongoConnectUrl, coll, data, callback) {
    MongoClient.connect(mongoConnectUrl, function(err, client){
        if(err)
            return console.log(err);

        var db = client.db('Bitcoin');
        var collection = db.collection(coll);

        collection.insert(data, function(err, result){
            client.close();
            //callback(result);
        });
        console.dir("InsertSingleData OK!");
    });
};

module.exports.selectHistoryRate = function(time,callback){
     MongoClient.connect(mongoConnectUrl, function(err, client){
        if(err)
            return console.log(err);

        var db = client.db('Bitcoin');
        var collection = db.collection('historyRate');
        var timedata = {'time':{'$lte':time,'$gte':time-31536000}};

        collection.find(timedata).toArray( function(err, result){
            console.log(result[0]);
            callback(result);
            client.close();
        });
    });
};

//GDAX Candle data
module.exports.selectLessThenDate = function(time,callback){
    MongoClient.connect(mongoConnectUrl, function(err, client){
        if(err)
            return console.log(err);

        var db = client.db('Bitcoin');
        var collection = db.collection('dayData');
        // var timedata = {'date':};

        collection.find('date').toArray( function(err, result){
            // console.log(result);
            callback(result);
            client.close();
        });
    });
}
module.exports.selectLessThenDateV2 = function(time,callback){
    MongoClient.connect(mongoConnectUrl, function(err, client){
        if(err)
            return console.log(err);

        var db = client.db('Bitcoin');
        var collection = db.collection('dayData');
        var timedata = {'date':time};

        collection.find(timedata).toArray( function(err, result){
            // console.log(result);
            callback(result);
            client.close();
        });
    });
}
module.exports.selectLessThenDate_GBP = function(time,callback){
    MongoClient.connect(mongoConnectUrl, function(err, client){
        if(err)
            return console.log(err);

        var db = client.db('Bitcoin');
        var collection = db.collection('GDAX_Candle_BTC_GBP');
        // var timedata = {'date':};
        collection.find('date').toArray( function(err, result){
            // console.log(result);
            callback(result);
            client.close();
        });
    });
}
module.exports.selectLessThenDateV2_GBP = function(time,callback){
    MongoClient.connect(mongoConnectUrl, function(err, client){
        if(err)
            return console.log(err);

        var db = client.db('Bitcoin');
        var collection = db.collection('GDAX_Candle_BTC_GBP');
        var timedata = {'date':time};

        collection.find(timedata).toArray( function(err, result){
            // console.log(result);
            callback(result);
            client.close();
        });
    });
}

module.exports.selectGlobalData = function(time,callback){
    MongoClient.connect(mongoConnectUrl, function(err, client){
        if(err)
            return console.log(err);

        var db = client.db('Bitcoin');
        var collection = db.collection('dayData');
        var timedata = {'time':{'$gte':time}};

        collection.find(timedata).toArray( function(err, result){
            // console.log(result);
            callback(result);
            client.close();
        });
    });
}

module.exports.MapData = function(data){
    var jsonArr = new Array();
    var nodedata = data.nodes;
    var list;
    var la;
    var lo;
    var city;
    var countrycode;

    for(var key in nodedata){
        var jsonObj = {};
        list = nodedata[key];
        city = list[6];
        countrycode = list[7];
        la = list[8];
        lo = list[9];
        var title = city+"/"+countrycode;
        jsonObj["zoomLevel"] = 5;
        jsonObj["scale"] = 0.5;
        jsonObj["title"] = title;
        jsonObj["latitude"] = la;
        jsonObj["longitude"] = lo;
        jsonArr.push(jsonObj);
    }
    console.log(jsonArr)
    return jsonArr;

}

module.exports.GlobalBitcoinData = function(data,coin,currency){

    var jsonObj = {};
    coinw = (coin).toString();

    var Data = data.data;

    var OutsideId = Data[coinw];
    var coinname = OutsideId.name;
    var InsideQuotes = OutsideId.quotes;
    var currencyname = InsideQuotes[currency];
    var price = currencyname.price;
    var volume = currencyname.volume_24h;
    var change = currencyname.percent_change_24h;
    // price= price.toFixed(2);
    jsonObj["coinname"]=coinname;
    jsonObj["currencyname"]=currency;
    jsonObj["price"]=((price).toFixed(2)).toLocaleString('en-US');
    jsonObj["volume"]=(volume).toLocaleString('en-US');
    if(change<0){
        jsonObj["change"]=change+'%';
    }else {
        jsonObj["change"] = "+"+change + '%';
    }

    // console.log(jsonObj)
    return jsonObj
}

/*Global OTC ETH Data USD AUD EUR CNY -- LidaGUO*/
module.exports.GlobalEthereumData = function(data,coin,currency){

    var jsonObj = {};
    coinw = (coin).toString();

    var Data = data.data;

    var OutsideId = Data[coinw];
    var coinname = OutsideId.name;
    var InsideQuotes = OutsideId.quotes;
    var currencyname = InsideQuotes[currency];
    var price = currencyname.price;
    var volume = currencyname.volume_24h;
    var change = currencyname.percent_change_24h;
    // price= price.toFixed(2);
    jsonObj["coinname"]=coinname;
    jsonObj["currencyname"]=currency;
    jsonObj["price"]=((price).toFixed(2)).toLocaleString('en-US');
    jsonObj["volume"]=(volume).toLocaleString('en-US');
    if(change<0){
        jsonObj["change"]=change+'%';
    }else {
        jsonObj["change"] = "+"+change + '%';
    }

    // console.log(jsonObj)
    return jsonObj
}
/*Global OTC LTC Data USD AUD EUR CNY -- LidaGUO*/
module.exports.GlobalLitecoinData = function(data,coin,currency){

    var jsonObj = {};
    coinw = (coin).toString();

    var Data = data.data;

    var OutsideId = Data[coinw];
    var coinname = OutsideId.name;
    var InsideQuotes = OutsideId.quotes;
    var currencyname = InsideQuotes[currency];
    var price = currencyname.price;
    var volume = currencyname.volume_24h;
    var change = currencyname.percent_change_24h;
    // price= price.toFixed(2);
    jsonObj["coinname"]=coinname;
    jsonObj["currencyname"]=currency;
    jsonObj["price"]=((price).toFixed(2)).toLocaleString('en-US');
    jsonObj["volume"]=(volume).toLocaleString('en-US');
    if(change<0){
        jsonObj["change"]=change+'%';
    }else {
        jsonObj["change"] = "+"+change + '%';
    }

    // console.log(jsonObj)
    return jsonObj
}
/*Global OTC EOS Data USD AUD EUR CNY -- LidaGUO*/
module.exports.GlobalEOSData = function(data,coin,currency){

    var jsonObj = {};
    coinw = (coin).toString();

    var Data = data.data;

    var OutsideId = Data[coinw];
    var coinname = OutsideId.name;
    var InsideQuotes = OutsideId.quotes;
    var currencyname = InsideQuotes[currency];
    var price = currencyname.price;
    var volume = currencyname.volume_24h;
    var change = currencyname.percent_change_24h;
    // price= price.toFixed(2);
    jsonObj["coinname"]=coinname;
    jsonObj["currencyname"]=currency;
    jsonObj["price"]=((price).toFixed(2)).toLocaleString('en-US');
    jsonObj["volume"]=(volume).toLocaleString('en-US');
    if(change<0){
        jsonObj["change"]=change+'%';
    }else {
        jsonObj["change"] = "+"+change + '%';
    }

    // console.log(jsonObj)
    return jsonObj
}



// var Candel_Stick = mongoose.model('Candel_Stick', Candel_Stick_Schema,'revisions');
// var Revision = mongoose.model('Revision', revSchema, 'revisions')
// module.exports = Candel_Stick;
// module.exports = Revision;

module.exports.selectUserData = function(data,callback){
    var username = data.username;
    var password = data.password;
    // console.log(username,password,"aaaa")

    MongoClient.connect(mongoConnectUrl, function(err, client){
        if(err)
            return console.log(err);

        var db = client.db('Bitcoin');
        var collection = db.collection('User');
        var name = {'username':username};

        collection.find(name).toArray( function(err, result){
            // console.log(result,"bbbb")
            if(result[0].password == password){
                console.log(result[0].password)
                result={result:'Success'}
                callback(result);

            }else{
                console.log(result[0].password)
                result={result:'Failure'}
                callback(result);
            }
            // console.log(result);
            client.close();
        });
    });
}

//depth chart revision for Bitfinex
//Order book Depth chart 的数据返回格式

// var aaaa=
//  {"bids":
//  [{"price":"7481.3","amount":"1.71801488","timestamp":"1528155244.0"},
//  {"price":"7481","amount":"0.8","timestamp":"1528155244.0"},
//  {"price":"7480.2","amount":"0.00307","timestamp":"1528155244.0"}
//  ],
//  "asks":
//  [{"price":"7481.4","amount":"5.04790866","timestamp":"1528155244.0"},
//  {"price":"7481.5","amount":"0.05025649","timestamp":"1528155244.0"},
//  {"price":"7482.9","amount":"1.49539886","timestamp":"1528155244.0"}
//  ]
//  }

// DepthForBitfinex(aaaa);
// function DepthForBitfinex(result){
module.exports.DepthForBitfinex = function(result) {

    result = JSON.parse(result)

    var bids = result.bids;
    var asks = result.asks;

    for (var i = 0; i < bids.length; i++) {
        var bprice = bids[i].price;
        var bvolume = bids[i].amount;
        result.bids[i]=[bprice,bvolume]

        var aprice = asks[i].price;
        var avolume = asks[i].amount;
        result.asks[i]=[aprice,avolume]
    }

    return result;
};
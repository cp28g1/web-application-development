/**
 * Created by MCIAKTE on 2018/3/25.
 */

Revision = require("./revision");
var mongoConnectUrl = 'mongodb://localhost:27017';

/*Lida APIs and Zhaohui real time data section*/

/*GDAX Bitcoin Website*/
module.exports.Product_HistoryRrate=function(start,end,req,res) {
    console.dir(start+end)

    const request = require('request')
    request({
        qs: {
            start: start,
            end: end,
            granularity: 60
        },
        method: 'GET',
        uri: 'https://api.gdax.com/products/BTC-USD/candles',
        headers: {
            'User-Agent': 'gdax-node-client',
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    }, function(a,b,data){
            var twoarray = Revision.ChangeToTwoDArray(data);
            var jsonarray = Revision.TwoDArrayToJsonArray(twoarray);
            var revisions = 'dayData';
            const callback = (result)=>{
                // console.log(result)
            }
            Revision.insertSingleData(mongoConnectUrl,revisions,jsonarray,callback);
        console.log('Granules from direct request', JSON.parse(data).length)
    })
};
module.exports.CatchRealtimeData=function(start,end,req,res) {
    console.dir(start+end)
    const request = require('request')
    request({
        qs: {
            start: start,
            end: end,
            granularity: 60
        },
        method: 'GET',
        uri: 'https://api.gdax.com/products/BTC-USD/candles',
        headers: {
            'User-Agent': 'gdax-node-client',
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    }, function(a,b,data){
        var twoarray = Revision.ChangeToTwoDArray(data);
        var jsonarray = Revision.TwoDArrayToJsonArray(twoarray);
        var revisions = 'dayData';
        const callback = (result)=>{
            // console.log(result)
        }
        number = JSON.parse(data).length;
        console.log('Granules from direct request', number)
        if(number==1) {
            Revision.insertSingleData(mongoConnectUrl, revisions, jsonarray, callback);
            state = 'success'
        }
        else {
            state = 'failure'
        }
    })
};
module.exports.Product_by_24H=function(req,res) {
    console.dir("2");
    const GDAX = require('gdax');
    const publicClient = new GDAX.PublicClient();
    const callback = (error, response, data) =>{
        if (error)
            return console.dir(error);

        var revisions = 'revisions';
        Revision.insertSingleData(mongoConnectUrl,revisions,data,callback);

        //res.json(data);
        console.dir("Product_by_24H OK!");
        return console.dir(data);
    };
    // publicClient.getProduct24HrStats('BTC-USD', callback);
    // publicClient.getProductHistoricRates('BTC-USD', callback);
    // publicClient.getProductTicker('BTC-USD', callback);
    publicClient.getProductHistoricRates(
        'BTC-USD',
        { granularity: 3600 },
        callback
    );
};
module.exports.Product_Order_BidAsk=function(callback,req,res) {
    const GDAX = require('gdax');
    const publicClient = new GDAX.PublicClient();
    // const callback = (error, response, data) =>{
    //     if (error)
    //         return console.dir(error);
    //     console.dir(data);
    //
    //     return data
    // };
    // It can be called many time in a loop.
    publicClient.getProductOrderBook('BTC-USD', callback);
    /*
     { sequence: 5475970332,
     bids: [ [ '8412.14', '6.81585015', 7 ] ],
     asks: [ [ '8412.15', '11.11790437', 6 ] ] }

     sequence: 5534297595,
     bids: [ [ '7551.09', '0.02859647', 3 ] ],
     asks: [ [ '7551.1', '25.10596852', 19 ] ]

     sequence: 5534316231,
     bids: [ [ '7537.05', '33.65451876', 11 ] ],
     asks: [ [ '7537.06', '6.45867502', 11 ] ]

     sequence: 5534332784,
     bids: [ [ '7544.04', '0.080136', 3 ] ],
     asks: [ [ '7544.05', '11.48259854', 19 ] ]

     sequence: 5534352144,
     bids: [ [ '7505', '2.07803914', 5 ] ],
     asks: [ [ '7505.01', '20.17786675', 21 ] ]
     */
};

/*Bitstamp Bitcoin Website*/
module.exports.Bitstamp_Trade_History=function(req,res){
    var Bitstamp = require('bitstamp');
    var bitstamp = new Bitstamp();
    bitstamp.transactions('btcusd', function(err, trades) {
        console.log(trades);
        /*
        {date: '1524586293',
         tid: '63469296',
         price: '9347.91',
         type: '1',
         amount: '0.02900000' },
        {date: '1524586284',
         tid: '63469289',
         price: '9353.71',
         type: '0',
         amount: '0.01452957' }
        */
    // });
    // const run = async () => {
    //     const ticker = await bitstamp.ticker(CURRENCY.ETH_BTC).then(({status, headers, body}) => console.log(body));
    //     await bitstamp.tickerHour(CURRENCY.ETH_BTC);
    //     await bitstamp.orderBook(CURRENCY.ETH_BTC);
    //     await bitstamp.transactions(CURRENCY.ETH_BTC, "hour");
    //     await bitstamp.conversionRate();
    // };
    // run().then(() => {
    //     console.log(bitstamp.getStats());
    //     bitstamp.close();
    // });
    });
};
module.exports.Bitstamp_Trade_wsTest=function(req,res){
    var Bitstamp = require('bitstamp-ws');

    var ws = new Bitstamp();
    ws.on('trade', function(trade) {
        console.log('new trade:', trade);
    });
    ws.on('data', function(data) {
        console.log('new order book event:', data);
    });
};
module.exports.Bitstamp_Currency_Price=function(req,res){
    "use strict";
    const {TickerStream, OrderBookStream, Bitstamp, CURRENCY} = require("node-bitstamp");
    console.log(CURRENCY);
    const tickerStream = new TickerStream();
    const tickerTopic = tickerStream.subscribe(CURRENCY.BTC_USD);
    tickerStream.on("connected", () => console.log("ticker stream connected."));
    tickerStream.on("disconnected", () => console.log("ticker stream disconnected."));
    tickerStream.on(tickerTopic, data => {
        console.log(data);
        /* e.g.
         { amount: 0.02296034,
         buy_order_id: 1397421499,
         sell_order_id: 1397763940,
         amount_str: '0.02296034',
         price_str: '506.09',
         timestamp: '1524735353',
         price: 506.09,
         type: 1,
         id: 63846872,
         cost: 11.619998470599999,
         currency: 'etheur' }
         */
    });
    const orderBookStream = new OrderBookStream();
    const orderBookTopic = orderBookStream.subscribe(CURRENCY.BTC_USD);
    orderBookStream.on("connected", () => console.log("order book stream connected."));
    orderBookStream.on("disconnected", () => console.log("order book stream disconnected."));
    orderBookStream.on(orderBookTopic, data => {
        console.log(data);
        /*
         { bids:
         [  [ '3284.06000000', '0.16927410' ],
            [ '3284.05000000', '1.00000000' ],
            [ '3284.02000000', '0.72755647' ],],
         asks:
         [  [ '3289.00000000', '3.16123001' ],
            [ '3291.99000000', '0.22000000' ],
            [ '3292.00000000', '49.94312963' ],
         ] }
         */
    });
    setTimeout(() => {
        console.log("closing after 10 seconds.");
        tickerStream.close();
        orderBookStream.close();
    }, 10000);
};
    /*Bitstamp URL Request Method*/


/*BitFinex Bitcoin Website*/
module.exports.BitFinex_Platform_Status=function (req, res) {
    const WebSocket = require('ws');
    const wss = new WebSocket('wss://api.bitfinex.com/ws/');
    wss.onmessage = (msg) => console.log(msg.data);
    wss.onopen = () => {
        // API keys setup here (See "Authenticated Channels")
    }
    /*
     {"event":"info","version":1.1,"platform":{"status":1}}
     */
};
    /*BitFinex_Candle_Data request method from url*/
module.exports.BitFinex_Candle_Data_Version1=function (req, res) {
    var url = "https://api.bitfinex.com/v2/"; //basic url in bitfinex api
    /*拿到最新last的数据Candle*/
    var request = require("request");
    request.get(
        `${url}/candles/trade:1m:tBTCUSD/last`,
    (error, response, body) => console.log(body)
    );
    /*仅仅拿到最新的一条数据 last
     [MTS,         [1526362740000,
     OPEN,          8719.1,
     CLOSE,         8716.8,
     HIGH,          8719.1,
     LOW,           8710.3,
     VOLUME]        26.19711688]
     */
    /*拿到全部hist的数据Candle*/
    request.get(
        `${url}/candles/trade:1D:tBTCUSD/hist`,
    (error, response, body) => console.log(body)
    );
    /*拿到全部的数据 hist
     [
     [1526342400000,8670.8,8721.8,8748.9,8615,3882.44864537],
     [1526256000000,8683.5,8670.8,8884.5,8271,37154.66777466]
     ]
     */
};
module.exports.BitFinex_Current_BTCUSD_Version2=function (req, res) {

    var url = "https://api.bitfinex.com/v2/"; //basic url in bitfinex api

    /*拿到最新last的数据Candle*/
    var request = require("request");
    request.get(
        `${url}/candles/trade:1m:tBTCUSD/last`,
        (error, response, body) => console.log(body)
    );
    /*仅仅拿到最新的一条数据 last
     [MTS,         [1526362740000,
     OPEN,          8719.1,
     CLOSE,         8716.8,
     HIGH,          8719.1,
     LOW,           8710.3,
     VOLUME]        26.19711688]
     */
};
    // var request = require("request");
    //
    // var options = { method: 'POST',
    //     url: 'https://api.bitfinex.com/v2/calc/trade/avg',
    //     qs: { symbol: 'tBTCUSD' } };
    //
    // request(options, function (error, response, body) {
    //     if (error) throw new Error(error);
    //
    //     console.log(body);
    // });


/*Binance Bitcoin Website*/
module.exports.Binance_Current_PriceUSDT=function (req, res) {
    const binance = require('node-binance-api');
    //API key and secret are important in Private data like trading history
    binance.options({
        APIKEY: 'x3M2cpEmAzQTroazxtueXn2ScP7zgmUPgxL2Ee9Yn99Btbi50QmGc6SECD2sGmd4',
        APISECRET: 'IUXWrJE39AWZf1dUyplSmFQIbjUUDO6E2YAU60kAXg3099yFiFaqZPFTeWaY2WFS',
        useServerTime: true, // If you get timestamp errors, synchronize to server time at startup
        test: true // If you want to use sandbox mode where orders are simulated
    });
    binance.prices((error, ticker) => {
        console.log("Prices with ()", ticker);
        console.log("Price of BTC: ", ticker.BTCUSDT);
    });
    /*
   { ETHBTC: '0.06755700',
     LTCBTC: '0.01591700',
     BNBBTC: '0.00148830',
     NEOBTC: '0.00790200',
    */
};
module.exports.Binance_Candle_BNB=function (req, res) {
    const binance = require('node-binance-api');
    binance.candlesticks("BNBBTC", "5m", (error, ticks, symbol) => {
        // Intervals: 1m,3m,5m,15m,30m,1h,2h,4h,6h,8h,12h,1d,3d,1w,1M
        console.log("candlesticks()", ticks);
        let last_tick = ticks[ticks.length - 1];
        let [time, open, high, low, close, volume, closeTime, assetVolume, trades, buyBaseVolume, buyAssetVolume, ignored] = last_tick;
        console.log(symbol+" last close: "+close);
    }, {limit: 500, endTime: 1514764800000});
    /*
     [ [ 1514615100000,
     '0.00060101',
     '0.00060261',
     '0.00060100',
     '0.00060108',
     '9919.00000000',
     1514615399999,
     '5.96749997',
     251,
     '3744.00000000',
     '2.25316015',
     '0' ],
     */
};
module.exports.Binance_Candle_wsBNB=function (req, res) {
    const binance = require('node-binance-api');
    binance.websockets.chart("BNBBTC", "1m", (symbol, interval, chart) => {
        let tick = binance.last(chart);
        const last = chart[tick].close;
        // console.log(chart);
        // console.log(symbol+" last price: "+last)
        /*
         {'1524652140000':
         {  open: '0.00150500',
         high: '0.00150880',
         low: '0.00150300',
         close: '0.00150530',
         volume: '3069.79000000' },
         */
        // Optionally convert 'chart' object to array:
        // let ohlc = binance.ohlc(chart);
        // console.log(symbol, ohlc);
        /*
        {open:
            [ 0.0015067,
             0.0015075,
             0.001507,
             0.0015058,
         high:
             [ 0.0015116,
             0.0015122,
             0.0015079,
         low:
             [ 0.0015045,
             0.001507,
             0.0015,
         close:
             [ 0.0015075,
             0.001508,
             0.0015058,
         volume:
             [ 2128,
             1216.26,
             4122.13,
        }
        */
    });
};
module.exports.Binance_Candle_wsBNB_realTime=function (req, res) {
    const binance = require('node-binance-api');
    // Periods: 1m,3m,5m,15m,30m,1h,2h,4h,6h,8h,12h,1d,3d,1w,1M
    binance.websockets.candlesticks(['BNBBTC'], "1m", (candlesticks) => {
        let { e:eventType, E:eventTime, s:symbol, k:ticks } = candlesticks;
        let { o:open, h:high, l:low, c:close, v:volume, n:trades, i:interval, x:isFinal, q:quoteVolume, V:buyVolume, Q:quoteBuyVolume } = ticks;
        console.log(symbol+" "+interval+" candlestick update");
        console.log("open: "+open);
        console.log("high: "+high);
        console.log("low: "+low);
        console.log("close: "+close);
        console.log("volume: "+volume);
        console.log("isFinal: "+isFinal);
    });
    /*
     BNBBTC 1m candlestick update
     open: 0.00155850
     high: 0.00156010
     low: 0.00155800
     close: 0.00155920
     volume: 2003.07000000
     isFinal: false
     BNBBTC 1m candlestick update
     open: 0.00155850
     high: 0.00156010
     low: 0.00155800
     close: 0.00155950
     volume: 2015.56000000
     isFinal: false
    */
};
module.exports.Binance_24hr_BNBchange=function (req, res) {
//After decision, we can choose four cryptocurrencies as symbols also as exchange pairs
    const binance = require('node-binance-api');
    binance.prevDay("BNBBTC", (error, prevDay, symbol) => {
        console.log(symbol+" previous day:", prevDay);
        console.log("BNB change since yesterday: "+prevDay.priceChangePercent+"%")
    });
    /*
     BNBBTC previous day: { symbol: 'BNBBTC',
     priceChange: '-0.00004180',
     priceChangePercent: '-2.681',
     weightedAvgPrice: '0.00153429',
     prevClosePrice: '0.00155910',
     lastPrice: '0.00151720',
     lastQty: '41.00000000',
     bidPrice: '0.00151700',
     bidQty: '324.21000000',
     askPrice: '0.00151860',
     askQty: '40.34000000',
     openPrice: '0.00155900',
     highPrice: '0.00163800',
     lowPrice: '0.00146750',
     volume: '2497653.54000000',
     quoteVolume: '3832.12782547',
     openTime: 1524620056738,
     closeTime: 1524706456738,
     firstId: 16379034,
     lastId: 16537128,
     count: 158095 }
     BNB change since yesterday: -2.681%
     */
};
module.exports.Binance_24hr_wsBNBchange=function (req, res) {
    const binance = require('node-binance-api');
    binance.websockets.prevDay('BNBBTC', (error, response) => {
        console.log(response);
    });
};
module.exports.Binance_Depth_wsBNB=function (req, res) {
    const binance = require('node-binance-api');
    binance.websockets.depthCache(['BNBBTC'], (symbol, depth) => {
        let bids = binance.sortBids(depth.bids);
        let asks = binance.sortAsks(depth.asks);
        console.log(symbol+" depth cache update");
        console.log("bids", bids);
        console.log("asks", asks);
        /*
         BNBBTC depth cache update
         bids { '0.00156350': 0.03,
         '0.00156340': 162.94,
         '0.00156330': 19.77,
         asks { '0.00156490': 99.48,
         '0.00156500': 167.76,
         '0.00156510': 1.62,
         BNBBTC depth cache update
         bids { '0.00156350': 0.03,
         '0.00156340': 162.94,
         '0.00156330': 4.77,
         asks { '0.00156490': 98.69,
         '0.00156500': 167.76,
         '0.00156510': 1.62,
         BNBBTC depth cache update
         bids { '0.00156350': 134.58,
         '0.00156340': 162.94,
         '0.00156330': 4.77,
         asks { '0.00156490': 98.69,
         '0.00156500': 167.76,
         '0.00156510': 1.62,
         */
        // console.log("best bid: "+binance.first(bids));
        // console.log("best ask: "+binance.first(asks));
    });
};

/*Kraken Bitcoin Website*/

/*CCXT async Bitcoin Website overview*/
const ccxt = require ('ccxt');
(async function () {
    /*Kraken*/
    let kraken    = new ccxt.kraken ();
    // console.log (kraken.id, await kraken.loadMarkets ())
    /*
    没什么用处*/
    // console.log (kraken.id, await kraken.fetchTicker ('BTC/USD'));
    /* 获取BTC/USD的所有信息
   { symbol: 'BTC/USD',
     timestamp: 1526289169720,
     datetime: '2018-05-14T09:12:49.720Z',
     high: 8749.6,
     low: 8286.3,
     bid: 8432.3,
     bidVolume: undefined,
     ask: 8437,
     askVolume: undefined,
     vwap: 8557.3419,
     open: 8698.2,
     close: 8440,
     last: 8440,
     previousClose: undefined,
     change: undefined,
     percentage: undefined,
     average: undefined,
     baseVolume: 4027.7047795,
     quoteVolume: 34466446.87044561,
     info:
     { a: [ '8437.00000', '1', '1.000' ],
     b: [ '8432.30000', '1', '1.000' ],
     c: [ '8440.00000', '0.90000000' ],
     v: [ '1490.18124232', '4027.70477950' ],
     p: [ '8499.62087', '8557.34190' ],
     t: [ 4914, 14234 ],
     l: [ '8286.30000', '8286.30000' ],
     h: [ '8706.90000', '8749.60000' ],
     o: '8698.20000' } }
     */
    // console.log (kraken.id, await kraken.fetchOrderBook (kraken.symbols[0]));
    /* 显示100个数据 并且必须跟fetchTicker合作使用 four 则symbols[0]找不到
     { bids:
     [ [ 0.16553, 1.651 ],
     [ 0.16551, 0.715 ],],
     asks:
     [ [ 0.16556, 0.023 ],
     [ 0.16568, 0.003 ], ],
     timestamp: undefined,
     datetime: undefined,
     nonce: undefined }
     * */
    // console.log (kraken.id, await kraken.fetchTrades ('BTC/USD'));
    /* TradeHistory 目前不准备做该图 该表
     [ { id: undefined,
     order: undefined,
     info: [ '8381.50000', '0.07799441', 1526280332.306, 'b', 'l', '' ],
     这个info其实就是所有信息的集合到一个数组里面了 分别是 price amount time side type fee
     timestamp: 1526280332306,
     datetime: '2018-05-14T06:45:32.306Z',
     symbol: 'BTC/USD',
     type: 'limit',
     side: 'buy',
     price: 8381.5,
     amount: 0.07799441,
     cost: 653.710147415,
     fee: undefined } ]
     */

    /*Bitstamp*/
    let bitstamp = new ccxt.bitstamp();
    // console.log(bitstamp.id, await bitstamp.fetchTicker('BTC/USD'));
    /* 可以做candle stick chart的实时更新 但是没有历史数据
     bitstamp { symbol: 'BTC/USD',
     timestamp: 1526352903000,
     datetime: '2018-05-15T02:55:03.000Z',
     high: 8900.9,
     low: 8286.57,
     bid: 8658.13,
     bidVolume: undefined,
     ask: 8664.68,
     askVolume: undefined,
     vwap: 8598.24,
     open: 8674.53,
     close: 8665.75,
     last: 8665.75,
     previousClose: undefined,
     change: undefined,
     percentage: undefined,
     average: undefined,
     baseVolume: 18678.8817228,
     quoteVolume: 160605507.98424786,
     info:
     { high: '8900.90',
     last: '8665.75',
     timestamp: '1526352903',
     bid: '8658.13',
     vwap: '8598.24',
     volume: '18678.88172280',
     low: '8286.57',
     ask: '8664.68',
     open: '8674.53'}
     }
     * */
    // console.log(bitstamp.id, await bitstamp.fetchOrderBook (bitstamp.symbols[0]));
    /* 必须跟fetchTicker合作使用
     {
     bids:
     [
        [ 0.16134011, 2.0756335 ],
        [ 0.1613401, 1.24711927 ]
     ],
     asks:
     [
        [ 0.16187981, 0.01 ],
        [ 0.16187983, 0.01 ]
     ],
     timestamp: 1526353720000,
     datetime: '2018-05-15T03:08:40.000Z',
     nonce: undefined
     }
    */
    // console.log(bitstamp.id, await bitstamp.fetchTrades('BTC/USD'));

    // let bitfinex  = new ccxt.bitfinex ({ verbose: true })
    // let huobi     = new ccxt.huobi ()
    // let okcoinusd = new ccxt.okcoinusd ()

    // console.log (bitfinex.id, await bitfinex.loadMarkets  ())
    // console.log (huobi.id, await huobi.loadMarkets ())
}) ();
    /*CCXT Bitcoin Website*/
module.exports.CCXT_Kraken_TickerBTCUSD=function (req, res) {
    let kraken    = new ccxt.kraken ();
    console.log (kraken.id, kraken.fetchMarkets());
    /*
     { ETHBTC: '0.06755700',
     LTCBTC: '0.01591700',
     BNBBTC: '0.00148830',
     NEOBTC: '0.00790200',
     */
};

/*Bitnodes WorldMap overview*/
module.exports.Bitnodes_World_Map_Data=function (req, res) {
    var url = "https://bitnodes.earn.com/api/v1/snapshots/latest/";
    getDataFromUrl(url, function(err, result)  {
        if (err) {
            console.log("error");
        } else {
            console.log(result) // Print the json response
        }
    });
    /* Method to get data from URL (from Web tutorial)
     var url = "https://bitnodes.earn.com/api/v1/snapshots/latest/";
     request({
     url: url,
     json: true
     }, function (error, response, body) {
     if (!error && response.statusCode === 200) {
     console.log(body); // Print the json response
     }
     });
    */
    /* WorldMap has become
     '52.44.250.161:8333':
     [ 70015,
     '/Bitcoin ABC:0.15.1(EB8.0)/',
     1521241436,
     33,
     572927,
     'ec2-52-44-250-161.compute-1.amazonaws.com',
     'Ashburn',
     'US',
     39.0853,
     -77.6452,
     'America/New_York',
     'AS14618',
     'Amazon.com, Inc.' ],
     */
};

//--------------Global OTC Data-------------
/*CoinMarketCap Global OTC Data*/
module.exports.CoinMC_Global_OTC_USD=function (callback) {
    var USDurl = "https://api.coinmarketcap.com/v2/ticker/?convert=USD&limit=10";
    getDataFromUrl(USDurl, function(err, body)  {
        if (err) {
            console.log("error");
        } else {
            // console.log(body)
            callback(body);
        }
    });
    /* Global OTC
     { coinname: 'Bitcoin',
     currencyname: 'USD',
     price: '8742.59',
     volume: '7,214,730,000',
     change: '+2.33%' }
     */
};
module.exports.CoinMC_Global_OTC_EUR=function (callback) {
    var EURurl = "https://api.coinmarketcap.com/v2/ticker/?convert=EUR&limit=10";
    getDataFromUrl(EURurl, function(err, body)  {
        if (err) {
            console.log("error");
        } else {
            callback(body);
            // console.log(body)
        }
    });
};
module.exports.CoinMC_Global_OTC_CNY=function (callback) {
    var CNYurl = "https://api.coinmarketcap.com/v2/ticker/?convert=CNY&limit=10";
    getDataFromUrl(CNYurl, function(err, body)  {
        if (err) {
            console.log("error");
        } else {
            callback(body);
            // console.log(body)
        }
    });
};
module.exports.CoinMC_Global_OTC_AUD=function (callback) {
    var AUDurl = "https://api.coinmarketcap.com/v2/ticker/?convert=AUD&limit=10";
    getDataFromUrl(AUDurl, function(err, body)  {
        if (err) {
            console.log("error");
        } else {
            callback(body);
            // console.log(body)
        }
    });
};
module.exports.CoinMC_Global_OTC_GBP=function (callback) {
    var GBPurl = "https://api.coinmarketcap.com/v2/ticker/?convert=GBP&limit=10";
    getDataFromUrl(GBPurl, function(err, body)  {
        if (err) {
            console.log("error");
        } else {
            callback(body);
            // console.log(body)
        }
    });
};
/*CoinMarketCap Global OTC Ethereum Data*/
module.exports.CoinMC_Global_OTC_ETHUSD=function (callback) {
    var USDurl = "https://api.coinmarketcap.com/v2/ticker/?convert=USD&limit=10";
    getDataFromUrl(USDurl, function(err, body)  {
        if (err) {
            console.log("error");
        } else {
            // console.log(body)
            callback(body);
        }
    });
    /* Global OTC
     { coinname: 'Bitcoin',
     currencyname: 'USD',
     price: '8742.59',
     volume: '7,214,730,000',
     change: '+2.33%' }
     */
};
module.exports.CoinMC_Global_OTC_ETHEUR=function (callback) {
    var USDurl = "https://api.coinmarketcap.com/v2/ticker/?convert=EUR&limit=10";
    getDataFromUrl(USDurl, function(err, body)  {
        if (err) {
            console.log("error");
        } else {
            // console.log(body)
            callback(body);
        }
    });
};
module.exports.CoinMC_Global_OTC_ETHCNY=function (callback) {
    var USDurl = "https://api.coinmarketcap.com/v2/ticker/?convert=CNY&limit=10";
    getDataFromUrl(USDurl, function(err, body)  {
        if (err) {
            console.log("error");
        } else {
            // console.log(body)
            callback(body);
        }
    });
};
module.exports.CoinMC_Global_OTC_ETHAUD=function (callback) {
    var GBPurl = "https://api.coinmarketcap.com/v2/ticker/?convert=AUD&limit=10";
    getDataFromUrl(GBPurl, function(err, body)  {
        if (err) {
            console.log("error");
        } else {
            // console.log(body)
            callback(body);
        }
    });
};
module.exports.CoinMC_Global_OTC_ETHGBP=function (callback) {
    var GBPurl = "https://api.coinmarketcap.com/v2/ticker/?convert=GBP&limit=10";
    getDataFromUrl(GBPurl, function(err, body)  {
        if (err) {
            console.log("error");
        } else {
            // console.log(body)
            callback(body);
        }
    });
};
/*CoinMarketCap Global OTC Litecoin Data*/
module.exports.CoinMC_Global_OTC_LTCUSD=function (callback) {
    var USDurl = "https://api.coinmarketcap.com/v2/ticker/?convert=USD&limit=10";
    getDataFromUrl(USDurl, function(err, body)  {
        if (err) {
            console.log("error");
        } else {
            // console.log(body)
            callback(body);
        }
    });
    /* Global OTC
     { coinname: 'Bitcoin',
     currencyname: 'USD',
     price: '8742.59',
     volume: '7,214,730,000',
     change: '+2.33%' }
     */
};
module.exports.CoinMC_Global_OTC_LTCEUR=function (callback) {
    var USDurl = "https://api.coinmarketcap.com/v2/ticker/?convert=EUR&limit=10";
    getDataFromUrl(USDurl, function(err, body)  {
        if (err) {
            console.log("error");
        } else {
            // console.log(body)
            callback(body);
        }
    });
};
module.exports.CoinMC_Global_OTC_LTCCNY=function (callback) {
    var USDurl = "https://api.coinmarketcap.com/v2/ticker/?convert=CNY&limit=10";
    getDataFromUrl(USDurl, function(err, body)  {
        if (err) {
            console.log("error");
        } else {
            // console.log(body)
            callback(body);
        }
    });
};
module.exports.CoinMC_Global_OTC_LTCAUD=function (callback) {
    var USDurl = "https://api.coinmarketcap.com/v2/ticker/?convert=AUD&limit=10";
    getDataFromUrl(USDurl, function(err, body)  {
        if (err) {
            console.log("error");
        } else {
            // console.log(body)
            callback(body);
        }
    });
};
module.exports.CoinMC_Global_OTC_LTCGBP=function (callback) {
    var GBPurl = "https://api.coinmarketcap.com/v2/ticker/?convert=GBP&limit=10";
    getDataFromUrl(GBPurl, function(err, body)  {
        if (err) {
            console.log("error");
        } else {
            // console.log(body)
            callback(body);
        }
    });
};
/*CoinMarketCap Global OTC EOS Data*/
module.exports.CoinMC_Global_OTC_EOSUSD=function (callback) {
    var USDurl = "https://api.coinmarketcap.com/v2/ticker/?convert=USD&limit=10";
    getDataFromUrl(USDurl, function(err, body)  {
        if (err) {
            console.log("error");
        } else {
            // console.log(body)
            callback(body);
        }
    });
    /* Global OTC
     { coinname: 'Bitcoin',
     currencyname: 'USD',
     price: '8742.59',
     volume: '7,214,730,000',
     change: '+2.33%' }
     */
};
module.exports.CoinMC_Global_OTC_EOSEUR=function (callback) {
    var USDurl = "https://api.coinmarketcap.com/v2/ticker/?convert=EUR&limit=10";
    getDataFromUrl(USDurl, function(err, body)  {
        if (err) {
            console.log("error");
        } else {
            // console.log(body)
            callback(body);
        }
    });
};
module.exports.CoinMC_Global_OTC_EOSCNY=function (callback) {
    var USDurl = "https://api.coinmarketcap.com/v2/ticker/?convert=CNY&limit=10";
    getDataFromUrl(USDurl, function(err, body)  {
        if (err) {
            console.log("error");
        } else {
            // console.log(body)
            callback(body);
        }
    });
};
module.exports.CoinMC_Global_OTC_EOSAUD=function (callback) {
    var USDurl = "https://api.coinmarketcap.com/v2/ticker/?convert=AUD&limit=10";
    getDataFromUrl(USDurl, function(err, body)  {
        if (err) {
            console.log("error");
        } else {
            // console.log(body)
            callback(body);
        }
    });
};
module.exports.CoinMC_Global_OTC_EOSGBP=function (callback) {
    var GBPurl = "https://api.coinmarketcap.com/v2/ticker/?convert=GBP&limit=10";
    getDataFromUrl(GBPurl, function(err, body)  {
        if (err) {
            console.log("error");
        } else {
            // console.log(body)
            callback(body);
        }
    });
};

//--------------Candle Stick BTC/USD Data-----------
/*BitFinex BTC/USD Candle Stick*/
module.exports.BitFinex_Candle_Data=function (callback) {
    var url = "https://api.bitfinex.com/v2/candles/trade:1h:tBTCUSD/hist?limit=1000&start="; //basic url in bitfinex api
    /*拿到全部hist的数据Candle*/
    var request = require("request");

    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();

    start = year + '-' + month + '-' + day + ' ' + hour + ':00:00';
    start = new Date(start)
    start = Date.parse(start)-3600000000;
    // console.log(start)
    // console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!")
    request.get(
        `${url}`+start+`&sort=1`,
        (error, response, body) => callback(body)
    )
    /*拿到全部的数据 hist
     [  e.g. MTS,   OPEN,  CLOSE, HIGH,  LOW, VOLUME
     [1526342400000,8670.8,8721.8,8748.9,8615,3882.44864537],
     [1526256000000,8683.5,8670.8,8884.5,8271,37154.66777466]
     ]
     */
};
module.exports.BitFinex_Candle_Data_GBP=function (callback) {
    var url = "https://api.bitfinex.com/v2/candles/trade:1h:tBTCGBP/hist?limit=1000&start="; //basic url in bitfinex api
    /*拿到全部hist的数据Candle*/
    var request = require("request");

    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();

    start = year + '-' + month + '-' + day + ' ' + hour + ':00:00';
    start = new Date(start)
    start = Date.parse(start)-3600000000;
    // console.log(start)
    // console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!")
    request.get(
        `${url}`+start+`&sort=1`,
        (error, response, body) => callback(body)
    )
    /*拿到全部的数据 hist
     [  e.g. MTS,   OPEN,  CLOSE, HIGH,  LOW, VOLUME
     [1526342400000,8670.8,8721.8,8748.9,8615,3882.44864537],
     [1526256000000,8683.5,8670.8,8884.5,8271,37154.66777466]
     ]
     */
};
module.exports.BitFinex_Candle_Data_BTC_EUR=function (callback) {
    var url = "https://api.bitfinex.com/v2/candles/trade:1h:tBTCEUR/hist?limit=1000&start="; //basic url in bitfinex api
    /*拿到全部hist的数据Candle*/
    var request = require("request");

    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();

    start = year + '-' + month + '-' + day + ' ' + hour + ':00:00';
    start = new Date(start)
    start = Date.parse(start)-3600000000;
    // console.log(start)
    // console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!")
    request.get(
        `${url}`+start+`&sort=1`,
        (error, response, body) => callback(body)
    )
    /*拿到全部的数据 hist
     [  e.g. MTS,   OPEN,  CLOSE, HIGH,  LOW, VOLUME
     [1526342400000,8670.8,8721.8,8748.9,8615,3882.44864537],
     [1526256000000,8683.5,8670.8,8884.5,8271,37154.66777466]
     ]
     */
};
module.exports.BitFinex_Candle_Data_ETH_USD=function (callback) {
    var url = "https://api.bitfinex.com/v2/candles/trade:1h:tETHUSD/hist?limit=1000&start="; //basic url in bitfinex api
    /*拿到全部hist的数据Candle*/
    var request = require("request");

    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();

    start = year + '-' + month + '-' + day + ' ' + hour + ':00:00';
    start = new Date(start)
    start = Date.parse(start)-3600000000;
    // console.log(start)
    // console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!")
    request.get(
        `${url}`+start+`&sort=1`,
        (error, response, body) => callback(body)
    )
    /*拿到全部的数据 hist
     [  e.g. MTS,   OPEN,  CLOSE, HIGH,  LOW, VOLUME
     [1526342400000,8670.8,8721.8,8748.9,8615,3882.44864537],
     [1526256000000,8683.5,8670.8,8884.5,8271,37154.66777466]
     ]
     */
};
module.exports.BitFinex_Candle_Data_ETH_GBP=function (callback) {
    var url = "https://api.bitfinex.com/v2/candles/trade:1h:tETHGBP/hist?limit=1000&start="; //basic url in bitfinex api
    /*拿到全部hist的数据Candle*/
    var request = require("request");

    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();

    start = year + '-' + month + '-' + day + ' ' + hour + ':00:00';
    start = new Date(start)
    start = Date.parse(start)-3600000000;
    // console.log(start)
    // console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!")
    request.get(
        `${url}`+start+`&sort=1`,
        (error, response, body) => callback(body)
    )
    /*拿到全部的数据 hist
     [  e.g. MTS,   OPEN,  CLOSE, HIGH,  LOW, VOLUME
     [1526342400000,8670.8,8721.8,8748.9,8615,3882.44864537],
     [1526256000000,8683.5,8670.8,8884.5,8271,37154.66777466]
     ]
     */
};
module.exports.BitFinex_Candle_Data_ETH_EUR=function (callback) {
    var url = "https://api.bitfinex.com/v2/candles/trade:1h:tETHEUR/hist?limit=1000&start="; //basic url in bitfinex api
    /*拿到全部hist的数据Candle*/
    var request = require("request");

    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();

    start = year + '-' + month + '-' + day + ' ' + hour + ':00:00';
    start = new Date(start)
    start = Date.parse(start)-3600000000;
    // console.log(start)
    // console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!")
    request.get(
        `${url}`+start+`&sort=1`,
        (error, response, body) => callback(body)
    )
    /*拿到全部的数据 hist
     [  e.g. MTS,   OPEN,  CLOSE, HIGH,  LOW, VOLUME
     [1526342400000,8670.8,8721.8,8748.9,8615,3882.44864537],
     [1526256000000,8683.5,8670.8,8884.5,8271,37154.66777466]
     ]
     */
};
/*GDAX Candle Stick*/
module.exports.GDAX_Candle_Data_API=function(start,end,callback,req,res) {
    //此方法的目的：通过GDAX获取每小时3600sec的Price和Volume-->>画出 第二行卡片的 Bar和Line 小图
    // console.dir(start+end)
    const request = require('request')
    request({
        qs: {
            start: start,
            end: end,
            granularity: 60
        },
        method: 'GET',
        uri: 'https://api.gdax.com/products/BTC-USD/candles',
        headers: {
            'User-Agent': 'gdax-node-client',
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    }, function(a,b,data){
        var twoarray = Revision.ChangeToTwoDArray(data);
        var jsonarray = Revision.TwoDArrayToJsonArray(twoarray);
        var revisions = 'dayData';
        callback(jsonarray)
    })

};
module.exports.GDAX_Candle_Data_GBP_API=function(start,end,callback,req,res) {
    //此方法的目的：通过GDAX获取每小时3600sec的Price和Volume-->>画出 第二行卡片的 Bar和Line 小图
    // console.dir(start+end)
    const request = require('request')
    request({
        qs: {
            start: start,
            end: end,
            granularity: 60
        },
        method: 'GET',
        uri: 'https://api.gdax.com/products/BTC-GBP/candles',
        headers: {
            'User-Agent': 'gdax-node-client',
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    }, function(a,b,data){
        var twoarray = Revision.ChangeToTwoDArray(data);
        var jsonarray = Revision.TwoDArrayToJsonArray(twoarray);
        var revisions = 'dayData';
        callback(jsonarray)
    })

};
module.exports.GDAX_Candle_Data_BTC_EUR_API=function(start,end,callback,req,res) {
    //此方法的目的：通过GDAX获取每小时3600sec的Price和Volume-->>画出 第二行卡片的 Bar和Line 小图
    // console.dir(start+end)
    const request = require('request')
    request({
        qs: {
            start: start,
            end: end,
            granularity: 60
        },
        method: 'GET',
        uri: 'https://api.gdax.com/products/BTC-EUR/candles',
        headers: {
            'User-Agent': 'gdax-node-client',
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    }, function(a,b,data){
        var twoarray = Revision.ChangeToTwoDArray(data);
        var jsonarray = Revision.TwoDArrayToJsonArray(twoarray);
        var revisions = 'dayData';
        callback(jsonarray)
    })

};
module.exports.GDAX_Candle_Data_ETH_USD_API=function(start,end,callback,req,res) {
    //此方法的目的：通过GDAX获取每小时3600sec的Price和Volume-->>画出 第二行卡片的 Bar和Line 小图
    // console.dir(start+end)
    const request = require('request')
    request({
        qs: {
            start: start,
            end: end,
            granularity: 60
        },
        method: 'GET',
        uri: 'https://api.gdax.com/products/ETH-USD/candles',
        headers: {
            'User-Agent': 'gdax-node-client',
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    }, function(a,b,data){
        var twoarray = Revision.ChangeToTwoDArray(data);
        var jsonarray = Revision.TwoDArrayToJsonArray(twoarray);
        var revisions = 'dayData';
        callback(jsonarray)
    })

};
module.exports.GDAX_Candle_Data_ETH_GBP_API=function(start,end,callback,req,res) {
    //此方法的目的：通过GDAX获取每小时3600sec的Price和Volume-->>画出 第二行卡片的 Bar和Line 小图
    // console.dir(start+end)
    const request = require('request')
    request({
        qs: {
            start: start,
            end: end,
            granularity: 60
        },
        method: 'GET',
        uri: 'https://api.gdax.com/products/ETH-GBP/candles',
        headers: {
            'User-Agent': 'gdax-node-client',
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    }, function(a,b,data){
        var twoarray = Revision.ChangeToTwoDArray(data);
        var jsonarray = Revision.TwoDArrayToJsonArray(twoarray);
        var revisions = 'dayData';
        callback(jsonarray)
    })

};
module.exports.GDAX_Candle_Data_ETH_EUR_API=function(start,end,callback,req,res) {
    //此方法的目的：通过GDAX获取每小时3600sec的Price和Volume-->>画出 第二行卡片的 Bar和Line 小图
    // console.dir(start+end)
    const request = require('request')
    request({
        qs: {
            start: start,
            end: end,
            granularity: 60
        },
        method: 'GET',
        uri: 'https://api.gdax.com/products/ETH-EUR/candles',
        headers: {
            'User-Agent': 'gdax-node-client',
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    }, function(a,b,data){
        var twoarray = Revision.ChangeToTwoDArray(data);
        var jsonarray = Revision.TwoDArrayToJsonArray(twoarray);
        var revisions = 'dayData';
        callback(jsonarray)
    })

};
//This part is written at the top of api_data.process.js
module.exports.CatchRealtimeData_Candle_GBP=function(start,end,req,res) {
    console.dir(start+end)
    const request = require('request')
    request({
        qs: {
            start: start,
            end: end,
            granularity: 60
        },
        method: 'GET',
        uri: 'https://api.gdax.com/products/BTC-GBP/candles',
        headers: {
            'User-Agent': 'gdax-node-client',
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    }, function(a,b,data){
        var twoarray = Revision.ChangeToTwoDArray(data);
        var jsonarray = Revision.TwoDArrayToJsonArray(twoarray);
        var revisions = 'GDAX_Candle_BTC_GBP';
        const callback = (result)=>{
            // console.log(result)
        }
        number = JSON.parse(data).length;
        console.log('Granules from direct request_GBP', number)
        if(number==1) {
            Revision.insertSingleData_GBP(mongoConnectUrl, revisions, jsonarray, callback);
            state = 'success'
        }
        else {
            state = 'failure'
        }
    })
};
/*Kraken Candle Stick*/
//测试数据
/*Bitstamp BTC/USD Candle Stick*/
//测试数据

//--------------Current BTC/USD Price Data-----------
/*BitFinex Current Price*/
module.exports.BitFinex_Current_BTCUSD=function (callback) {
    var url = "https://api.bitfinex.com/v2/"; //basic url in bitfinex api
    /*拿到最新last的数据Candle*/
    var request = require("request");
    request.get(
        `${url}/candles/trade:1D:tBTCUSD/last`,
        (error, response, body) => callback(body)
        //不知道能不能body.close找到Last Current Price
    );
    /*仅仅拿到最新的一条数据 last
     [MTS,         [1526362740000,
     OPEN,          8719.1,
     CLOSE,         8716.8,
     HIGH,          8719.1,
     LOW,           8710.3,
     VOLUME]        26.19711688]
     */
};
module.exports.BitFinex_Current_BTC_GBP=function (callback) {
    var url = "https://api.bitfinex.com/v2/"; //basic url in bitfinex api
    /*拿到最新last的数据Candle*/
    var request = require("request");
    request.get(
        `${url}/candles/trade:1D:tBTCGBP/last`,
        (error, response, body) => callback(body)
        //不知道能不能body.close找到Last Current Price
    );
    /*仅仅拿到最新的一条数据 last
     [MTS,         [1526362740000,
     OPEN,          8719.1,
     CLOSE,         8716.8,
     HIGH,          8719.1,
     LOW,           8710.3,
     VOLUME]        26.19711688]
     */
};
module.exports.BitFinex_Current_BTC_EUR=function (callback) {
    var url = "https://api.bitfinex.com/v2/"; //basic url in bitfinex api
    /*拿到最新last的数据Candle*/
    var request = require("request");
    request.get(
        `${url}/candles/trade:1D:tBTCEUR/last`,
        (error, response, body) => callback(body)
        //不知道能不能body.close找到Last Current Price
    );
    /*仅仅拿到最新的一条数据 last
     [MTS,         [1526362740000,
     OPEN,          8719.1,
     CLOSE,         8716.8,
     HIGH,          8719.1,
     LOW,           8710.3,
     VOLUME]        26.19711688]
     */
};
module.exports.BitFinex_Current_ETHUSD=function (callback) {
    var url = "https://api.bitfinex.com/v2/"; //basic url in bitfinex api
    /*拿到最新last的数据Candle*/
    var request = require("request");
    request.get(
        `${url}/candles/trade:1D:tETHUSD/last`,
        (error, response, body) => callback(body)
        //不知道能不能body.close找到Last Current Price
    );
    /*仅仅拿到最新的一条数据 last
     [MTS,         [1526362740000,
     OPEN,          8719.1,
     CLOSE,         8716.8,
     HIGH,          8719.1,
     LOW,           8710.3,
     VOLUME]        26.19711688]
     */
};
module.exports.BitFinex_Current_ETH_GBP=function (callback) {
    var url = "https://api.bitfinex.com/v2/"; //basic url in bitfinex api
    /*拿到最新last的数据Candle*/
    var request = require("request");
    request.get(
        `${url}/candles/trade:1D:tETHGBP/last`,
        (error, response, body) => callback(body)
        //不知道能不能body.close找到Last Current Price
    );
    /*仅仅拿到最新的一条数据 last
     [MTS,         [1526362740000,
     OPEN,          8719.1,
     CLOSE,         8716.8,
     HIGH,          8719.1,
     LOW,           8710.3,
     VOLUME]        26.19711688]
     */
};
module.exports.BitFinex_Current_ETH_EUR=function (callback) {
    var url = "https://api.bitfinex.com/v2/"; //basic url in bitfinex api
    /*拿到最新last的数据Candle*/
    var request = require("request");
    request.get(
        `${url}/candles/trade:1D:tETHEUR/last`,
        (error, response, body) => callback(body)
        //不知道能不能body.close找到Last Current Price
    );
    /*仅仅拿到最新的一条数据 last
     [MTS,         [1526362740000,
     OPEN,          8719.1,
     CLOSE,         8716.8,
     HIGH,          8719.1,
     LOW,           8710.3,
     VOLUME]        26.19711688]
     */
};
/*GDAX Current Price*/
module.exports.GDAX_Current_Price=function(callback) {
    const Gdax = require('gdax');
    const publicClient = new Gdax.PublicClient();
    publicClient.getProductTicker('BTC-USD', function (err,response,body) {
        if (err) {
            console.log("-----GDAX_Current_Price has error----");
        } else {
            // console.log(body);
            callback(body);
            /*
             {trade_id: 44042376,
             price: '7300.00000000',<<<<<------
             size: '0.01380000',
             bid: '7300',
             ask: '7300.01',
             volume: '5280.73877818',<<<<<------
             time: '2018-05-27T04:43:41.080000Z' }
             */
        }
    });
};
module.exports.GDAX_Current_Price_GBP=function(callback) {
    const Gdax = require('gdax');
    const publicClient = new Gdax.PublicClient();
    publicClient.getProductTicker('BTC-GBP', function (err,response,body) {
        if (err) {
            console.log("-----GDAX_Current_Price has error----");
        } else {
            // console.log(body);
            callback(body);
            /*
             {trade_id: 44042376,
             price: '7300.00000000',<<<<<------
             size: '0.01380000',
             bid: '7300',
             ask: '7300.01',
             volume: '5280.73877818',<<<<<------
             time: '2018-05-27T04:43:41.080000Z' }
             */
        }
    });
};
module.exports.GDAX_Current_Price_BTC_EUR=function(callback) {
    const Gdax = require('gdax');
    const publicClient = new Gdax.PublicClient();
    publicClient.getProductTicker('BTC-EUR', function (err,response,body) {
        if (err) {
            console.log("-----GDAX_Current_Price has error----");
        } else {
            // console.log(body);
            callback(body);
            /*
             {trade_id: 44042376,
             price: '7300.00000000',<<<<<------
             size: '0.01380000',
             bid: '7300',
             ask: '7300.01',
             volume: '5280.73877818',<<<<<------
             time: '2018-05-27T04:43:41.080000Z' }
             */
        }
    });
};
module.exports.GDAX_Current_Price_ETH=function(callback) {
    const Gdax = require('gdax');
    const publicClient = new Gdax.PublicClient();
    publicClient.getProductTicker('ETH-USD', function (err,response,body) {
        if (err) {
            console.log("-----GDAX_Current_Price has error----");
        } else {
            // console.log(body);
            callback(body);
            /*
             {trade_id: 44042376,
             price: '7300.00000000',<<<<<------
             size: '0.01380000',
             bid: '7300',
             ask: '7300.01',
             volume: '5280.73877818',<<<<<------
             time: '2018-05-27T04:43:41.080000Z' }
             */
        }
    });
};
module.exports.GDAX_Current_Price_GBP_ETH=function(callback) {
    const Gdax = require('gdax');
    const publicClient = new Gdax.PublicClient();
    publicClient.getProductTicker('ETH-GBP', function (err,response,body) {
        if (err) {
            console.log("-----GDAX_Current_Price has error----");
        } else {
            // console.log(body);
            callback(body);
            /*
             {trade_id: 44042376,
             price: '7300.00000000',<<<<<------
             size: '0.01380000',
             bid: '7300',
             ask: '7300.01',
             volume: '5280.73877818',<<<<<------
             time: '2018-05-27T04:43:41.080000Z' }
             */
        }
    });
};
module.exports.GDAX_Current_Price_ETH_EUR=function(callback) {
    const Gdax = require('gdax');
    const publicClient = new Gdax.PublicClient();
    publicClient.getProductTicker('ETH-EUR', function (err,response,body) {
        if (err) {
            console.log("-----GDAX_Current_Price has error----");
        } else {
            // console.log(body);
            callback(body);
            /*
             {trade_id: 44042376,
             price: '7300.00000000',<<<<<------
             size: '0.01380000',
             bid: '7300',
             ask: '7300.01',
             volume: '5280.73877818',<<<<<------
             time: '2018-05-27T04:43:41.080000Z' }
             */
        }
    });
};
/*Kraken Current Price*/
//测试数据
/*Bitstamp Current Price*/
//测试数据

//--------------CardView hourly Volume and closePrice-----------
/*BitFinex CardView Volume and closePrice*/
module.exports.BitFinex_Vol_Price_Data=function (callback) {
    var url = "https://api.bitfinex.com/v2/candles/trade:1h:tBTCUSD/hist?limit=24&start="; //basic url in bitfinex api
    /*拿到全部hist的数据Candle    -------->>>>>>> BTC/USD <<<<<<<<<-------*/
    var request = require("request");

    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();

    start = year + '-' + month + '-' + day + ' ' + hour + ':00:00';
    start = new Date(start)
    start = Date.parse(start)-86400000;
    // console.log(start)
    // console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!")
    request.get(
        `${url}`+start+`&sort=1`,
        (error, response, body) => callback(body)
    )
    /*拿到全部的数据 hist
     [  e.g. MTS,   OPEN,  CLOSE, HIGH,  LOW, VOLUME
     [1526342400000,8670.8,8721.8,8748.9,8615,3882.44864537],
     [1526256000000,8683.5,8670.8,8884.5,8271,37154.66777466]
     ]
     */
};
module.exports.BitFinex_Vol_Price_GBP_Data=function (callback) {
    var url = "https://api.bitfinex.com/v2/candles/trade:1h:tBTCGBP/hist?limit=24&start="; //basic url in bitfinex api
    /*拿到全部hist的数据Candle    -------->>>>>>> BTC/EUR <<<<<<<<<-------*/
    var request = require("request");

    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();

    start = year + '-' + month + '-' + day + ' ' + hour + ':00:00';
    start = new Date(start)
    start = Date.parse(start)-86400000;
    // console.log(start)
    // console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!")
    request.get(
        `${url}`+start+`&sort=1`,
        (error, response, body) => callback(body)
    )
    /*拿到全部的数据 hist
     [  e.g. MTS,   OPEN,  CLOSE, HIGH,  LOW, VOLUME
     [1526342400000,8670.8,8721.8,8748.9,8615,3882.44864537],
     [1526256000000,8683.5,8670.8,8884.5,8271,37154.66777466]
     ]
     */
};
module.exports.BitFinex_Vol_Price_EUR_Data_BTC=function (callback) {
    var url = "https://api.bitfinex.com/v2/candles/trade:1h:tBTCEUR/hist?limit=24&start="; //basic url in bitfinex api
    /*拿到全部hist的数据Candle    -------->>>>>>> BTC/EUR <<<<<<<<<-------*/
    var request = require("request");

    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();

    start = year + '-' + month + '-' + day + ' ' + hour + ':00:00';
    start = new Date(start)
    start = Date.parse(start)-86400000;
    // console.log(start)
    // console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!")
    request.get(
        `${url}`+start+`&sort=1`,
        (error, response, body) => callback(body)
    )
    /*拿到全部的数据 hist
     [  e.g. MTS,   OPEN,  CLOSE, HIGH,  LOW, VOLUME
     [1526342400000,8670.8,8721.8,8748.9,8615,3882.44864537],
     [1526256000000,8683.5,8670.8,8884.5,8271,37154.66777466]
     ]
     */
};
module.exports.BitFinex_Vol_Price_Data_ETH=function (callback) {
    var url = "https://api.bitfinex.com/v2/candles/trade:1h:tETHUSD/hist?limit=24&start="; //basic url in bitfinex api
    /*拿到全部hist的数据Candle    -------->>>>>>> BTC/USD <<<<<<<<<-------*/
    var request = require("request");

    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();

    start = year + '-' + month + '-' + day + ' ' + hour + ':00:00';
    start = new Date(start)
    start = Date.parse(start)-86400000;
    // console.log(start)
    // console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!")
    request.get(
        `${url}`+start+`&sort=1`,
        (error, response, body) => callback(body)
    )
    /*拿到全部的数据 hist
     [  e.g. MTS,   OPEN,  CLOSE, HIGH,  LOW, VOLUME
     [1526342400000,8670.8,8721.8,8748.9,8615,3882.44864537],
     [1526256000000,8683.5,8670.8,8884.5,8271,37154.66777466]
     ]
     */
};
module.exports.BitFinex_Vol_Price_GBP_Data_ETH=function (callback) {
    var url = "https://api.bitfinex.com/v2/candles/trade:1h:tETHGBP/hist?limit=24&start="; //basic url in bitfinex api
    /*拿到全部hist的数据Candle    -------->>>>>>> BTC/EUR <<<<<<<<<-------*/
    var request = require("request");

    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();

    start = year + '-' + month + '-' + day + ' ' + hour + ':00:00';
    start = new Date(start)
    start = Date.parse(start)-86400000;
    // console.log(start)
    // console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!")
    request.get(
        `${url}`+start+`&sort=1`,
        (error, response, body) => callback(body)
    )
    /*拿到全部的数据 hist
     [  e.g. MTS,   OPEN,  CLOSE, HIGH,  LOW, VOLUME
     [1526342400000,8670.8,8721.8,8748.9,8615,3882.44864537],
     [1526256000000,8683.5,8670.8,8884.5,8271,37154.66777466]
     ]
     */
};
module.exports.BitFinex_Vol_Price_EUR_Data_ETH=function (callback) {
    var url = "https://api.bitfinex.com/v2/candles/trade:1h:tETHEUR/hist?limit=24&start="; //basic url in bitfinex api
    /*拿到全部hist的数据Candle    -------->>>>>>> BTC/EUR <<<<<<<<<-------*/
    var request = require("request");

    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();

    start = year + '-' + month + '-' + day + ' ' + hour + ':00:00';
    start = new Date(start)
    start = Date.parse(start)-86400000;
    // console.log(start)
    // console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!")
    request.get(
        `${url}`+start+`&sort=1`,
        (error, response, body) => callback(body)
    )
    /*拿到全部的数据 hist
     [  e.g. MTS,   OPEN,  CLOSE, HIGH,  LOW, VOLUME
     [1526342400000,8670.8,8721.8,8748.9,8615,3882.44864537],
     [1526256000000,8683.5,8670.8,8884.5,8271,37154.66777466]
     ]
     */
};

/*GDAX CardView Volume and closePrice*/
module.exports.GDAX_Vol_Price_Data=function(start,end,callback,req,res) {
    //此方法的目的：通过GDAX获取每小时3600sec的Price和Volume-->>画出 第二行卡片的 Bar和Line 小图
    console.dir(start+end)
    const request = require('request')
    request({
        qs: {
            start: start,
            end: end,
            granularity: 3600
        },
        method: 'GET',
        uri: 'https://api.gdax.com/products/BTC-USD/candles',
        headers: {
            'User-Agent': 'gdax-node-client',
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    }, function(a,b,data){
        var twoarray = Revision.ChangeToTwoDArray(data);
        var jsonarray = Revision.TwoDArrayToJsonArray(twoarray);
        var revisions = 'dayData';
        callback(jsonarray)
        // const callback = (result)=>{
        //     // console.log(result)
        // }
        // number = JSON.parse(data).length;
        // console.log('Granules from direct request', number)
        // if(number==1) {
        //     Revision.insertSingleData(mongoConnectUrl, revisions, jsonarray, callback);
        //     state = 'success'
        // }
        // else {
        //     state = 'failure'
        // }
    })

};
module.exports.GDAX_Vol_Price_GBP_Data=function(start,end,callback,req,res) {
    //此方法的目的：通过GDAX获取每小时3600sec的Price和Volume-->>画出 第二行卡片的 Bar和Line 小图
    //
    console.dir(start+end)
    const request = require('request')
    request({
        qs: {
            start: start,
            end: end,
            granularity: 3600
        },
        method: 'GET',
        uri: 'https://api.gdax.com/products/BTC-GBP/candles',
        headers: {
            'User-Agent': 'gdax-node-client',
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    }, function(a,b,data){
        var twoarray = Revision.ChangeToTwoDArray(data);
        var jsonarray = Revision.TwoDArrayToJsonArray(twoarray);
        var revisions = 'dayData';
        callback(jsonarray)
        // const callback = (result)=>{
        //     // console.log(result)
        // }
        // number = JSON.parse(data).length;
        // console.log('Granules from direct request', number)
        // if(number==1) {
        //     Revision.insertSingleData(mongoConnectUrl, revisions, jsonarray, callback);
        //     state = 'success'
        // }
        // else {
        //     state = 'failure'
        // }
    })

};
module.exports.GDAX_Vol_Price_EUR_Data_BTC=function(start,end,callback,req,res) {
    //此方法的目的：通过GDAX获取每小时3600sec的Price和Volume-->>画出 第二行卡片的 Bar和Line 小图
    //
    console.dir(start+end)
    const request = require('request')
    request({
        qs: {
            start: start,
            end: end,
            granularity: 3600
        },
        method: 'GET',
        uri: 'https://api.gdax.com/products/BTC-EUR/candles',
        headers: {
            'User-Agent': 'gdax-node-client',
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    }, function(a,b,data){
        var twoarray = Revision.ChangeToTwoDArray(data);
        var jsonarray = Revision.TwoDArrayToJsonArray(twoarray);
        var revisions = 'dayData';
        callback(jsonarray)
        // const callback = (result)=>{
        //     // console.log(result)
        // }
        // number = JSON.parse(data).length;
        // console.log('Granules from direct request', number)
        // if(number==1) {
        //     Revision.insertSingleData(mongoConnectUrl, revisions, jsonarray, callback);
        //     state = 'success'
        // }
        // else {
        //     state = 'failure'
        // }
    })

};
module.exports.GDAX_Vol_Price_Data_ETH=function(start,end,callback,req,res) {
    //此方法的目的：通过GDAX获取每小时3600sec的Price和Volume-->>画出 第二行卡片的 Bar和Line 小图
    console.dir(start+end)
    const request = require('request')
    request({
        qs: {
            start: start,
            end: end,
            granularity: 3600
        },
        method: 'GET',
        uri: 'https://api.gdax.com/products/ETH-USD/candles',
        headers: {
            'User-Agent': 'gdax-node-client',
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    }, function(a,b,data){
        var twoarray = Revision.ChangeToTwoDArray(data);
        var jsonarray = Revision.TwoDArrayToJsonArray(twoarray);
        var revisions = 'dayData';
        callback(jsonarray)
        // const callback = (result)=>{
        //     // console.log(result)
        // }
        // number = JSON.parse(data).length;
        // console.log('Granules from direct request', number)
        // if(number==1) {
        //     Revision.insertSingleData(mongoConnectUrl, revisions, jsonarray, callback);
        //     state = 'success'
        // }
        // else {
        //     state = 'failure'
        // }
    })

};
module.exports.GDAX_Vol_Price_GBP_Data_ETH=function(start,end,callback,req,res) {
    //此方法的目的：通过GDAX获取每小时3600sec的Price和Volume-->>画出 第二行卡片的 Bar和Line 小图
    //
    console.dir(start+end)
    const request = require('request')
    request({
        qs: {
            start: start,
            end: end,
            granularity: 3600
        },
        method: 'GET',
        uri: 'https://api.gdax.com/products/ETH-GBP/candles',
        headers: {
            'User-Agent': 'gdax-node-client',
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    }, function(a,b,data){
        var twoarray = Revision.ChangeToTwoDArray(data);
        var jsonarray = Revision.TwoDArrayToJsonArray(twoarray);
        var revisions = 'dayData';
        callback(jsonarray)
        // const callback = (result)=>{
        //     // console.log(result)
        // }
        // number = JSON.parse(data).length;
        // console.log('Granules from direct request', number)
        // if(number==1) {
        //     Revision.insertSingleData(mongoConnectUrl, revisions, jsonarray, callback);
        //     state = 'success'
        // }
        // else {
        //     state = 'failure'
        // }
    })

};
module.exports.GDAX_Vol_Price_EUR_Data_ETH=function(start,end,callback,req,res) {
    //此方法的目的：通过GDAX获取每小时3600sec的Price和Volume-->>画出 第二行卡片的 Bar和Line 小图
    //
    console.dir(start+end)
    const request = require('request')
    request({
        qs: {
            start: start,
            end: end,
            granularity: 3600
        },
        method: 'GET',
        uri: 'https://api.gdax.com/products/ETH-EUR/candles',
        headers: {
            'User-Agent': 'gdax-node-client',
            Accept: 'application/json',
            'Content-Type': 'application/json'
        }
    }, function(a,b,data){
        var twoarray = Revision.ChangeToTwoDArray(data);
        var jsonarray = Revision.TwoDArrayToJsonArray(twoarray);
        var revisions = 'dayData';
        callback(jsonarray)
        // const callback = (result)=>{
        //     // console.log(result)
        // }
        // number = JSON.parse(data).length;
        // console.log('Granules from direct request', number)
        // if(number==1) {
        //     Revision.insertSingleData(mongoConnectUrl, revisions, jsonarray, callback);
        //     state = 'success'
        // }
        // else {
        //     state = 'failure'
        // }
    })

};
/*Bitstamp CardView Volume and closePrice*/
//测试数据
/*Kraken CardView Volume and closePrice*/
//测试数据
/*LakeBTC CardView Volume and closePrice*/
//测试数据

//--------------OrderBook -----------
/*BitFinex Order book Data*/
module.exports.BitFinex_Orderbook_Depth_BTCUSD=function (callback,req, res) {

    var url = "https://api.bitfinex.com/v1/"; //此处的v2改成了v1，是因为从WS到REST

    /*拿到最新 这一毫秒的 bids 和 asks值  各50个值 相同价钱的Vol求和*/
    var request = require("request");
    request.get(
        `${url}/book/BTCUSD/?group=1&limit_bids=50&limit_asks=50`,
        (error, response, body) => { callback(body)}
    );
    /*Order book Depth chart 的数据返回格式
     {"bids":
        [{"price":"7481.3","amount":"1.71801488","timestamp":"1528155244.0"},
         {"price":"7481","amount":"0.8","timestamp":"1528155244.0"},
         {"price":"7480.2","amount":"0.00307","timestamp":"1528155244.0"},...{}
        ],
      "asks":
        [{"price":"7481.4","amount":"5.04790866","timestamp":"1528155244.0"},
         {"price":"7481.5","amount":"0.05025649","timestamp":"1528155244.0"},
         {"price":"7482.9","amount":"1.49539886","timestamp":"1528155244.0"},...{}
        ]
     }
     */
};
module.exports.BitFinex_Orderbook_Depth_BTCGBP=function (callback,req, res) {

    var url = "https://api.bitfinex.com/v1/"; //此处的v2改成了v1，是因为从WS到REST

    /*拿到最新 这一毫秒的 bids 和 asks值  各50个值 相同价钱的Vol求和*/
    var request = require("request");
    request.get(
        `${url}/book/BTCGBP/?group=1&limit_bids=50&limit_asks=50`,
        (error, response, body) => {callback(body)}
    );
    /*Order book Depth chart 的数据返回格式
    ------>>>> price <<<<----
    ------>>>> amount 就是 Volume <<<<----
     {"bids":
     [
        {"price":"5617.7","amount":"22.54270935","timestamp":"1528155505.0"},
        {"price":"5617.6","amount":"0.2","timestamp":"1528155505.0"}, ... ,{}
     ],
     "asks":
     [
        {"price":"5617.9","amount":"25","timestamp":"1528155505.0"},
        {"price":"5619","amount":"1.836","timestamp":"1528155505.0"}, ... ,{}
     ]
     }
     */
};
module.exports.BitFinex_Orderbook_Depth_BTCEUR=function (callback,req, res) {

    var url = "https://api.bitfinex.com/v1/"; //此处的v2改成了v1，是因为从WS到REST

    /*拿到最新 这一毫秒的 bids 和 asks值  各50个值 相同价钱的Vol求和*/
    var request = require("request");
    request.get(
        `${url}/book/BTCEUR/?group=1&limit_bids=50&limit_asks=50`,
        (error, response, body) => { callback(body)}
    );
    /*Order book Depth chart 的数据返回格式
     {"bids":
     [{"price":"7481.3","amount":"1.71801488","timestamp":"1528155244.0"},
     {"price":"7481","amount":"0.8","timestamp":"1528155244.0"},
     {"price":"7480.2","amount":"0.00307","timestamp":"1528155244.0"},...{}
     ],
     "asks":
     [{"price":"7481.4","amount":"5.04790866","timestamp":"1528155244.0"},
     {"price":"7481.5","amount":"0.05025649","timestamp":"1528155244.0"},
     {"price":"7482.9","amount":"1.49539886","timestamp":"1528155244.0"},...{}
     ]
     }
     */
};
module.exports.BitFinex_Orderbook_Depth_ETHUSD=function (callback,req, res) {

    var url = "https://api.bitfinex.com/v1/"; //此处的v2改成了v1，是因为从WS到REST

    /*拿到最新 这一毫秒的 bids 和 asks值  各50个值 相同价钱的Vol求和*/
    var request = require("request");
    request.get(
        `${url}/book/ETHUSD/?group=1&limit_bids=50&limit_asks=50`,
        (error, response, body) => { callback(body)}
    );
    /*Order book Depth chart 的数据返回格式
     {"bids":
     [{"price":"7481.3","amount":"1.71801488","timestamp":"1528155244.0"},
     {"price":"7481","amount":"0.8","timestamp":"1528155244.0"},
     {"price":"7480.2","amount":"0.00307","timestamp":"1528155244.0"},...{}
     ],
     "asks":
     [{"price":"7481.4","amount":"5.04790866","timestamp":"1528155244.0"},
     {"price":"7481.5","amount":"0.05025649","timestamp":"1528155244.0"},
     {"price":"7482.9","amount":"1.49539886","timestamp":"1528155244.0"},...{}
     ]
     }
     */
};
module.exports.BitFinex_Orderbook_Depth_ETHGBP=function (callback,req, res) {

    var url = "https://api.bitfinex.com/v1/"; //此处的v2改成了v1，是因为从WS到REST

    /*拿到最新 这一毫秒的 bids 和 asks值  各50个值 相同价钱的Vol求和*/
    var request = require("request");
    request.get(
        `${url}/book/ETHGBP/?group=1&limit_bids=50&limit_asks=50`,
        (error, response, body) => {callback(body)}
    );
    /*Order book Depth chart 的数据返回格式
     ------>>>> price <<<<----
     ------>>>> amount 就是 Volume <<<<----
     {"bids":
     [
     {"price":"5617.7","amount":"22.54270935","timestamp":"1528155505.0"},
     {"price":"5617.6","amount":"0.2","timestamp":"1528155505.0"}, ... ,{}
     ],
     "asks":
     [
     {"price":"5617.9","amount":"25","timestamp":"1528155505.0"},
     {"price":"5619","amount":"1.836","timestamp":"1528155505.0"}, ... ,{}
     ]
     }
     */
};
module.exports.BitFinex_Orderbook_Depth_ETHEUR=function (callback,req, res) {

    var url = "https://api.bitfinex.com/v1/"; //此处的v2改成了v1，是因为从WS到REST

    /*拿到最新 这一毫秒的 bids 和 asks值  各50个值 相同价钱的Vol求和*/
    var request = require("request");
    request.get(
        `${url}/book/ETHEUR/?group=1&limit_bids=50&limit_asks=50`,
        (error, response, body) => {callback(body)}
    );
    /*Order book Depth chart 的数据返回格式
     ------>>>> price <<<<----
     ------>>>> amount 就是 Volume <<<<----
     {"bids":
     [
     {"price":"5617.7","amount":"22.54270935","timestamp":"1528155505.0"},
     {"price":"5617.6","amount":"0.2","timestamp":"1528155505.0"}, ... ,{}
     ],
     "asks":
     [
     {"price":"5617.9","amount":"25","timestamp":"1528155505.0"},
     {"price":"5619","amount":"1.836","timestamp":"1528155505.0"}, ... ,{}
     ]
     }
     */
};
/*GDAX Order book Data*/
//GDAX uses url to get JSON to draw depth chart in --->>> depth-gdax-btc-usd.js <<<---
//GDAX draw depth chart with GBP in depth folder ---->>> depth-gdax-btc-gbp.js <<<----

/*Get JSON from URL*/
function getDataFromUrl(url,callback) {
    var request = require("request");
    request({
        url: url,
        json: true
    }, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            callback(error, body);
        }
    });
}
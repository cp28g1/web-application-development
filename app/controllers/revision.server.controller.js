Revision = require("./models/revision");
Api_data = require("./models/api_data.process");
var mongoConnectUrl = 'mongodb://localhost:27017';

/*Zhaohui's data process*/
module.exports.showMain=function (req, res) {
    res.render('NEW.ejs')
};
module.exports.catchData = function (req,res) {

    var currentDate = new Date();
    Api_data.Product_HistoryRate();

    /*Here we want to put 'history data'
    which can be called dead data, need to progress before demo*/
};
module.exports.Candle_Stick_Data=function(req,res) {
    var currentDate = new Date();
    var currentDateIso8601 = Date.parse(currentDate)
    console.log(currentDateIso8601);
    Revision.selectHistoryRate(currentDateIso8601/1000,function (result) {
        //勿删 数组化最简单的方法
        // var arrayData = result.map(item => {
        //     return Object.values(item)
        // })
        var TwoDArray = Revision.JsonArrayTwoDArray(result) ;
        res.send(TwoDArray);
    });
    console.dir("Candle_Stick_Data OK!");
};
module.exports.showIndex=function (req, res) {
    res.render('dashboard-analytics.html')
    // var currentDate = new Date();
    // Api_data.Product_HistoryRrate();
    // Api_data.Bitstamp_Trade_wsTest;
    // function test(req,res) {
    //     Api_data.Binance_Current_PriceUSDT();
    //     console.dir("Binance_Current_Price_USDT OK!");
    };

//GDAX candle data insert to MongoDB
module.exports.catchRealtimeData = function (req, res) {

    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();

    start = year + '-' + month + '-' + day + ' ' + '00:00:00';
    start = new Date(start)

    function realupdate() {
        start = Date.parse(start)
        // console.log(start)
        var nomalStart = new Date(start);
        // console.log(nomalStart)
        start = start + 60000;
        var end = start
        var nomalEnd = new Date(end);

        var currentDate = new Date();
        var currentDateIso8601 = Date.parse(currentDate)

        if (start >= currentDateIso8601 - 60000) {
            start = start - 60000;
            console.log('overtime')
        }
        else {
            Api_data.CatchRealtimeData(nomalStart, nomalEnd);
        }

        start = new Date(start)

    }
    setInterval(realupdate, 5000);
};
module.exports.catchRealtimeDataGroup = function (req, res) {

    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();

    startG = year + '-' + month + '-' + day + ' ' + '00:00:00';
    startG = new Date(startG)

    function realupdate() {
        startG = Date.parse(startG)
        // console.log(start)
        var nomalStart = new Date(startG);
        // console.log(nomalStart)
        startG = startG + 60000;
        var end = startG
        var nomalEnd = new Date(end);

        var currentDate = new Date();
        var currentDateIso8601 = Date.parse(currentDate)

        if (startG >= currentDateIso8601 - 60000) {
            startG = startG - 60000;
            console.log('overtime')
        }
        else {
            Api_data.CatchRealtimeData_Candle_GBP(nomalStart, nomalEnd);
        }

        startG = new Date(startG)

    }
    setInterval(realupdate, 5000);
};

/*Bitstamp Bitcoin Website*/
module.exports.Candle_Stick_Data_Bitstamp=function (req, res) {
    Api_data.Bitstamp_Trade_History();
    console.dir("Bitstamp_Trade_History OK!");
};
module.exports.Bitstamp_Trade_wsTest=function (req, res) {
    Api_data.Bitstamp_Trade_wsTest();
    console.dir("Bitstamp_Trade_wsTest OK!");
};
module.exports.Bitstamp_Trade_History=function (req, res) {
    Api_data.Bitstamp_Trade_History();
    console.dir("Bitstamp_Trade_History OK!");
};
module.exports.Bitstamp_Currency_Price=function (req, res) {
    Api_data.Bitstamp_Currency_Price();
    console.dir("Bitstamp_Current_Price OK!");
};

/*Leon's depth chart render*/
module.exports.depth=function (req, res) {
    res.render('depth.html')
}
module.exports.Realtime_Global_Data = function (req, res) {
        var realDate = new Date();
        var year = realDate.getFullYear();
        var month = realDate.getMonth() + 1;
        var day = realDate.getDate();

        var today = year + '-' + month + '-' + day + ' ' + '00:00:00';
        today = new Date(today);
        today = Date.parse(today) / 1000;
        // var timetest =1525183860;
        // console.dir(today);
        Revision.selectGlobalData(today, function (result) {
            var globalresult = Revision.JsonArrayForDepth(result);
            res.send(globalresult);
        });
    }
module.exports.Candel_Stick_Data = function (req, res) {
        var currentDate = new Date();
        var currentDateIso8601 = Date.parse(currentDate)
        var time = 1525183860;

        Revision.selectDepthData(time, function (result) {
            //勿删 数组化最简单的方法
            // var arrayData = result.map(item => {
            //     return Object.values(item)
            // })

            //var TwoDArray = Revision.JsonArrayTwoDArray(result) ;
            // console.log(result);
            var depthresult = Revision.JsonArrayForDepth(result);
            //console.log(depthresult)
            // var depthdata = Revision.JsonArrayForDepth(result) ;
            //
            res.send(depthresult);
        });
        console.dir("Candel_Stick_Data OK!");
    };

module.exports.showIndex = function (req, res) {
        res.render('dashboard-analytics.html');
    };

module.exports.depth = function (req, res) {
    res.render('depth.html')
};

module.exports.login = function (req, res) {
    res.render('user-login.html')
};

module.exports.reg = function (req, res) {
    res.render('user-register.html')
};

module.exports.forgotpwd = function (req, res) {
    res.render('user-forgotpwd.html')
};

module.exports.profile = function (req, res) {
    res.render('user-profile-page.html')
};
module.exports.lock = function (req, res) {
    res.render('user-lock-screen.html')
};


/*Binance Bitcoin Website*/
module.exports.Binance_Current_Price_USDT = function (req, res) {
    Api_data.Binance_Current_PriceUSDT();
    console.dir("Binance_Current_Price_USDT OK!");
};
module.exports.Binance_Candle_BNB = function (req, res) {
    Api_data.Binance_Candle_BNB();
    console.dir("Binance_Candle_BNB OK!");
};
module.exports.Binance_Candle_wsBNB = function (req, res) {
    Api_data.Binance_Candle_wsBNB();
    console.dir("Binance_Candle_wsBNB OK!");
};
module.exports.Binance_Candle_wsBNB_realTime = function (req, res) {
    Api_data.Binance_Candle_wsBNB_realTime();
    console.dir("Binance_Candle_wsBNB_realTime OK!");
};
module.exports.Binance_24hr_BNBchange = function (req, res) {
    Api_data.Binance_24hr_BNBchange();
    console.dir("Binance_24hr_BNBchange OK!");
};
module.exports.Binance_24hr_wsBNBchange = function (req, res) {
    Api_data.Binance_24hr_wsBNBchange();
    console.dir("Binance_24hr_wsBNBchange OK!");
};
module.exports.Binance_Depth_wsBNB = function (req, res) {
    Api_data.Binance_Depth_wsBNB();
    console.dir("Binance_Depth_wsBNB OK!");
};

/*Bitstamp Bitcoin Website*/
module.exports.Candle_Stick_Data_Bitstamp = function (req, res) {
    Api_data.Bitstamp_Trade_History();
    console.dir("Bitstamp_Trade_History OK!");
};
module.exports.Bitstamp_Trade_wsTest = function (req, res) {
    Api_data.Bitstamp_Trade_wsTest();
    console.dir("Bitstamp_Trade_wsTest OK!");
};
module.exports.Bitstamp_Currency_Price = function (req, res) {
    Api_data.Bitstamp_Currency_Price();
    console.dir("Bitstamp_Current_Price OK!");
};

/*BitFinex Bitcoin Website*/
module.exports.BitFinex_Platform_Status = function (req, res) {
    Api_data.BitFinex_Platform_Status();
    console.dir("BitFinex Platform Status OK!");
};
module.exports.BitFinex_Candle_Data = function (req, res) {
    Api_data.BitFinex_Candle_Data();
    console.dir("BitFinex Candle Data OK!");
};


/*Bitnodes WorldMap Data*/
module.exports.Bitnodes_World_Map_Data = function (req, res) {
    Api_data.Bitnodes_World_Map_Data();
    console.dir("Bitnodes World Map Data success!");
};

/*CCXT Kraken Bitcoin Website*/
module.exports.CCXT_Kraken_TickerBTCUSD = function (req, res) {
    Api_data.CCXT_Kraken_TickerBTCUSD();
    console.dir("CCXT Kraken Ticker BTC/USD OK!");
};

/*CoinMarketCap Global Data*/
module.exports.CoinMC_Global_OTC_USD = function (req, res) {
    Api_data.CoinMC_Global_OTC_USD();
    console.dir("Global_OTC_BTC/USD OK!");
};
module.exports.CoinMC_Global_OTC_EUR = function (req, res) {
    Api_data.CoinMC_Global_OTC_EUR();
    console.dir("Global_OTC_BTC/EUR OK!");
};
module.exports.CoinMC_Global_OTC_CNY = function (req, res) {
    Api_data.CoinMC_Global_OTC_CNY();
    console.dir("Global_OTC_BTC/CNY OK!");
};
module.exports.CoinMC_Global_OTC_AUD = function (req, res) {
    Api_data.CoinMC_Global_OTC_AUD();
    console.dir("Global_OTC_BTC/AUD OK!");
};
module.exports.CoinMC_Global_OTC_GBP = function (req, res) {
    Api_data.CoinMC_Global_OTC_GBP();
    console.dir("Global_OTC_BTC/GBP OK!");
};

/*Global data callback controller*/
module.exports.GlobalUSD = function (req, res) {
    const callback = (result)=>{
        //return result
        result = Revision.GlobalBitcoinData(result,"1","USD")
        res.send(result);
    }
    var data = Api_data.CoinMC_Global_OTC_USD(callback);

    //console.dir("Global_OTC_BTC/USD OK!");
};
module.exports.GlobalEUR = function (req, res) {
    const callback = (result)=>{
        //return result
        result = Revision.GlobalBitcoinData(result,"1","EUR");
        res.send(result);
    };
    var data = Api_data.CoinMC_Global_OTC_EUR(callback);
    //console.dir("Global_OTC_BTC/EUR OK!");
};
module.exports.GlobalCNY = function (req, res) {
    const callback = (result)=>{
        //return result
        result = Revision.GlobalBitcoinData(result,"1","CNY");
        res.send(result);
    };
    var data = Api_data.CoinMC_Global_OTC_CNY(callback);
    console.dir("Global_OTC_BTC/CNY OK!");
};
module.exports.GlobalAUD = function (req, res) {
    const callback = (result)=>{
        //return result
        result = Revision.GlobalBitcoinData(result,"1","AUD");
        res.send(result);
    };
    var data = Api_data.CoinMC_Global_OTC_AUD(callback);
    console.dir("Global_OTC_BTC/AUD OK!");
};
module.exports.GlobalGBP = function (req, res) {
    const callback = (result)=>{
        //return result
        result = Revision.GlobalBitcoinData(result,"1","GBP");
        res.send(result);
    };
    var data = Api_data.CoinMC_Global_OTC_GBP(callback);
    console.dir("Global_OTC_BTC/GBP OK!");
};
/*Global data Ethereum callback controller*/
module.exports.Global_ETH_USD = function (req, res) {
    const callback = (result)=>{
        //return result
        result = Revision.GlobalEthereumData(result,"1027","USD")
        res.send(result);
    }
    var data = Api_data.CoinMC_Global_OTC_ETHUSD(callback);

    //console.dir("Global_OTC_BTC/USD OK!");
};
module.exports.Global_ETH_EUR = function (req, res) {
    const callback = (result)=>{
        //return result
        result = Revision.GlobalEthereumData(result,"1027","EUR");
        res.send(result);
    };
    var data = Api_data.CoinMC_Global_OTC_ETHEUR(callback);
};
module.exports.Global_ETH_CNY = function (req, res) {
    const callback = (result)=>{
        //return result
        result = Revision.GlobalEthereumData(result,"1027","CNY");
        res.send(result);
    };
    var data = Api_data.CoinMC_Global_OTC_ETHCNY(callback);
    console.dir("Global_OTC_BTC/CNY OK!");
};
module.exports.Global_ETH_AUD = function (req, res) {
    const callback = (result)=>{
        //return result
        result = Revision.GlobalEthereumData(result,"1027","AUD");
        res.send(result);
    };
    var data = Api_data.CoinMC_Global_OTC_ETHAUD(callback);
    console.dir("Global_OTC_BTC/AUD OK!");
};
module.exports.Global_ETH_GBP = function (req, res) {
    const callback = (result)=>{
        //return result
        result = Revision.GlobalEthereumData(result,"1027","GBP");
        res.send(result);
    };
    var data = Api_data.CoinMC_Global_OTC_ETHGBP(callback);
    console.dir("Global_OTC_ETH/GBP OK!");
};
/*Global data Litecoin callback controller*/
module.exports.Global_LTC_USD = function (req, res) {
    const callback = (result)=>{
        //return result
        result = Revision.GlobalLitecoinData(result,"2","USD")
        res.send(result);
    }
    var data = Api_data.CoinMC_Global_OTC_LTCUSD(callback);

    //console.dir("Global_OTC_BTC/USD OK!");
};
module.exports.Global_LTC_EUR = function (req, res) {
    const callback = (result)=>{
        //return result
        result = Revision.GlobalLitecoinData(result,"2","EUR");
        res.send(result);
    };
    var data = Api_data.CoinMC_Global_OTC_LTCEUR(callback);
};
module.exports.Global_LTC_CNY = function (req, res) {
    const callback = (result)=>{
        //return result
        result = Revision.GlobalLitecoinData(result,"2","CNY");
        res.send(result);
    };
    var data = Api_data.CoinMC_Global_OTC_LTCCNY(callback);
    console.dir("Global_OTC_BTC/CNY OK!");
};
module.exports.Global_LTC_AUD = function (req, res) {
    const callback = (result)=>{
        //return result
        result = Revision.GlobalLitecoinData(result,"2","AUD");
        res.send(result);
    };
    var data = Api_data.CoinMC_Global_OTC_LTCAUD(callback);
    console.dir("Global_OTC_BTC/AUD OK!");
};
module.exports.Global_LTC_GBP = function (req, res) {
    const callback = (result)=>{
        //return result
        result = Revision.GlobalLitecoinData(result,"2","GBP");
        res.send(result);
    };
    var data = Api_data.CoinMC_Global_OTC_LTCGBP(callback);
    console.dir("Global_OTC_LTC/GBP OK!");
};
/*Global data EOS callback controller*/
module.exports.Global_EOS_USD = function (req, res) {
    const callback = (result)=>{
        //return result
        result = Revision.GlobalEOSData(result,"1765","USD")
        res.send(result);
    }
    var data = Api_data.CoinMC_Global_OTC_EOSUSD(callback);

    //console.dir("Global_OTC_BTC/USD OK!");
};
module.exports.Global_EOS_EUR = function (req, res) {
    const callback = (result)=>{
        //return result
        result = Revision.GlobalEOSData(result,"1765","EUR");
        res.send(result);
    };
    var data = Api_data.CoinMC_Global_OTC_EOSEUR(callback);
};
module.exports.Global_EOS_CNY = function (req, res) {
    const callback = (result)=>{
        //return result
        result = Revision.GlobalEOSData(result,"1765","CNY");
        res.send(result);
    };
    var data = Api_data.CoinMC_Global_OTC_EOSCNY(callback);
    console.dir("Global_OTC_BTC/CNY OK!");
};
module.exports.Global_EOS_AUD = function (req, res) {
    const callback = (result)=>{
        //return result
        result = Revision.GlobalEOSData(result,"1765","AUD");
        res.send(result);
    };
    var data = Api_data.CoinMC_Global_OTC_EOSAUD(callback);
    console.dir("Global_OTC_EOS/AUD OK!");
};
module.exports.Global_EOS_GBP = function (req, res) {
    const callback = (result)=>{
        //return result
        result = Revision.GlobalEOSData(result,"1765","GBP");
        res.send(result);
    };
    var data = Api_data.CoinMC_Global_OTC_EOSGBP(callback);
    console.dir("Global_OTC_EOS/GBP OK!");
};



/*Current Price*/
/*GDAX Current Price and Volume of BTC/USD*/
module.exports.GDAX_Current_Price = function (req, res) {
    const callback = (result) =>{
        result.price = parseFloat(result.price).toFixed(2)
        res.send(result);
    };
    Api_data.GDAX_Current_Price(callback);
    console.dir("GDAX_Current_Price OK!");
};
module.exports.GDAX_Current_Price_BTC_GBP = function (req, res) {
    const callback = (result) =>{
        result.price = parseFloat(result.price).toFixed(2)
        res.send(result);
    };
    Api_data.GDAX_Current_Price_GBP(callback);
    console.dir("GDAX_Current_Price OK!");
};
module.exports.GDAX_Current_Price_BTC_EUR = function (req, res) {
    const callback = (result) =>{
        result.price = parseFloat(result.price).toFixed(2)
        res.send(result);
    };
    Api_data.GDAX_Current_Price_BTC_EUR(callback);
    console.dir("GDAX_Current_Price OK!");
};
module.exports.GDAX_Current_Price_ETH = function (req, res) {
    const callback = (result) =>{
        result.price = parseFloat(result.price).toFixed(2)
        res.send(result);
    };
    Api_data.GDAX_Current_Price_ETH(callback);
    console.dir("GDAX_Current_Price OK!");
};
module.exports.GDAX_Current_Price_ETH_GBP = function (req, res) {
    const callback = (result) =>{
        result.price = parseFloat(result.price).toFixed(2)
        res.send(result);
    };
    Api_data.GDAX_Current_Price_GBP_ETH(callback);
    console.dir("GDAX_Current_Price OK!");
};
module.exports.GDAX_Current_Price_ETH_EUR = function (req, res) {
    const callback = (result) =>{
        result.price = parseFloat(result.price).toFixed(2)
        res.send(result);
    };
    Api_data.GDAX_Current_Price_ETH_EUR(callback);
    console.dir("GDAX_Current_Price OK!");
};
/*Bitfinex Current Price BTC/USD*/
module.exports.BitFinex_Current_Price_BTC_USD = function (req, res) {
    const callback = (result) =>{
        result = Revision.StringToArray(result)
        res.send(result);
    };
    Api_data.BitFinex_Current_BTCUSD(callback);
    console.dir("GDAX_Current_Price OK!");
};
module.exports.BitFinex_Current_Price_BTC_GBP = function (req, res) {
    const callback = (result) =>{
        result = Revision.StringToArray(result)
        res.send(result);
    };
    Api_data.BitFinex_Current_BTC_GBP(callback);
    console.dir("GDAX_Current_Price OK!");
};
module.exports.BitFinex_Current_Price_BTC_EUR = function (req, res) {
    const callback = (result) =>{
        result = Revision.StringToArray(result)
        res.send(result);
    };
    Api_data.BitFinex_Current_BTC_EUR(callback);
    console.dir("GDAX_Current_Price OK!");
};
module.exports.BitFinex_Current_Price_ETH_USD = function (req, res) {
    const callback = (result) =>{
        result = Revision.StringToArray(result)
        res.send(result);
    };
    Api_data.BitFinex_Current_ETHUSD(callback);
    console.dir("GDAX_Current_Price OK!");
};
module.exports.BitFinex_Current_Price_ETH_GBP = function (req, res) {
    const callback = (result) =>{
        result = Revision.StringToArray(result)
        res.send(result);
    };
    Api_data.BitFinex_Current_ETH_GBP(callback);
    console.dir("GDAX_Current_Price OK!");
};
module.exports.BitFinex_Current_Price_ETH_EUR = function (req, res) {
    const callback = (result) =>{
        result = Revision.StringToArray(result)
        res.send(result);
    };
    Api_data.BitFinex_Current_ETH_EUR(callback);
    console.dir("GDAX_Current_Price OK!");
};
/*Bitstamp Current Price BTC/USD*/

/*Kraken Current Price BTC/USD*/

/*24hr Volume*/
/*GDAX 24hr Volume*/
module.exports.Product_by_24H = function (req, res) {
    Api_data.Product_by_24H();
    console.dir("Product_by_24H----->>> OK!");
};

//candel data processer
module.exports.candelGDAX = function (req, res) {
    var realDate = new Date();
    var time = realDate.getTime();
    var date = time/1000
    date = Math.floor(date)
    // date = 999999999999999999;
    // console.log(date)
    Revision.selectLessThenDate(date,function (result) {
        // result.
        // console.log(result)
        res.json(result);

    })
};
module.exports.candelGDAXupdate = function (req, res) {
    // var realDate = new Date();
    // var time = realDate.getTime();
    // var date = time/1000
    // date = Math.floor(date)
    // console.log("aaa")
    var starttime = req.body.time;
    var endtime = parseInt(starttime)+60;
    // console.log(starttime,endtime)

    Revision.selectLessThenDateV2(endtime,function (result) {

        // console.log(result)
        res.json(result);

    })

};
module.exports.candelGDAX_GBP = function (req, res) {
    var realDate = new Date();
    var time = realDate.getTime();
    var date = time/1000
    date = Math.floor(date)
    Revision.selectLessThenDate_GBP(date,function (result) {
        res.json(result);
    })
};
module.exports.candelGDAXupdate_GBP = function (req, res) {
    // var realDate = new Date();
    // var time = realDate.getTime();
    // var date = time/1000
    // date = Math.floor(date)
    // console.log("aaa")
    var starttime = req.body.time;
    var endtime = parseInt(starttime)+60;
    // console.log(starttime,endtime)

    Revision.selectLessThenDateV2_GBP(endtime,function (result) {

        // console.log(result)
        res.json(result);

    })

};
module.exports.candelGDAX_BTC_UTC_API = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        var arraysort = []
        var j=0;
        for(var i = result.length-1;i>-1;i--){

                arraysort[j]=result[i];
                j++;

        }
        // console.log(arraysort)
        res.send(arraysort);
    }
    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();
    var mins = realDate.getMinutes();
    var startforBTCGDAX = year + '-' + month + '-' + day + ' ' + hour + ':'+mins +':00';
    // console.log(startforBTCGDAX)
    startforBTCGDAX = new Date(startforBTCGDAX)
    var nomalend = startforBTCGDAX;
    startforBTCGDAX = Date.parse(startforBTCGDAX)
    startforBTCGDAX = startforBTCGDAX - 18000000;
    var nomalstart = new Date(startforBTCGDAX);

    Api_data.GDAX_Candle_Data_API(nomalstart,nomalend,callback);

};
module.exports.candelGDAX_BTC_UTC_API_Update = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        res.send(result);
    }
    var starttime = req.body.time;
    var endtime = parseInt(starttime)+60;
    starttime = new Date(starttime*1000)
    endtime =  new Date(endtime*1000)
    console.log(starttime,endtime)
    // var realDate = new Date();
    // var year = realDate.getFullYear();
    // var month = realDate.getMonth() + 1;
    // var day = realDate.getDate();
    // var hour = realDate.getHours();
    // var mins = realDate.getMinutes();
    // var startforBTCGDAX = year + '-' + month + '-' + day + ' ' + hour + ':'+mins +':00';
    // startforBTCGDAX = new Date(startforBTCGDAX)
    // var nomalend = startforBTCGDAX;
    // startforBTCGDAX = Date.parse(startforBTCGDAX)
    // startforBTCGDAX = startforBTCGDAX - 86400000;
    // var nomalstart = new Date(startforBTCGDAX);

    Api_data.GDAX_Candle_Data_API(starttime,endtime,callback);

};
module.exports.candelGDAX_BTC_GBP_API = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        var arraysort = []
        var j=0;
        for(var i = result.length-1;i>-1;i--){

            arraysort[j]=result[i];
            j++;

        }
        // console.log(arraysort)
        res.send(arraysort);
    }
    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();
    var mins = realDate.getMinutes();
    var startforBTCGDAX = year + '-' + month + '-' + day + ' ' + hour + ':'+mins +':00';
    // console.log(startforBTCGDAX)
    startforBTCGDAX = new Date(startforBTCGDAX)
    var nomalend = startforBTCGDAX;
    startforBTCGDAX = Date.parse(startforBTCGDAX)
    startforBTCGDAX = startforBTCGDAX - 18000000;
    var nomalstart = new Date(startforBTCGDAX);

    Api_data.GDAX_Candle_Data_GBP_API(nomalstart,nomalend,callback);

};
module.exports.candelGDAX_BTC_GBP_API_Update = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        res.send(result);
    }
    var starttime = req.body.time;
    var endtime = parseInt(starttime)+60;
    starttime = new Date(starttime*1000)
    endtime =  new Date(endtime*1000)
    console.log(starttime,endtime)
    // var realDate = new Date();
    // var year = realDate.getFullYear();
    // var month = realDate.getMonth() + 1;
    // var day = realDate.getDate();
    // var hour = realDate.getHours();
    // var mins = realDate.getMinutes();
    // var startforBTCGDAX = year + '-' + month + '-' + day + ' ' + hour + ':'+mins +':00';
    // startforBTCGDAX = new Date(startforBTCGDAX)
    // var nomalend = startforBTCGDAX;
    // startforBTCGDAX = Date.parse(startforBTCGDAX)
    // startforBTCGDAX = startforBTCGDAX - 86400000;
    // var nomalstart = new Date(startforBTCGDAX);

    Api_data.GDAX_Candle_Data_GBP_API(starttime,endtime,callback);

};
module.exports.candelGDAX_BTC_EUR_API = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        var arraysort = []
        var j=0;
        for(var i = result.length-1;i>-1;i--){

            arraysort[j]=result[i];
            j++;

        }
        // console.log(arraysort)
        res.send(arraysort);
    }
    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();
    var mins = realDate.getMinutes();
    var startforBTCGDAX = year + '-' + month + '-' + day + ' ' + hour + ':'+mins +':00';
    // console.log(startforBTCGDAX)
    startforBTCGDAX = new Date(startforBTCGDAX)
    var nomalend = startforBTCGDAX;
    startforBTCGDAX = Date.parse(startforBTCGDAX)
    startforBTCGDAX = startforBTCGDAX - 18000000;
    var nomalstart = new Date(startforBTCGDAX);

    Api_data.GDAX_Candle_Data_BTC_EUR_API(nomalstart,nomalend,callback);

};
module.exports.candelGDAX_BTC_EUR_API_Update = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        res.send(result);
    }
    var starttime = req.body.time;
    var endtime = parseInt(starttime)+60;
    starttime = new Date(starttime*1000)
    endtime =  new Date(endtime*1000)
    console.log(starttime,endtime)
    // var realDate = new Date();
    // var year = realDate.getFullYear();
    // var month = realDate.getMonth() + 1;
    // var day = realDate.getDate();
    // var hour = realDate.getHours();
    // var mins = realDate.getMinutes();
    // var startforBTCGDAX = year + '-' + month + '-' + day + ' ' + hour + ':'+mins +':00';
    // startforBTCGDAX = new Date(startforBTCGDAX)
    // var nomalend = startforBTCGDAX;
    // startforBTCGDAX = Date.parse(startforBTCGDAX)
    // startforBTCGDAX = startforBTCGDAX - 86400000;
    // var nomalstart = new Date(startforBTCGDAX);

    Api_data.GDAX_Candle_Data_BTC_EUR_API(starttime,endtime,callback);

};

module.exports.candelGDAX_ETH_UTC_API = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        var arraysort = []
        var j=0;
        for(var i = result.length-1;i>-1;i--){

            arraysort[j]=result[i];
            j++;

        }
        // console.log(arraysort)
        res.send(arraysort);
    }
    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();
    var mins = realDate.getMinutes();
    var startforBTCGDAX = year + '-' + month + '-' + day + ' ' + hour + ':'+mins +':00';
    // console.log(startforBTCGDAX)
    startforBTCGDAX = new Date(startforBTCGDAX)
    var nomalend = startforBTCGDAX;
    startforBTCGDAX = Date.parse(startforBTCGDAX)
    startforBTCGDAX = startforBTCGDAX - 18000000;
    var nomalstart = new Date(startforBTCGDAX);

    Api_data.GDAX_Candle_Data_ETH_USD_API(nomalstart,nomalend,callback);

};
module.exports.candelGDAX_ETH_UTC_API_Update = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        res.send(result);
    }
    var starttime = req.body.time;
    var endtime = parseInt(starttime)+60;
    starttime = new Date(starttime*1000)
    endtime =  new Date(endtime*1000)
    console.log(starttime,endtime)

    Api_data.GDAX_Candle_Data_ETH_USD_API(starttime,endtime,callback);

};
module.exports.candelGDAX_ETH_GBP_API = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        var arraysort = []
        var j=0;
        for(var i = result.length-1;i>-1;i--){

            arraysort[j]=result[i];
            j++;

        }
        // console.log(arraysort)
        res.send(arraysort);
    }
    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();
    var mins = realDate.getMinutes();
    var startforBTCGDAX = year + '-' + month + '-' + day + ' ' + hour + ':'+mins +':00';
    // console.log(startforBTCGDAX)
    startforBTCGDAX = new Date(startforBTCGDAX)
    var nomalend = startforBTCGDAX;
    startforBTCGDAX = Date.parse(startforBTCGDAX)
    startforBTCGDAX = startforBTCGDAX - 18000000;
    var nomalstart = new Date(startforBTCGDAX);

    Api_data.GDAX_Candle_Data_ETH_GBP_API(nomalstart,nomalend,callback);

};
module.exports.candelGDAX_ETH_GBP_API_Update = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        res.send(result);
    }
    var starttime = req.body.time;
    var endtime = parseInt(starttime)+60;
    starttime = new Date(starttime*1000)
    endtime =  new Date(endtime*1000)
    console.log(starttime,endtime)
    // var realDate = new Date();
    // var year = realDate.getFullYear();
    // var month = realDate.getMonth() + 1;
    // var day = realDate.getDate();
    // var hour = realDate.getHours();
    // var mins = realDate.getMinutes();
    // var startforBTCGDAX = year + '-' + month + '-' + day + ' ' + hour + ':'+mins +':00';
    // startforBTCGDAX = new Date(startforBTCGDAX)
    // var nomalend = startforBTCGDAX;
    // startforBTCGDAX = Date.parse(startforBTCGDAX)
    // startforBTCGDAX = startforBTCGDAX - 86400000;
    // var nomalstart = new Date(startforBTCGDAX);

    Api_data.GDAX_Candle_Data_ETH_GBP_API(starttime,endtime,callback);

};
module.exports.candelGDAX_ETH_EUR_API = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        var arraysort = []
        var j=0;
        for(var i = result.length-1;i>-1;i--){

            arraysort[j]=result[i];
            j++;

        }
        // console.log(arraysort)
        res.send(arraysort);
    }
    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();
    var mins = realDate.getMinutes();
    var startforBTCGDAX = year + '-' + month + '-' + day + ' ' + hour + ':'+mins +':00';
    // console.log(startforBTCGDAX)
    startforBTCGDAX = new Date(startforBTCGDAX)
    var nomalend = startforBTCGDAX;
    startforBTCGDAX = Date.parse(startforBTCGDAX)
    startforBTCGDAX = startforBTCGDAX - 18000000;
    var nomalstart = new Date(startforBTCGDAX);

    Api_data.GDAX_Candle_Data_ETH_EUR_API(nomalstart,nomalend,callback);

};
module.exports.candelGDAX_ETH_EUR_API_Update = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        res.send(result);
    }
    var starttime = req.body.time;
    var endtime = parseInt(starttime)+60;
    starttime = new Date(starttime*1000)
    endtime =  new Date(endtime*1000)
    console.log(starttime,endtime)
    // var realDate = new Date();
    // var year = realDate.getFullYear();
    // var month = realDate.getMonth() + 1;
    // var day = realDate.getDate();
    // var hour = realDate.getHours();
    // var mins = realDate.getMinutes();
    // var startforBTCGDAX = year + '-' + month + '-' + day + ' ' + hour + ':'+mins +':00';
    // startforBTCGDAX = new Date(startforBTCGDAX)
    // var nomalend = startforBTCGDAX;
    // startforBTCGDAX = Date.parse(startforBTCGDAX)
    // startforBTCGDAX = startforBTCGDAX - 86400000;
    // var nomalstart = new Date(startforBTCGDAX);

    Api_data.GDAX_Candle_Data_ETH_EUR_API(starttime,endtime,callback);

};

module.exports.candleBitfinex = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        //return result
        result = Revision.ChangeToTwoDArray(result)
        result = Revision.TwoDArrayToJsonArrayForBitfinex(result)
        // console.log(result)
        res.send(result);
    }
    Api_data.BitFinex_Candle_Data(callback);
};
module.exports.candleBitfinexGBP = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        //return result
        result = Revision.ChangeToTwoDArray(result)
        result = Revision.TwoDArrayToJsonArrayForBitfinex(result)
        // console.log(result)
        res.send(result);
    }
    Api_data.BitFinex_Candle_Data_GBP(callback);
};
module.exports.candleBitfinex_BTC_EUR = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        //return result
        result = Revision.ChangeToTwoDArray(result)
        result = Revision.TwoDArrayToJsonArrayForBitfinex(result)
        // console.log(result)
        res.send(result);
    }
    Api_data.BitFinex_Candle_Data_BTC_EUR(callback);
};
module.exports.candleBitfinex_ETH_USD = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        //return result
        result = Revision.ChangeToTwoDArray(result)
        result = Revision.TwoDArrayToJsonArrayForBitfinex(result)
        // console.log(result)
        res.send(result);
    }
    Api_data.BitFinex_Candle_Data_ETH_USD(callback);
};
module.exports.candleBitfinex_ETH_GBP = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        //return result
        result = Revision.ChangeToTwoDArray(result)
        result = Revision.TwoDArrayToJsonArrayForBitfinex(result)
        // console.log(result)
        res.send(result);
    }
    Api_data.BitFinex_Candle_Data_ETH_GBP(callback);
};
module.exports.candleBitfinex_ETH_EUR = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        //return result
        result = Revision.ChangeToTwoDArray(result)
        result = Revision.TwoDArrayToJsonArrayForBitfinex(result)
        // console.log(result)
        res.send(result);
    }
    Api_data.BitFinex_Candle_Data_ETH_EUR(callback);
};

//card volume and price
module.exports.cardGDAXVolPrice = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        res.send(result);
    }
    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();
    var startforBTCGDAX = year + '-' + month + '-' + day + ' ' + hour + ':00:00';
    startforBTCGDAX = new Date(startforBTCGDAX)
    var nomalend = startforBTCGDAX;
    // console.log(nomalend);
    startforBTCGDAX = Date.parse(startforBTCGDAX)
    startforBTCGDAX = startforBTCGDAX - 86400000;
    var nomalstart = new Date(startforBTCGDAX);
    // console.log(nomalstart);

    Api_data.GDAX_Vol_Price_Data(nomalstart,nomalend,callback);
};
module.exports.cardGDAXGBPVolPrice = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        res.send(result);
    }
    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();
    var startforBTCGDAX = year + '-' + month + '-' + day + ' ' + hour + ':00:00';
    startforBTCGDAX = new Date(startforBTCGDAX)
    var nomalend = startforBTCGDAX;
    // console.log(nomalend);
    startforBTCGDAX = Date.parse(startforBTCGDAX)
    startforBTCGDAX = startforBTCGDAX - 86400000;
    var nomalstart = new Date(startforBTCGDAX);
    // console.log(nomalstart);

    Api_data.GDAX_Vol_Price_GBP_Data(nomalstart,nomalend,callback);
};
module.exports.cardGDAXEURVolPriceBTC = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        res.send(result);
    }
    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();
    var startforBTCGDAX = year + '-' + month + '-' + day + ' ' + hour + ':00:00';
    startforBTCGDAX = new Date(startforBTCGDAX)
    var nomalend = startforBTCGDAX;
    // console.log(nomalend);
    startforBTCGDAX = Date.parse(startforBTCGDAX)
    startforBTCGDAX = startforBTCGDAX - 86400000;
    var nomalstart = new Date(startforBTCGDAX);
    // console.log(nomalstart);

    Api_data.GDAX_Vol_Price_EUR_Data_BTC(nomalstart,nomalend,callback);
};
module.exports.cardBitfinexVolPrice = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        //return result
        result = Revision.ChangeToTwoDArray(result)
        result = Revision.TwoDArrayToJsonArrayForBitfinex(result)
        // console.log(result)
        res.send(result);
    }
    Api_data.BitFinex_Vol_Price_Data(callback);
};
module.exports.cardBitfinexGBPVolPrice = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        //return result
        result = Revision.ChangeToTwoDArray(result)
        result = Revision.TwoDArrayToJsonArrayForBitfinex(result)
        // console.log(result)
        res.send(result);
    }
    Api_data.BitFinex_Vol_Price_GBP_Data(callback);
};
module.exports.cardBitfinexEURVolPriceBTC = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        //return result
        result = Revision.ChangeToTwoDArray(result)
        result = Revision.TwoDArrayToJsonArrayForBitfinex(result)
        // console.log(result)
        res.send(result);
    }
    Api_data.BitFinex_Vol_Price_EUR_Data_BTC(callback);
};
module.exports.cardGDAXVolPriceETH = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        res.send(result);
    }
    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();
    var startforETHGDAX = year + '-' + month + '-' + day + ' ' + hour + ':00:00';
    startforETHGDAX = new Date(startforETHGDAX)
    var nomalend = startforETHGDAX;
    // console.log(nomalend);
    startforETHGDAX = Date.parse(startforETHGDAX)
    startforETHGDAX = startforETHGDAX - 86400000;
    var nomalstart = new Date(startforETHGDAX);
    // console.log(nomalstart);

    Api_data.GDAX_Vol_Price_Data_ETH(nomalstart,nomalend,callback);
};
module.exports.cardGDAXGBPVolPriceETH = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        res.send(result);
    }
    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();
    var startforETHGDAX = year + '-' + month + '-' + day + ' ' + hour + ':00:00';
    startforETHGDAX = new Date(startforETHGDAX)
    var nomalend = startforETHGDAX;
    // console.log(nomalend);
    startforETHGDAX = Date.parse(startforETHGDAX)
    startforETHGDAX = startforETHGDAX - 86400000;
    var nomalstart = new Date(startforETHGDAX);
    // console.log(nomalstart);

    Api_data.GDAX_Vol_Price_GBP_Data_ETH(nomalstart,nomalend,callback);
};
module.exports.cardGDAXEURVolPriceETH = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        res.send(result);
    }
    var realDate = new Date();
    var year = realDate.getFullYear();
    var month = realDate.getMonth() + 1;
    var day = realDate.getDate();
    var hour = realDate.getHours();
    var startforETHGDAX = year + '-' + month + '-' + day + ' ' + hour + ':00:00';
    startforETHGDAX = new Date(startforETHGDAX)
    var nomalend = startforETHGDAX;
    // console.log(nomalend);
    startforETHGDAX = Date.parse(startforETHGDAX)
    startforETHGDAX = startforETHGDAX - 86400000;
    var nomalstart = new Date(startforETHGDAX);
    // console.log(nomalstart);

    Api_data.GDAX_Vol_Price_EUR_Data_ETH(nomalstart,nomalend,callback);
};
module.exports.cardBitfinexVolPriceETH = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        //return result
        result = Revision.ChangeToTwoDArray(result)
        result = Revision.TwoDArrayToJsonArrayForBitfinex(result)
        // console.log(result)
        res.send(result);
    }
    Api_data.BitFinex_Vol_Price_Data_ETH(callback);
};
module.exports.cardBitfinexGBPVolPriceETH = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        //return result
        result = Revision.ChangeToTwoDArray(result)
        result = Revision.TwoDArrayToJsonArrayForBitfinex(result)
        // console.log(result)
        res.send(result);
    }
    Api_data.BitFinex_Vol_Price_GBP_Data_ETH(callback);
};
module.exports.cardBitfinexEURVolPriceETH = function (req, res) {
    const callback = (result)=>{
        // console.log(result)
        //return result
        result = Revision.ChangeToTwoDArray(result)
        result = Revision.TwoDArrayToJsonArrayForBitfinex(result)
        // console.log(result)
        res.send(result);
    }
    Api_data.BitFinex_Vol_Price_EUR_Data_ETH(callback);
};

//User Function
module.exports.UserRegister = function (req, res) {
    var revisions = 'User';
    var data = req.body;
    console.dir(data)
    const callback = (result)=>{
        // console.log(result)
    }
    Revision.insertSingleData(mongoConnectUrl,revisions,data,callback);

    var result = {
        result:"Success"
    }
    res.json(result)
}

//LoginResult = {}
module.exports.UserLogin = function (req, res) {

    var data = req.body;
    // console.log(data)

    // callback = (result)=>{
    //     console.log(result)
    //     // LoginResult = result;
    //     res.send(result)
    // }
    Revision.selectUserData(data, function (result) {
        // var globalresult = Revision.JsonArrayForDepth(result);

        // var json = {result: result.result};
        // LoginResult = result;
        console.log(result);
        res.json(result);
    });
}

module.exports.UserInsert = function (req, res) {
    console.dir(req.body)
    var bbb = {
        AAA:"aaa"
    }
    // console.dir(bbb.AAA)
    res.json(bbb)
}

module.exports.Product_Order_BidAsk=function (req, res) {
    const callback = (error, response, data) =>{
        if (error)
            return console.dir(error);
        console.dir(data);


    };
    Api_data.Product_Order_BidAsk(callback);
    console.dir("Product_Order_BidAsk OK!");
};


//Depth Bitfinex
module.exports.BitFinex_Depth_USD = function (req, res) {
    const callback = (body) =>{

        body = Revision.DepthForBitfinex(body)
        res.send(body)

    };

    Api_data.BitFinex_Orderbook_Depth_BTCUSD(callback);
}
module.exports.BitFinex_Depth_GBP = function (req, res) {
    const callback = (body) =>{

        body = Revision.DepthForBitfinex(body)
        res.send(body)

    };

    Api_data.BitFinex_Orderbook_Depth_BTCGBP(callback);


}
module.exports.BitFinex_Depth_BTC_EUR = function (req, res) {
    const callback = (body) =>{

        body = Revision.DepthForBitfinex(body)
        res.send(body)

    };

    Api_data.BitFinex_Orderbook_Depth_BTCEUR(callback);
}
module.exports.BitFinex_Depth_ETH_USD = function (req, res) {
    const callback = (body) =>{

        body = Revision.DepthForBitfinex(body)
        res.send(body)

    };

    Api_data.BitFinex_Orderbook_Depth_ETHUSD(callback);
}
module.exports.BitFinex_Depth_ETH_GBP = function (req, res) {
    const callback = (body) =>{

        body = Revision.DepthForBitfinex(body)
        res.send(body)

    };

    Api_data.BitFinex_Orderbook_Depth_ETHGBP(callback);


}
module.exports.BitFinex_Depth_ETH_EUR = function (req, res) {
    const callback = (body) =>{

        body = Revision.DepthForBitfinex(body)
        res.send(body)

    };

    Api_data.BitFinex_Orderbook_Depth_ETHEUR(callback);


}


var express = require('express');
var controller = require('../controllers/revision.server.controller')
var Revision = require('../controllers/models/revision')

var router = express.Router();

router.get('/',controller.showMain)
router.get('/catchData',controller.catchData)
router.get('/catchRealtimeData',controller.catchRealtimeData)
router.get('/catchRealtimeData_GBP',controller.catchRealtimeDataGroup)
router.get('/catchGlobalData',controller.Realtime_Global_Data)

router.get('/Candle_Stick_Data',controller.Candle_Stick_Data)
router.get('/index', controller.showIndex)
router.get('/depth', controller.depth)

router.get("/login",controller.login);
router.get("/reg",controller.reg);

router.get("/profile",controller.profile);
router.get("/forgotpwd",controller.forgotpwd);
router.get("/lock",controller.lock);

//Global parts
router.get('/GlobalUSD', controller.GlobalUSD)
router.get('/GlobalEUR', controller.GlobalEUR)
router.get('/GlobalAUD', controller.GlobalAUD)
router.get('/GlobalCNY', controller.GlobalCNY)
router.get('/GlobalGBP', controller.GlobalGBP)

//Global ETH parts
router.get('/Global_ETH_USD', controller.Global_ETH_USD);
router.get('/Global_ETH_EUR', controller.Global_ETH_EUR);
router.get('/Global_ETH_AUD', controller.Global_ETH_AUD);
router.get('/Global_ETH_CNY', controller.Global_ETH_CNY);
router.get('/Global_ETH_GBP', controller.Global_ETH_GBP);
//Global LTC parts;
router.get('/Global_LTC_USD', controller.Global_LTC_USD);
router.get('/Global_LTC_EUR', controller.Global_LTC_EUR);
router.get('/Global_LTC_CNY', controller.Global_LTC_CNY);
router.get('/Global_LTC_AUD', controller.Global_LTC_AUD);
router.get('/Global_LTC_GBP', controller.Global_LTC_GBP);
//Global EOS parts
router.get('/Global_EOS_USD', controller.Global_EOS_USD);
router.get('/Global_EOS_EUR', controller.Global_EOS_EUR);
router.get('/Global_EOS_CNY', controller.Global_EOS_CNY);
router.get('/Global_EOS_AUD', controller.Global_EOS_AUD);
router.get('/Global_EOS_GBP', controller.Global_EOS_GBP);


router.get('/Product_Order_BidAsk',controller.Product_Order_BidAsk)



//revision data processor test
router.get('/MapData', Revision.MapData)
router.get('/GlobalData', Revision.GlobalBitcoinData)
router.post('/FBtest',controller.UserInsert)

module.exports= router

/*GDAX status*/
router.get('/GDAX_Current_Price',controller.GDAX_Current_Price);
router.get('/Product_by_24H',controller.Product_by_24H);

/*Bitstamp status*/
router.get('/Candle_Stick_Data_Bitstamp',controller.Candle_Stick_Data_Bitstamp);
router.get('/Bitstamp_Trade_wsTest',controller.Bitstamp_Trade_wsTest);
router.get('/Bitstamp_Current_Price',controller.Bitstamp_Currency_Price);
router.get('/Bitstamp_Trade_History',controller.Bitstamp_Trade_History);

/*BitFinex status*/
router.get('/BitFinex_Platform_Status',controller.BitFinex_Platform_Status);
router.get('/BitFinex_Candle_Data',controller.BitFinex_Candle_Data);



/*Binance Status*/
router.get('/Binance_Current_Price_USDT',controller.Binance_Current_Price_USDT);
router.get('/Binance_Candle_BNB',controller.Binance_Candle_BNB);
router.get('/Binance_Candle_wsBNB',controller.Binance_Candle_wsBNB);
router.get('/Binance_Candle_wsBNB_realTime',controller.Binance_Candle_wsBNB_realTime);
router.get('/Binance_24hr_BNBchange',controller.Binance_24hr_BNBchange);
router.get('/Binance_24hr_wsBNBchange',controller.Binance_24hr_wsBNBchange);
router.get('/Binance_Depth_wsBNB',controller.Binance_Depth_wsBNB);

/*Kraken Status*/


/*CCXT Kraken Status*/
router.get('/CCXT_Kraken_TickerBTCUSD',controller.CCXT_Kraken_TickerBTCUSD);


/*Bitnodes World Map*/
router.get('/Bitnodes_World_Map_Data',controller.Bitnodes_World_Map_Data);

/*CoinMarketCap Global Status*/
router.get('/CoinMC_Global_OTC_USD',controller.CoinMC_Global_OTC_USD);
router.get('/CoinMC_Global_OTC_EUR',controller.CoinMC_Global_OTC_EUR);
router.get('/CoinMC_Global_OTC_CNY',controller.CoinMC_Global_OTC_CNY);
router.get('/CoinMC_Global_OTC_AUD',controller.CoinMC_Global_OTC_AUD);

//candel data
router.post('/candelGDAX',controller.candelGDAX);
router.post('/candelGDAXupdate',controller.candelGDAXupdate);
router.post('/candelGDAX_GBP',controller.candelGDAX_GBP);
router.post('/candelGDAXupdate_GBP',controller.candelGDAXupdate_GBP);

router.post('/candleBitfinex',controller.candleBitfinex);
router.post('/candleBitfinexGBP',controller.candleBitfinexGBP);
router.post('/candleBitfinex_BTC_EUR',controller.candleBitfinex_BTC_EUR);
router.post('/candleBitfinex_ETH_USD',controller.candleBitfinex_ETH_USD);
router.post('/candleBitfinex_ETH_GBP',controller.candleBitfinex_ETH_GBP);
router.post('/candleBitfinex_ETH_EUR',controller.candleBitfinex_ETH_EUR);

router.post('/candelGDAX_BTC_UTC_API',controller.candelGDAX_BTC_UTC_API);
router.post('/candelGDAX_BTC_UTC_API_Update',controller.candelGDAX_BTC_UTC_API_Update);
router.post('/candelGDAX_BTC_GBP_API',controller.candelGDAX_BTC_GBP_API);
router.post('/candelGDAX_BTC_GBP_API_Update',controller.candelGDAX_BTC_GBP_API_Update);
router.post('/candelGDAX_BTC_EUR_API',controller.candelGDAX_BTC_EUR_API);
router.post('/candelGDAX_BTC_EUR_API_Update',controller.candelGDAX_BTC_EUR_API_Update);

router.post('/candelGDAX_ETH_UTC_API',controller.candelGDAX_ETH_UTC_API);
router.post('/candelGDAX_ETH_UTC_API_Update',controller.candelGDAX_ETH_UTC_API_Update);
router.post('/candelGDAX_ETH_GBP_API',controller.candelGDAX_ETH_GBP_API);
router.post('/candelGDAX_ETH_GBP_API_Update',controller.candelGDAX_ETH_GBP_API_Update);
router.post('/candelGDAX_ETH_EUR_API',controller.candelGDAX_ETH_EUR_API);
router.post('/candelGDAX_ETH_EUR_API_Update',controller.candelGDAX_ETH_EUR_API_Update);

//second card currency price and volume
router.get('/GDAX_CurrencyPrice&Vol_BTC_USD',controller.GDAX_Current_Price);
router.get('/GDAX_CurrencyPrice&Vol_BTC_GBP',controller.GDAX_Current_Price_BTC_GBP);
router.get('/GDAX_CurrencyPrice&Vol_BTC_EUR',controller.GDAX_Current_Price_BTC_EUR);
router.get('/BitFinex_CurrencyPrice&Vol_BTC_USD',controller.BitFinex_Current_Price_BTC_USD);
router.get('/BitFinex_CurrencyPrice&Vol_BTC_GBP',controller.BitFinex_Current_Price_BTC_GBP);
router.get('/BitFinex_CurrencyPrice&Vol_BTC_EUR',controller.BitFinex_Current_Price_BTC_EUR);

router.get('/GDAX_CurrencyPrice&Vol_ETH_USD',controller.GDAX_Current_Price_ETH);
router.get('/GDAX_CurrencyPrice&Vol_ETH_GBP',controller.GDAX_Current_Price_ETH_GBP);
router.get('/GDAX_CurrencyPrice&Vol_ETH_EUR',controller.GDAX_Current_Price_ETH_EUR);
router.get('/BitFinex_CurrencyPrice&Vol_ETH_USD',controller.BitFinex_Current_Price_ETH_USD);
router.get('/BitFinex_CurrencyPrice&Vol_ETH_GBP',controller.BitFinex_Current_Price_ETH_GBP);
router.get('/BitFinex_CurrencyPrice&Vol_ETH_EUR',controller.BitFinex_Current_Price_ETH_EUR);

//card data
router.post('/cardGDAXVolPrice',controller.cardGDAXVolPrice);
router.post('/cardGDAXGBPVolPrice',controller.cardGDAXGBPVolPrice);
router.post('/cardGDAXEURVolPriceBTC',controller.cardGDAXEURVolPriceBTC);
router.post('/cardBitfinexVolPrice',controller.cardBitfinexVolPrice);
router.post('/cardBitfinexGBPVolPrice',controller.cardBitfinexGBPVolPrice);
router.post('/cardBitfinexEURVolPriceBTC',controller.cardBitfinexEURVolPriceBTC);

router.post('/cardGDAXVolPriceETH',controller.cardGDAXVolPriceETH);
router.post('/cardGDAXGBPVolPriceETH',controller.cardGDAXGBPVolPriceETH);
router.post('/cardGDAXEURVolPriceETH',controller.cardGDAXEURVolPriceETH);
router.post('/cardBitfinexVolPriceETH',controller.cardBitfinexVolPriceETH);
router.post('/cardBitfinexGBPVolPriceETH',controller.cardBitfinexGBPVolPriceETH);
router.post('/cardBitfinexEURVolPriceETH',controller.cardBitfinexEURVolPriceETH);

//user function
router.post('/UserRegister',controller.UserRegister)
router.post('/UserLogin',controller.UserLogin)

//Bitfinex Depth
router.get('/BitfinexDepthUSD',controller.BitFinex_Depth_USD)
router.get('/BitfinexDepthGBP',controller.BitFinex_Depth_GBP)
router.get('/BitfinexDepth_BTC_EUR',controller.BitFinex_Depth_BTC_EUR)
router.get('/BitfinexDepth_ETH_USD',controller.BitFinex_Depth_ETH_USD)
router.get('/BitfinexDepth_ETH_GBP',controller.BitFinex_Depth_ETH_GBP)
router.get('/BitfinexDepth_ETH_EUR',controller.BitFinex_Depth_ETH_EUR)

module.exports= router;
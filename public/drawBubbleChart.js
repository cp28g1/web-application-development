function drawBubbleChart01(){

    // require.config({
    //     paths:{
    //         echarts: '/echarts.js'
    //     }
    // });
    //
    // require(
    //     [
    //         'echarts',
    //         'echarts/chart/bubble'
    //     ],
    //     function (ec) {
    //         var data = [
    //             [[28604,77,17096869,'Australia',1990],
    //                 [31163,77.4,27662440,'Canada',1990],
    //                 [1516,68,1154605773,'China',1990],
    //                 [13670,74.7,10582082,'Cuba',1990],
    //                 [28599,75,4986705,'Finland',1990],
    //                 [29476,77.1,56943299,'France',1990],
    //                 [31476,75.4,78958237,'Germany',1990],
    //                 [28666,78.1,254830,'Iceland',1990],
    //                 [1777,57.7,870601776,'India',1990],
    //                 [29550,79.1,122249285,'Japan',1990],
    //                 [2076,67.9,20194354,'North Korea',1990],
    //                 [12087,72,42972254,'South Korea',1990],
    //                 [24021,75.4,3397534,'New Zealand',1990],
    //                 [43296,76.8,4240375,'Norway',1990],
    //                 [10088,70.8,38195258,'Poland',1990],
    //                 [19349,69.6,147568552,'Russia',1990],
    //                 [10670,67.3,53994605,'Turkey',1990],
    //                 [26424,75.7,57110117,'United Kingdom',1990],
    //                 [37062,75.4,252847810,'United States',1990]],
    //             [[44056,81.8,23968973,'Australia',2015],
    //                 [43294,81.7,35939927,'Canada',2015],
    //                 [13334,76.9,1376048943,'China',2015],
    //                 [21291,78.5,11389562,'Cuba',2015],
    //                 [38923,80.8,5503457,'Finland',2015],
    //                 [37599,81.9,64395345,'France',2015],
    //                 [44053,81.1,80688545,'Germany',2015],
    //                 [42182,82.8,329425,'Iceland',2015],
    //                 [5903,66.8,1311050527,'India',2015],
    //                 [36162,83.5,126573481,'Japan',2015],
    //                 [1390,71.4,25155317,'North Korea',2015],
    //                 [34644,80.7,50293439,'South Korea',2015],
    //                 [34186,80.6,4528526,'New Zealand',2015],
    //                 [64304,81.6,5210967,'Norway',2015],
    //                 [24787,77.3,38611794,'Poland',2015],
    //                 [23038,73.13,143456918,'Russia',2015],
    //                 [19360,76.5,78665830,'Turkey',2015],
    //                 [38225,81.4,64715810,'United Kingdom',2015],
    //                 [53354,79.1,321773631,'United States',2015]]
    //         ];
    //
    //         var myChart = ec.init(document.getElementById('modal01content'));
    //
    //         option = {
    //             backgroundColor: new echarts.graphic.RadialGradient(0.3, 0.3, 0.8, [{
    //                 offset: 0,
    //                 color: '#f7f8fa'
    //             }, {
    //                 offset: 1,
    //                 color: '#cdd0d5'
    //             }]),
    //             title: {
    //                 text: '1990 与 2015 年各国家人均寿命与 GDP'
    //             },
    //             legend: {
    //                 right: 10,
    //                 data: ['1990', '2015']
    //             },
    //             xAxis: {
    //                 splitLine: {
    //                     lineStyle: {
    //                         type: 'dashed'
    //                     }
    //                 }
    //             },
    //             yAxis: {
    //                 splitLine: {
    //                     lineStyle: {
    //                         type: 'dashed'
    //                     }
    //                 },
    //                 scale: true
    //             },
    //             series: [{
    //                 name: '1990',
    //                 data: data[0],
    //                 type: 'scatter',
    //                 symbolSize: function (data) {
    //                     return Math.sqrt(data[2]) / 5e2;
    //                 },
    //                 label: {
    //                     emphasis: {
    //                         show: true,
    //                         formatter: function (param) {
    //                             return param.data[3];
    //                         },
    //                         position: 'top'
    //                     }
    //                 },
    //                 itemStyle: {
    //                     normal: {
    //                         shadowBlur: 10,
    //                         shadowColor: 'rgba(120, 36, 50, 0.5)',
    //                         shadowOffsetY: 5,
    //                         color: new echarts.graphic.RadialGradient(0.4, 0.3, 1, [{
    //                             offset: 0,
    //                             color: 'rgb(251, 118, 123)'
    //                         }, {
    //                             offset: 1,
    //                             color: 'rgb(204, 46, 72)'
    //                         }])
    //                     }
    //                 }
    //             }, {
    //                 name: '2015',
    //                 data: data[1],
    //                 type: 'scatter',
    //                 symbolSize: function (data) {
    //                     return Math.sqrt(data[2]) / 5e2;
    //                 },
    //                 label: {
    //                     emphasis: {
    //                         show: true,
    //                         formatter: function (param) {
    //                             return param.data[3];
    //                         },
    //                         position: 'top'
    //                     }
    //                 },
    //                 itemStyle: {
    //                     normal: {
    //                         shadowBlur: 10,
    //                         shadowColor: 'rgba(25, 100, 150, 0.5)',
    //                         shadowOffsetY: 5,
    //                         color: new echarts.graphic.RadialGradient(0.4, 0.3, 1, [{
    //                             offset: 0,
    //                             color: 'rgb(129, 227, 238)'
    //                         }, {
    //                             offset: 1,
    //                             color: 'rgb(25, 183, 207)'
    //                         }])
    //                     }
    //                 }
    //             }]
    //         };
    //         myChart.setOption(option);
    //     }
    // );

    var chart = AmCharts.makeChart("modal01content", {
        "type": "xy",
        "theme": "light",
        "marginRight": 80,
        "dataDateFormat": "YYYY-MM-DD",
        "startDuration": 1.5,
        "trendLines": [],
        "balloon": {
            "adjustBorderColor": false,
            "shadowAlpha": 0,
            "fixedPosition":true
        },
        "graphs": [{
            "balloonText": "<div style='margin:5px;'><b>[[x]]</b><br>y:<b>[[y]]</b><br>value:<b>[[value]]</b></div>",
            "bullet": "round",
            "id": "AmGraph-1",
            "lineAlpha": 0,
            "lineColor": "#b0de09",
            "fillAlphas": 0,
            "valueField": "aValue",
            "xField": "date",
            "yField": "ay"
        }, {
            "balloonText": "<div style='margin:5px;'><b>[[x]]</b><br>y:<b>[[y]]</b><br>value:<b>[[value]]</b></div>",
            "bullet": "round",
            "id": "AmGraph-2",
            "lineAlpha": 0,
            "lineColor": "#fcd202",
            "fillAlphas": 0,
            "valueField": "bValue",
            "xField": "date",
            "yField": "by"
        }],
        "valueAxes": [{
            "id": "ValueAxis-1",
            "axisAlpha": 0
        }, {
            "id": "ValueAxis-2",
            "axisAlpha": 0,
            "position": "bottom",
            "type": "date",
            "minimumDate": new Date(2014, 11, 31),
            "maximumDate": new Date(2015, 0, 13)
        }],
        "allLabels": [],
        "titles": [],
        "dataProvider": [{
            "date": "2015-01-01",
            "ay": 6.5,
            "by": 2.2,
            "aValue": 15,
            "bValue": 10,
        }, {
            "date": "2015-01-02",
            "ay": 12.3,
            "by": 4.9,
            "aValue": 8,
            "bValue": 3
        }, {
            "date": "2015-01-03",
            "ay": 12.3,
            "by": 5.1,
            "aValue": 16,
            "bValue": 4
        }, {
            "date": "2015-01-04",
            "ay": 2.8,
            "by": 13.3,
            "aValue": 9,
            "bValue": 13
        }, {
            "date": "2015-01-05",
            "ay": 3.5,
            "by": 6.1,
            "aValue": 5,
            "bValue": 2
        }, {
            "date": "2015-01-06",
            "ay": 5.1,
            "by": 8.3,
            "aValue": 10,
            "bValue": 17
        }, {
            "date": "2015-01-07",
            "ay": 6.7,
            "by": 10.5,
            "aValue": 3,
            "bValue": 10
        }, {
            "date": "2015-01-08",
            "ay": 8,
            "by": 12.3,
            "aValue": 5,
            "bValue": 13
        }, {
            "date": "2015-01-09",
            "ay": 8.9,
            "by": 4.5,
            "aValue": 8,
            "bValue": 11
        }, {
            "date": "2015-01-10",
            "ay": 9.7,
            "by": 15,
            "aValue": 15,
            "bValue": 10
        }, {
            "date": "2015-01-11",
            "ay": 10.4,
            "by": 10.8,
            "aValue": 1,
            "bValue": 11
        }, {
            "date": "2015-01-12",
            "ay": 1.7,
            "by": 19,
            "aValue": 12,
            "bValue": 3
        }],

        "export": {
            "enabled": true
        },

        "chartScrollbar": {
            "offset": 15,
            "scrollbarHeight": 5
        },

        "chartCursor":{
            "pan":true,
            "cursorAlpha":0,
            "valueLineAlpha":0
        }
    });


}

function drawBubbleChart02(){

    var chart = AmCharts.makeChart("modal02content", {
        "type": "xy",
        "theme": "light",
        "marginRight": 80,
        "dataDateFormat": "YYYY-MM-DD",
        "startDuration": 1.5,
        "trendLines": [],
        "balloon": {
            "adjustBorderColor": false,
            "shadowAlpha": 0,
            "fixedPosition":true
        },
        "graphs": [{
            "balloonText": "<div style='margin:5px;'><b>[[x]]</b><br>y:<b>[[y]]</b><br>value:<b>[[value]]</b></div>",
            "bullet": "round",
            "id": "AmGraph-1",
            "lineAlpha": 0,
            "lineColor": "#b0de09",
            "fillAlphas": 0,
            "valueField": "aValue",
            "xField": "date",
            "yField": "ay"
        }, {
            "balloonText": "<div style='margin:5px;'><b>[[x]]</b><br>y:<b>[[y]]</b><br>value:<b>[[value]]</b></div>",
            "bullet": "round",
            "id": "AmGraph-2",
            "lineAlpha": 0,
            "lineColor": "#fcd202",
            "fillAlphas": 0,
            "valueField": "bValue",
            "xField": "date",
            "yField": "by"
        }],
        "valueAxes": [{
            "id": "ValueAxis-1",
            "axisAlpha": 0
        }, {
            "id": "ValueAxis-2",
            "axisAlpha": 0,
            "position": "bottom",
            "type": "date",
            "minimumDate": new Date(2014, 11, 31),
            "maximumDate": new Date(2015, 0, 13)
        }],
        "allLabels": [],
        "titles": [],
        "dataProvider": [{
            "date": "2015-01-01",
            "ay": 6.5,
            "by": 2.2,
            "aValue": 15,
            "bValue": 10,
        }, {
            "date": "2015-01-02",
            "ay": 12.3,
            "by": 4.9,
            "aValue": 8,
            "bValue": 3
        }, {
            "date": "2015-01-03",
            "ay": 12.3,
            "by": 5.1,
            "aValue": 16,
            "bValue": 4
        }, {
            "date": "2015-01-04",
            "ay": 2.8,
            "by": 13.3,
            "aValue": 9,
            "bValue": 13
        }, {
            "date": "2015-01-05",
            "ay": 3.5,
            "by": 6.1,
            "aValue": 5,
            "bValue": 2
        }, {
            "date": "2015-01-06",
            "ay": 5.1,
            "by": 8.3,
            "aValue": 10,
            "bValue": 17
        }, {
            "date": "2015-01-07",
            "ay": 6.7,
            "by": 10.5,
            "aValue": 3,
            "bValue": 10
        }, {
            "date": "2015-01-08",
            "ay": 8,
            "by": 12.3,
            "aValue": 5,
            "bValue": 13
        }, {
            "date": "2015-01-09",
            "ay": 8.9,
            "by": 4.5,
            "aValue": 8,
            "bValue": 11
        }, {
            "date": "2015-01-10",
            "ay": 9.7,
            "by": 15,
            "aValue": 15,
            "bValue": 10
        }, {
            "date": "2015-01-11",
            "ay": 10.4,
            "by": 10.8,
            "aValue": 1,
            "bValue": 11
        }, {
            "date": "2015-01-12",
            "ay": 1.7,
            "by": 19,
            "aValue": 12,
            "bValue": 3
        }],

        "export": {
            "enabled": true
        },

        "chartScrollbar": {
            "offset": 15,
            "scrollbarHeight": 5
        },

        "chartCursor":{
            "pan":true,
            "cursorAlpha":0,
            "valueLineAlpha":0

        }
    });


}

function drawBubbleChart03(){
    var chart = AmCharts.makeChart("modal03content", {
        "type": "xy",
        "theme": "light",
        "marginRight": 80,
        "dataDateFormat": "YYYY-MM-DD",
        "startDuration": 1.5,
        "trendLines": [],
        "balloon": {
            "adjustBorderColor": false,
            "shadowAlpha": 0,
            "fixedPosition":true
        },
        "graphs": [{
            "balloonText": "<div style='margin:5px;'><b>[[x]]</b><br>y:<b>[[y]]</b><br>value:<b>[[value]]</b></div>",
            "bullet": "round",
            "id": "AmGraph-1",
            "lineAlpha": 0,
            "lineColor": "#b0de09",
            "fillAlphas": 0,
            "valueField": "aValue",
            "xField": "date",
            "yField": "ay"
        }, {
            "balloonText": "<div style='margin:5px;'><b>[[x]]</b><br>y:<b>[[y]]</b><br>value:<b>[[value]]</b></div>",
            "bullet": "round",
            "id": "AmGraph-2",
            "lineAlpha": 0,
            "lineColor": "#fcd202",
            "fillAlphas": 0,
            "valueField": "bValue",
            "xField": "date",
            "yField": "by"
        }],
        "valueAxes": [{
            "id": "ValueAxis-1",
            "axisAlpha": 0
        }, {
            "id": "ValueAxis-2",
            "axisAlpha": 0,
            "position": "bottom",
            "type": "date",
            "minimumDate": new Date(2014, 11, 31),
            "maximumDate": new Date(2015, 0, 13)
        }],
        "allLabels": [],
        "titles": [],
        "dataProvider": [{
            "date": "2015-01-01",
            "ay": 6.5,
            "by": 2.2,
            "aValue": 15,
            "bValue": 10,
        }, {
            "date": "2015-01-02",
            "ay": 12.3,
            "by": 4.9,
            "aValue": 8,
            "bValue": 3
        }, {
            "date": "2015-01-03",
            "ay": 12.3,
            "by": 5.1,
            "aValue": 16,
            "bValue": 4
        }, {
            "date": "2015-01-04",
            "ay": 2.8,
            "by": 13.3,
            "aValue": 9,
            "bValue": 13
        }, {
            "date": "2015-01-05",
            "ay": 3.5,
            "by": 6.1,
            "aValue": 5,
            "bValue": 2
        }, {
            "date": "2015-01-06",
            "ay": 5.1,
            "by": 8.3,
            "aValue": 10,
            "bValue": 17
        }, {
            "date": "2015-01-07",
            "ay": 6.7,
            "by": 10.5,
            "aValue": 3,
            "bValue": 10
        }, {
            "date": "2015-01-08",
            "ay": 8,
            "by": 12.3,
            "aValue": 5,
            "bValue": 13
        }, {
            "date": "2015-01-09",
            "ay": 8.9,
            "by": 4.5,
            "aValue": 8,
            "bValue": 11
        }, {
            "date": "2015-01-10",
            "ay": 9.7,
            "by": 15,
            "aValue": 15,
            "bValue": 10
        }, {
            "date": "2015-01-11",
            "ay": 10.4,
            "by": 10.8,
            "aValue": 1,
            "bValue": 11
        }, {
            "date": "2015-01-12",
            "ay": 1.7,
            "by": 19,
            "aValue": 12,
            "bValue": 3
        }],

        "export": {
            "enabled": true
        },

        "chartScrollbar": {
            "offset": 15,
            "scrollbarHeight": 5
        },

        "chartCursor":{
            "pan":true,
            "cursorAlpha":0,
            "valueLineAlpha":0

        }
    });


}




$(document).ready(function () {
    // menu dropdown
    $(".dropdown-button").dropdown(function () {
        hover: false;
    });

    // existing currency pair
    $("#vc").text("BTC");
    $("#fc").text("USD");

    // initial card views
    menuonload();
    FirstLineUSD();
    SecondLineBTCUSD();
    Third_Fourth_Exchange_Bar();

    $("#FirstLineCard1").click(function () {

        firstLineCard1();
        $("#SecondLineCard1Text").click();

        if ($("#title1").text() === "BTC/USD") {
            SecondLineBTCUSD();
        }
        if ($("#title1").text() === "BTC/GBP") {
            SecondLineBTCGBP();
        }
        if ($("#title1").text() === "BTC/EUR") {
            SecondLineBTCEUR();
        }
        if ($("#title1").text() === "BTC/CNY") {
            SecondLineBTCCNY();
        }
    })

    $("#FirstLineCard2").click(function () {

        firstLineCard2();
        $("#SecondLineCard1Text").click();

        if ($("#title2").text() === "ETH/USD") {
            SecondLineETHUSD();
        }
        if ($("#title2").text() === "ETH/GBP") {
            SecondLineETHGBP();
        }
        if ($("#title2").text() === "ETH/EUR") {
            SecondLineETHEUR();
        }
        if ($("#title2").text() === "ETH/CNY") {
            SecondLineETHCNY();
        }
    })

    $("#FirstLineCard3").click(function () {
        firstLineCard3();
    });

    $("#FirstLineCard4").click(function () {
        firstLineCard4();
    });

    $("#SecondLineCard1Text, #SecondLineCard1Bar").click(function () {
        secondLineCard1();
    });

    $("#SecondLineCard2Text, #SecondLineCard2Bar").click(function () {
        secondLineCard2();
    });

    $("#SecondLineCard3Text, #SecondLineCard3Bar").click(function () {
        secondLineCard3();
    });

    $("#SecondLineCard4Text, #SecondLineCard4Bar").click(function () {
        secondLineCard4();
    });

})

function menuonload() {
    $("#otc-usd").click(function () {
        $("#fc").text("USD");

        FirstLineUSD();
        SecondLineBTCUSD();
        firstLineCard1();
        $("#SecondLineCard1Text").click();

    });

    $("#otc-gbp").click(function () {
        $("#fc").text("GBP");

        FirstLineGBP();
        SecondLineBTCGBP();
        firstLineCard1();
        $("#SecondLineCard1Text").click();

    });

    $("#otc-eur").click(function () {
        $("#fc").text("EUR");

        FirstLineEUR();
        SecondLineBTCEUR();
        firstLineCard1();
        $("#SecondLineCard1Text").click();

    });

    $("#otc-cny").click(function () {
        $("#fc").text("CNY");

        FirstLineCNY();
        SecondLineBTCCNY();
        firstLineCard1();
    });
}

function FirstLineUSD() {
    $(".card-symbol").text("$");

    $("#title1").text("BTC/USD");

    $.get("http://localhost:8080/data/GlobalUSD", function (data, status) {
        $("#volume1").text(data.volume);
        $("#price1").text(data.price);
        $("#change1").text(data.change);
    })

    $("#title2").text("ETH/USD");
    $.get("http://localhost:8080/data/Global_ETH_USD", function (data, status) {
        $("#volume2").text(data.volume);
        $("#price2").text(data.price);
        $("#change2").text(data.change);
    })

    $("#title3").text("EOS/USD");
    $.get("http://localhost:8080/data/Global_EOS_USD", function (data, status) {
        $("#volume3").text(data.volume);
        $("#price3").text(data.price);
        $("#change3").text(data.change);
    })

    $("#title4").text("LTC/USD");
    $.get("http://localhost:8080/data/Global_LTC_USD", function (data, status) {
        $("#volume4").text(data.volume);
        $("#price4").text(data.price);
        $("#change4").text(data.change);
    })
}

function FirstLineGBP() {
    $(".card-symbol").text("£");

    $("#title1").text("BTC/GBP");
    $.get("http://localhost:8080/data/GlobalGBP", function (data, status) {
        $("#volume1").text(data.volume);
        $("#price1").text(data.price);
        $("#change1").text(data.change);
    })

    $("#title2").text("ETH/GBP");
    $.get("http://localhost:8080/data/Global_ETH_GBP", function (data, status) {
        $("#volume2").text(data.volume);
        $("#price2").text(data.price);
        $("#change2").text(data.change);
    })

    $("#title3").text("EOS/GBP");
    $.get("http://localhost:8080/data/Global_EOS_GBP", function (data, status) {
        $("#volume3").text(data.volume);
        $("#price3").text(data.price);
        $("#change3").text(data.change);
    })

    $("#title4").text("LTC/GBP");
    $.get("http://localhost:8080/data/Global_LTC_GBP", function (data, status) {
        $("#volume4").text(data.volume);
        $("#price4").text(data.price);
        $("#change4").text(data.change);
    })
}

function FirstLineEUR() {
    $("#title1").text("BTC/EUR");
    $.get("http://localhost:8080/data/GlobalEUR", function (data, status) {
        $("#volume1").text(data.volume);
        $("#price1").text(data.price);
        $("#change1").text(data.change);
    })

    $("#title2").text("ETH/EUR");
    $.get("http://localhost:8080/data/Global_ETH_EUR", function (data, status) {
        $("#volume2").text(data.volume);
        $("#price2").text(data.price);
        $("#change2").text(data.change);
    })

    $("#title3").text("EOS/EUR");
    $.get("http://localhost:8080/data/Global_EOS_EUR", function (data, status) {
        $("#volume3").text(data.volume);
        $("#price3").text(data.price);
        $("#change3").text(data.change);
    })

    $("#title4").text("LTC/EUR");
    $.get("http://localhost:8080/data/Global_LTC_EUR", function (data, status) {
        $("#volume4").text(data.volume);
        $("#price4").text(data.price);
        $("#change4").text(data.change);
    })

    $(".card-symbol").text("€");
}

function FirstLineCNY() {
    $("#title1").text("BTC/CNY");
    $.get("http://localhost:8080/data/GlobalCNY", function (data, status) {
        $("#volume1").text(data.volume);
        $("#price1").text(data.price);
        $("#change1").text(data.change);
    })

    $("#title2").text("ETH/CNY");
    $.get("http://localhost:8080/data/Global_ETH_CNY", function (data, status) {
        $("#volume2").text(data.volume);
        $("#price2").text(data.price);
        $("#change2").text(data.change);
    })

    $("#title3").text("EOS/CNY");
    $.get("http://localhost:8080/data/Global_EOS_CNY", function (data, status) {
        $("#volume3").text(data.volume);
        $("#price3").text(data.price);
        $("#change3").text(data.change);
    })

    $("#title4").text("LTC/CNY");
    $.get("http://localhost:8080/data/Global_LTC_CNY", function (data, status) {
        $("#volume4").text(data.volume);
        $("#price4").text(data.price);
        $("#change4").text(data.change);
    })

    $(".card-symbol").text("¥");
}

function SecondLineBTCUSD() {
    $(".card-symbol-2").text("$");
    $(".exchangePair").text(" BTC/USD");

    $("#exchangeTitle1").text(" GDAX");
    $.get("http://localhost:8080/data/GDAX_CurrencyPrice&Vol_BTC_USD", function (data, status) {
        console.log("USD-GDAX-BTC");
        console.log(data);
        $("#exchangePrice1").text(data.price);
        $("#exchangeVolume1").text(data.volume);
    });
    GDAX_BTC_USD_Bar();

    $("#exchangeTitle2").text(" Bitfinex");
    $.get("http://localhost:8080/data/BitFinex_CurrencyPrice&Vol_BTC_USD", function (data, status) {
        console.log("USD-BitFinex-BTC");
        console.log(data);
        $("#exchangePrice2").text(parseFloat(data[3]).toFixed(2));
        $("#exchangeVolume2").text(data[6]);
    });
    Bitfinex_BTC_USD_Bar();

    $("#exchangeTitle3").text(" Bitstamp");
    // $.get("http://localhost:8080/data/???", function (data, status) {
    //     $("#exchangePrice3").text(data.price);
    //     $("#exchangeVolume3").text(data.volume);
    //  })

    $("#exchangeTitle4").text(" Kraken");
    // $.get("http://localhost:8080/data/???", function (data, status) {
    //     $("#exchangePrice4").text(data.price);
    //     $("#exchangeVolume4").text(data.volume);
    // })

    fullSecondLineCard();
}

function SecondLineBTCEUR() {
    $(".card-symbol-2").text("€");
    $(".exchangePair").text(" BTC/EUR");

    $("#exchangeTitle1").text(" GDAX");
    $.get("http://localhost:8080/data/GDAX_CurrencyPrice&Vol_BTC_EUR", function (data, status) {
        $("#exchangePrice1").text(data.price);
        $("#exchangeVolume1").text(data.volume);
    })
    GDAX_BTC_EUR_Bar();

    $("#exchangeTitle2").text(" Bitfinex");
    $.get("http://localhost:8080/data/BitFinex_CurrencyPrice&Vol_BTC_EUR", function (data, status) {
        $("#exchangePrice2").text(parseFloat(data[3]).toFixed(2));
        $("#exchangeVolume2").text(data[6]);
    })
    Bitfinex_BTC_EUR_Bar();

    $("#exchangeTitle3").text(" Bitstamp");
    $("#exchangePrice3").text("");
    $("#exchangeVolume3").text("");

    $("#exchangeTitle4").text(" Kraken");
    $("#exchangePrice4").text("");
    $("#exchangeVolume4").text("");

    fullSecondLineCard();
}

function SecondLineBTCGBP() {
    $(".card-symbol-2").text("£");
    $(".exchangePair").text(" BTC/GBP");

    $("#exchangeTitle1").text(" GDAX");
    $.get("http://localhost:8080/data/GDAX_CurrencyPrice&Vol_BTC_GBP", function (data, status) {
        console.log("GBP-GDAX-BTC")
        console.log(data)
        $("#exchangePrice1").text(data.price);
        $("#exchangeVolume1").text(data.volume);
    })
    GDAX_BTC_GBP_Bar();

    $("#exchangeTitle2").text(" Bitfinex");
    $.get("http://localhost:8080/data/BitFinex_CurrencyPrice&Vol_BTC_GBP", function (data, status) {
        console.log("GBP-BitFinex-BTC")
        console.log(data)
        $("#exchangePrice2").text(parseFloat(data[3]).toFixed(2));
        $("#exchangeVolume2").text(data[6]);
    })
    Bitfinex_BTC_GBP_Bar();

    fullSecondLineCard();
}

function SecondLineBTCCNY() {
    $(".card-symbol-2").text("¥");
    $(".exchangePair").text(" BTC/CNY");

    $("#exchangeTitle1").text(" OKEX");
    $("#exchangePrice1").text("");
    $("#exchangeVolume1").text("");

    $("#exchangeTitle2").text(" Huobi");
    $("#exchangePrice2").text("");
    $("#exchangeVolume2").text("");

    $("#exchange-card-3").css("visibility", "hidden");
    $("#exchange-card-4").css("visibility", "hidden");
}

function SecondLineETHUSD() {
    $(".card-symbol-2").text("$");
    $(".exchangePair").text(" ETH/USD");

    $("#exchangeTitle1").text(" GDAX");
    $.get("http://localhost:8080/data/GDAX_CurrencyPrice&Vol_ETH_USD", function (data, status) {
        $("#exchangePrice1").text(data.price);
        $("#exchangeVolume1").text(data.volume);
    })
    GDAX_ETH_USD_Bar();

    $("#exchangeTitle2").text(" Bitfinex");
    $.get("http://localhost:8080/data/BitFinex_CurrencyPrice&Vol_ETH_USD", function (data, status) {
        $("#exchangePrice2").text(parseFloat(data[3]).toFixed(2));
        $("#exchangeVolume2").text(data[6]);
    })
    Bitfinex_ETH_USD_Bar();

    fullSecondLineCard();
}

function SecondLineETHEUR() {

    $(".card-symbol-2").text("€");
    $(".exchangePair").text(" ETH/EUR");

    $("#exchangeTitle1").text(" GDAX");
    $.get("http://localhost:8080/data/GDAX_CurrencyPrice&Vol_ETH_EUR", function (data, status) {
        $("#exchangePrice1").text(data.price);
        $("#exchangeVolume1").text(data.volume);
    })
    GDAX_ETH_EUR_Bar();

    $("#exchangeTitle2").text(" Bitfinex");
    $.get("http://localhost:8080/data/BitFinex_CurrencyPrice&Vol_ETH_EUR", function (data, status) {
        $("#exchangePrice2").text(parseFloat(data[3]).toFixed(2));
        $("#exchangeVolume2").text(data[6]);
    })
    Bitfinex_ETH_EUR_Bar();

    $("#exchangeTitle3").text(" Bitstamp");
    $("#exchangePrice3").text("");
    $("#exchangeVolume3").text("");

    $("#exchangeTitle4").text(" Kraken");
    $("#exchangePrice4").text("");
    $("#exchangeVolume4").text("");

    fullSecondLineCard();
}

function SecondLineETHGBP() {
    $(".card-symbol-2").text("£");
    $(".exchangePair").text(" ETH/GBP");

    $("#exchangeTitle1").text(" GDAX");
    $("#exchangePrice1").text("");
    $("#exchangeVolume1").text("");
    GDAX_ETH_GBP_Bar();

    $("#exchangeTitle2").text(" Bitfinex");
    $.get("http://localhost:8080/data/BitFinex_CurrencyPrice&Vol_ETH_GBP", function (data, status) {
        $("#exchangePrice2").text(parseFloat(data[3]).toFixed(2));
        $("#exchangeVolume2").text(data[6]);
    })
    Bitfinex_ETH_GBP_Bar();

    $("#exchangeTitle3").text(" Bitstamp");
    $("#exchangePrice3").text("");
    $("#exchangeVolume3").text("");

    $("#exchangeTitle4").text(" Kraken");
    $("#exchangePrice4").text("");
    $("#exchangeVolume4").text("");

    fullSecondLineCard();
}

function SecondLineETHCNY() {
    $(".card-symbol-2").text("¥");
    $(".exchangePair").text(" ETH/CNY");

    $("#exchangeTitle1").text(" OKEX");
    $("#exchangePrice1").text("");
    $("#exchangeVolume1").text("");

    $("#exchangeTitle2").text(" Huobi");
    $("#exchangePrice2").text("");
    $("#exchangeVolume2").text("");

    $("#exchange-card-3").css("visibility", "hidden");
    $("#exchange-card-4").css("visibility", "hidden");
}

function GDAX_BTC_USD_Bar() {
    $.ajax({
        type: 'POST',
        url: "http://localhost:8080/data/cardGDAXVolPrice",
        data:{},
        success: function (data) {
            var arraybar = [];
            var arrayprice = [];
            for(var i =0 ; i < data.length; i++){
                var time = data[i].date;
                time = new Date(time);
                data[i].date = time
                arraybar.push(data[i].volume)
                arrayprice.push(data[i].close)
            }
            console.log("wwwww")
            console.log(arrayprice,arraybar)
            // GDAX - Bar
            $('#compositebar1').sparkline(arraybar, {
                type: 'bar',
                barColor: '#fff',
                height: '25',
                width: '100%',
                barWidth: '7',
                barSpacing: 4
            });

            $('#compositebar1').sparkline(arrayprice, {
                composite: true,
                type: 'line',
                width: '100%',
                lineWidth: 2,
                lineColor: '#fff3e0',
                fillColor: 'rgba(0, 94, 255, 0)',
                highlightSpotColor: '#fff3e0',
                highlightLineColor: '#fff3e0',
                minSpotColor: '#00bcd4',
                maxSpotColor: '#00e676',
                spotColor: '#fff3e0',
                spotRadius: 4
            });

        }, error: function () {
            alert("wrong in candle2");
        }
    })
}

function GDAX_ETH_USD_Bar() {
    $.ajax({
        type: 'POST',
        url: "http://localhost:8080/data/cardGDAXVolPriceETH",
        data:{},
        success: function (data) {
            var arraybar = [];
            var arrayprice = [];
            for(var i =0 ; i < data.length; i++){
                var time = data[i].date;
                time = new Date(time);
                data[i].date = time
                arraybar.push(data[i].volume)
                arrayprice.push(data[i].close)
            }

            // GDAX - Bar
            $('#compositebar1').sparkline(arraybar, {
                type: 'bar',
                barColor: '#fff',
                height: '25',
                width: '100%',
                barWidth: '7',
                barSpacing: 4
            });

            $('#compositebar1').sparkline(arrayprice, {
                composite: true,
                type: 'line',
                width: '100%',
                lineWidth: 2,
                lineColor: '#fff3e0',
                fillColor: 'rgba(0, 94, 255, 0)',
                highlightSpotColor: '#fff3e0',
                highlightLineColor: '#fff3e0',
                minSpotColor: '#00bcd4',
                maxSpotColor: '#00e676',
                spotColor: '#fff3e0',
                spotRadius: 4
            });

        }, error: function () {
            alert("wrong in candle2");
        }
    })
}

function GDAX_BTC_EUR_Bar() {
    $.ajax({
        type: 'POST',
        url: "http://localhost:8080/data/cardGDAXEURVolPriceBTC",
        data:{},
        success: function (data) {
            var arraybar = [];
            var arrayprice = [];
            for(var i =0 ; i < data.length; i++){
                var time = data[i].date;
                time = new Date(time);
                data[i].date = time
                arraybar.push(data[i].volume)
                arrayprice.push(data[i].close)
            }

            // GDAX - Bar
            $('#compositebar1').sparkline(arraybar, {
                type: 'bar',
                barColor: '#fff',
                height: '25',
                width: '100%',
                barWidth: '7',
                barSpacing: 4
            });

            $('#compositebar1').sparkline(arrayprice, {
                composite: true,
                type: 'line',
                width: '100%',
                lineWidth: 2,
                lineColor: '#fff3e0',
                fillColor: 'rgba(0, 94, 255, 0)',
                highlightSpotColor: '#fff3e0',
                highlightLineColor: '#fff3e0',
                minSpotColor: '#00bcd4',
                maxSpotColor: '#00e676',
                spotColor: '#fff3e0',
                spotRadius: 4
            });

        }, error: function () {
            alert("wrong in candle2");
        }
    })
}

function GDAX_ETH_EUR_Bar() {
    $.ajax({
        type: 'POST',
        url: "http://localhost:8080/data/cardGDAXEURVolPriceETH",
        data:{},
        success: function (data) {
            var arraybar = [];
            var arrayprice = [];
            for(var i =0 ; i < data.length; i++){
                var time = data[i].date;
                time = new Date(time);
                data[i].date = time
                arraybar.push(data[i].volume)
                arrayprice.push(data[i].close)
            }

            // GDAX - Bar
            $('#compositebar1').sparkline(arraybar, {
                type: 'bar',
                barColor: '#fff',
                height: '25',
                width: '100%',
                barWidth: '7',
                barSpacing: 4
            });

            $('#compositebar1').sparkline(arrayprice, {
                composite: true,
                type: 'line',
                width: '100%',
                lineWidth: 2,
                lineColor: '#fff3e0',
                fillColor: 'rgba(0, 94, 255, 0)',
                highlightSpotColor: '#fff3e0',
                highlightLineColor: '#fff3e0',
                minSpotColor: '#00bcd4',
                maxSpotColor: '#00e676',
                spotColor: '#fff3e0',
                spotRadius: 4
            });

        }, error: function () {
            alert("wrong in candle2");
        }
    })
}

function GDAX_BTC_GBP_Bar() {
    $.ajax({
        type: 'POST',
        url: "http://localhost:8080/data/cardGDAXGBPVolPrice",
        data:{},
        success: function (data) {
            var arraybar = [];
            var arrayprice = [];
            for(var i =0 ; i < data.length; i++){
                var time = data[i].date;
                time = new Date(time);
                data[i].date = time
                arraybar.push(data[i].volume)
                arrayprice.push(data[i].close)
            }

            // GDAX - Bar
            $('#compositebar1').sparkline(arraybar, {
                type: 'bar',
                barColor: '#fff',
                height: '25',
                width: '100%',
                barWidth: '7',
                barSpacing: 4
            });

            $('#compositebar1').sparkline(arrayprice, {
                composite: true,
                type: 'line',
                width: '100%',
                lineWidth: 2,
                lineColor: '#fff3e0',
                fillColor: 'rgba(0, 94, 255, 0)',
                highlightSpotColor: '#fff3e0',
                highlightLineColor: '#fff3e0',
                minSpotColor: '#00bcd4',
                maxSpotColor: '#00e676',
                spotColor: '#fff3e0',
                spotRadius: 4
            });

        }, error: function () {
            alert("wrong in candle2");
        }
    })
}

function GDAX_ETH_GBP_Bar() {
    $.ajax({
        type: 'POST',
        url: "http://localhost:8080/data/cardGDAXGBPVolPriceETH",
        data:{},
        success: function (data) {
            var arraybar = [];
            var arrayprice = [];
            for(var i =0 ; i < data.length; i++){
                var time = data[i].date;
                time = new Date(time);
                data[i].date = time
                arraybar.push(data[i].volume)
                arrayprice.push(data[i].close)
            }

            // GDAX - Bar
            $('#compositebar1').sparkline(arraybar, {
                type: 'bar',
                barColor: '#fff',
                height: '25',
                width: '100%',
                barWidth: '7',
                barSpacing: 4
            });

            $('#compositebar1').sparkline(arrayprice, {
                composite: true,
                type: 'line',
                width: '100%',
                lineWidth: 2,
                lineColor: '#fff3e0',
                fillColor: 'rgba(0, 94, 255, 0)',
                highlightSpotColor: '#fff3e0',
                highlightLineColor: '#fff3e0',
                minSpotColor: '#00bcd4',
                maxSpotColor: '#00e676',
                spotColor: '#fff3e0',
                spotRadius: 4
            });

        }, error: function () {
            alert("wrong in candle2");
        }
    })
}

function Bitfinex_BTC_USD_Bar() {
    $.ajax({
        type: 'POST',
        url: "http://localhost:8080/data/cardBitfinexVolPrice",
        data:{},
        success: function (data) {
            var arraybar = [];
            var arrayprice = [];
            for(var i =0 ; i < data.length; i++){
                var time = data[i].date;
                time = new Date(time);
                data[i].date = time
                arraybar.push(data[i].volume)
                arrayprice.push(data[i].close)
            }

            // BITFINEX - Bar
            $('#compositebar2').sparkline(arraybar, {
                type: 'bar',
                barColor: '#fff',
                height: '25',
                width: '100%',
                barWidth: '7',
                barSpacing: 4
            });
            // BITFINEX - Line
            $('#compositebar2').sparkline(arrayprice, {
                composite: true,
                type: 'line',
                width: '100%',
                lineWidth: 2,
                lineColor: '#fff3e0',
                fillColor: 'rgba(0, 94, 255, 0)',
                highlightSpotColor: '#fff3e0',
                highlightLineColor: '#fff3e0',
                minSpotColor: '#00bcd4',
                maxSpotColor: '#00e676',
                spotColor: '#fff3e0',
                spotRadius: 4
            });

        }, error: function () {
            alert("wrong in candle2");
        }
    })
}

function Bitfinex_ETH_USD_Bar() {
    $.ajax({
        type: 'POST',
        url: "http://localhost:8080/data/cardBitfinexVolPriceETH",
        data:{},
        success: function (data) {
            var arraybar = [];
            var arrayprice = [];
            for(var i =0 ; i < data.length; i++){
                var time = data[i].date;
                time = new Date(time);
                data[i].date = time
                arraybar.push(data[i].volume)
                arrayprice.push(data[i].close)
            }

            // BITFINEX - Bar
            $('#compositebar2').sparkline(arraybar, {
                type: 'bar',
                barColor: '#fff',
                height: '25',
                width: '100%',
                barWidth: '7',
                barSpacing: 4
            });
            // BITFINEX - Line
            $('#compositebar2').sparkline(arrayprice, {
                composite: true,
                type: 'line',
                width: '100%',
                lineWidth: 2,
                lineColor: '#fff3e0',
                fillColor: 'rgba(0, 94, 255, 0)',
                highlightSpotColor: '#fff3e0',
                highlightLineColor: '#fff3e0',
                minSpotColor: '#00bcd4',
                maxSpotColor: '#00e676',
                spotColor: '#fff3e0',
                spotRadius: 4
            });

        }, error: function () {
            alert("wrong in candle2");
        }
    })
}

function Bitfinex_BTC_EUR_Bar() {
    $.ajax({
        type: 'POST',
        url: "http://localhost:8080/data/cardBitfinexEURVolPriceBTC",
        data:{},
        success: function (data) {
            var arraybar = [];
            var arrayprice = [];
            for(var i =0 ; i < data.length; i++){
                var time = data[i].date;
                time = new Date(time);
                data[i].date = time
                arraybar.push(data[i].volume)
                arrayprice.push(data[i].close)
            }

            // BITFINEX - Bar
            $('#compositebar2').sparkline(arraybar, {
                type: 'bar',
                barColor: '#fff',
                height: '25',
                width: '100%',
                barWidth: '7',
                barSpacing: 4
            });
            // BITFINEX - Line
            $('#compositebar2').sparkline(arrayprice, {
                composite: true,
                type: 'line',
                width: '100%',
                lineWidth: 2,
                lineColor: '#fff3e0',
                fillColor: 'rgba(0, 94, 255, 0)',
                highlightSpotColor: '#fff3e0',
                highlightLineColor: '#fff3e0',
                minSpotColor: '#00bcd4',
                maxSpotColor: '#00e676',
                spotColor: '#fff3e0',
                spotRadius: 4
            });

        }, error: function () {
            alert("wrong in candle2");
        }
    })
}

function Bitfinex_ETH_EUR_Bar() {
    $.ajax({
        type: 'POST',
        url: "http://localhost:8080/data/cardBitfinexEURVolPriceETH",
        data:{},
        success: function (data) {
            var arraybar = [];
            var arrayprice = [];
            for(var i =0 ; i < data.length; i++){
                var time = data[i].date;
                time = new Date(time);
                data[i].date = time
                arraybar.push(data[i].volume)
                arrayprice.push(data[i].close)
            }

            // BITFINEX - Bar
            $('#compositebar2').sparkline(arraybar, {
                type: 'bar',
                barColor: '#fff',
                height: '25',
                width: '100%',
                barWidth: '7',
                barSpacing: 4
            });
            // BITFINEX - Line
            $('#compositebar2').sparkline(arrayprice, {
                composite: true,
                type: 'line',
                width: '100%',
                lineWidth: 2,
                lineColor: '#fff3e0',
                fillColor: 'rgba(0, 94, 255, 0)',
                highlightSpotColor: '#fff3e0',
                highlightLineColor: '#fff3e0',
                minSpotColor: '#00bcd4',
                maxSpotColor: '#00e676',
                spotColor: '#fff3e0',
                spotRadius: 4
            });

        }, error: function () {
            alert("wrong in candle2");
        }
    })
}

function Bitfinex_BTC_GBP_Bar() {
    $.ajax({
        type: 'POST',
        url: "http://localhost:8080/data/cardBitfinexGBPVolPrice",
        data:{},
        success: function (data) {
            var arraybar = [];
            var arrayprice = [];
            for(var i =0 ; i < data.length; i++){
                var time = data[i].date;
                time = new Date(time);
                data[i].date = time
                arraybar.push(data[i].volume)
                arrayprice.push(data[i].close)
            }

            // BITFINEX - Bar
            $('#compositebar2').sparkline(arraybar, {
                type: 'bar',
                barColor: '#fff',
                height: '25',
                width: '100%',
                barWidth: '7',
                barSpacing: 4
            });
            // BITFINEX - Line
            $('#compositebar2').sparkline(arrayprice, {
                composite: true,
                type: 'line',
                width: '100%',
                lineWidth: 2,
                lineColor: '#fff3e0',
                fillColor: 'rgba(0, 94, 255, 0)',
                highlightSpotColor: '#fff3e0',
                highlightLineColor: '#fff3e0',
                minSpotColor: '#00bcd4',
                maxSpotColor: '#00e676',
                spotColor: '#fff3e0',
                spotRadius: 4
            });

        }, error: function () {
            alert("wrong in candle2");
        }
    })
}

function Bitfinex_ETH_GBP_Bar() {
    $.ajax({
        type: 'POST',
        url: "http://localhost:8080/data/cardBitfinexGBPVolPriceETH",
        data:{},
        success: function (data) {
            var arraybar = [];
            var arrayprice = [];
            for(var i =0 ; i < data.length; i++){
                var time = data[i].date;
                time = new Date(time);
                data[i].date = time
                arraybar.push(data[i].volume)
                arrayprice.push(data[i].close)
            }

            // BITFINEX - Bar
            $('#compositebar2').sparkline(arraybar, {
                type: 'bar',
                barColor: '#fff',
                height: '25',
                width: '100%',
                barWidth: '7',
                barSpacing: 4
            });
            // BITFINEX - Line
            $('#compositebar2').sparkline(arrayprice, {
                composite: true,
                type: 'line',
                width: '100%',
                lineWidth: 2,
                lineColor: '#fff3e0',
                fillColor: 'rgba(0, 94, 255, 0)',
                highlightSpotColor: '#fff3e0',
                highlightLineColor: '#fff3e0',
                minSpotColor: '#00bcd4',
                maxSpotColor: '#00e676',
                spotColor: '#fff3e0',
                spotRadius: 4
            });

        }, error: function () {
            alert("wrong in candle2");
        }
    })
}

function Third_Fourth_Exchange_Bar() {
    //  the 3rd exchange card
    $('#compositebar3').sparkline([4, 6, 7, 7, 4, 3, 2, 3, 1, 4, 6, 5, 9, 4, 6, 7, 7, 4, 6, 5, 9], {
        type: 'bar',
        barColor: '#fff',
        height: '25',
        width: '100%',
        barWidth: '7',
        barSpacing: 4
    });

    $('#compositebar3').sparkline([4, 1, 5, 7, 9, 9, 8, 8, 4, 2, 5, 6, 7, 4, 1, 5, 7, 9, 9, 8, 8], {
        composite: true,
        type: 'line',
        width: '100%',
        lineWidth: 2,
        lineColor: '#fff3e0',
        fillColor: 'rgba(0, 94, 255, 0)',
        highlightSpotColor: '#fff3e0',
        highlightLineColor: '#fff3e0',
        minSpotColor: '#00bcd4',
        maxSpotColor: '#00e676',
        spotColor: '#fff3e0',
        spotRadius: 4
    });

    //  the 4th exchange card
    $('#compositebar4').sparkline([4, 6, 7, 7, 4, 3, 2, 3, 1, 4, 6, 5, 9, 4, 6, 7, 7, 4, 6, 5, 9], {
        type: 'bar',
        barColor: '#fff',
        height: '25',
        width: '100%',
        barWidth: '7',
        barSpacing: 4
    });

    $('#compositebar4').sparkline([4, 1, 5, 7, 9, 9, 8, 8, 4, 2, 5, 6, 7, 4, 1, 5, 7, 9, 9, 8, 8], {
        composite: true,
        type: 'line',
        width: '100%',
        lineWidth: 2,
        lineColor: '#fff3e0',
        fillColor: 'rgba(0, 94, 255, 0)',
        highlightSpotColor: '#fff3e0',
        highlightLineColor: '#fff3e0',
        minSpotColor: '#00bcd4',
        maxSpotColor: '#00e676',
        spotColor: '#fff3e0',
        spotRadius: 4
    });
}

function fullSecondLineCard() {
    $("#exchange-card-1").css("visibility", "visible");
    $("#exchange-card-2").css("visibility", "visible");
    $("#exchange-card-3").css("visibility", "visible");
    $("#exchange-card-4").css("visibility", "visible");
}

function firstLineCard1() {
    $("#vc").text("BTC");
    document.getElementById("FirstLineCard1").classList.remove("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("FirstLineCard1").classList.add("gradient-45deg-purple-deep-orange");
    document.getElementById("FirstLineCard2").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("FirstLineCard2").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("FirstLineCard3").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("FirstLineCard3").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("FirstLineCard4").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("FirstLineCard4").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    secondLineCard1();
}

function firstLineCard2() {
    $("#vc").text("ETH");
    document.getElementById("FirstLineCard2").classList.remove("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("FirstLineCard2").classList.add("gradient-45deg-purple-deep-orange");
    document.getElementById("FirstLineCard1").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("FirstLineCard1").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("FirstLineCard3").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("FirstLineCard3").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("FirstLineCard4").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("FirstLineCard4").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    secondLineCard1();
}

function firstLineCard3() {
    $("#vc").text("EOS");
    document.getElementById("FirstLineCard3").classList.remove("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("FirstLineCard3").classList.add("gradient-45deg-purple-deep-orange");
    document.getElementById("FirstLineCard1").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("FirstLineCard1").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("FirstLineCard2").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("FirstLineCard2").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("FirstLineCard4").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("FirstLineCard4").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    secondLineCard1();
}

function firstLineCard4() {
    $("#vc").text("LTC");
    document.getElementById("FirstLineCard4").classList.remove("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("FirstLineCard4").classList.add("gradient-45deg-purple-deep-orange");
    document.getElementById("FirstLineCard1").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("FirstLineCard1").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("FirstLineCard3").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("FirstLineCard3").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("FirstLineCard2").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("FirstLineCard2").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    secondLineCard1();
}

function secondLineCard1() {
    document.getElementById("SecondLineCard1Text").classList.remove("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("SecondLineCard1Text").classList.add("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard1Bar").classList.remove("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("SecondLineCard1Bar").classList.add("gradient-45deg-purple-deep-orange");

    document.getElementById("SecondLineCard2Text").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard2Text").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("SecondLineCard2Bar").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard2Bar").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");

    document.getElementById("SecondLineCard3Text").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard3Text").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("SecondLineCard3Bar").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard3Bar").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");

    document.getElementById("SecondLineCard4Text").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard4Text").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("SecondLineCard4Bar").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard4Bar").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
}

function secondLineCard2() {
    document.getElementById("SecondLineCard2Text").classList.remove("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("SecondLineCard2Text").classList.add("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard2Bar").classList.remove("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("SecondLineCard2Bar").classList.add("gradient-45deg-purple-deep-orange");

    document.getElementById("SecondLineCard1Text").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard1Text").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("SecondLineCard1Bar").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard1Bar").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");

    document.getElementById("SecondLineCard3Text").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard3Text").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("SecondLineCard3Bar").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard3Bar").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");

    document.getElementById("SecondLineCard4Text").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard4Text").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("SecondLineCard4Bar").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard4Bar").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
}

function secondLineCard3() {
    document.getElementById("SecondLineCard3Text").classList.remove("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("SecondLineCard3Text").classList.add("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard3Bar").classList.remove("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("SecondLineCard3Bar").classList.add("gradient-45deg-purple-deep-orange");

    document.getElementById("SecondLineCard1Text").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard1Text").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("SecondLineCard1Bar").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard1Bar").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");

    document.getElementById("SecondLineCard2Text").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard2Text").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("SecondLineCard2Bar").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard2Bar").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");

    document.getElementById("SecondLineCard4Text").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard4Text").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("SecondLineCard4Bar").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard4Bar").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
}

function secondLineCard4() {
    document.getElementById("SecondLineCard4Text").classList.remove("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("SecondLineCard4Text").classList.add("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard4Bar").classList.remove("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("SecondLineCard4Bar").classList.add("gradient-45deg-purple-deep-orange");

    document.getElementById("SecondLineCard1Text").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard1Text").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("SecondLineCard1Bar").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard1Bar").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");

    document.getElementById("SecondLineCard3Text").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard3Text").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("SecondLineCard3Bar").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard3Bar").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");

    document.getElementById("SecondLineCard2Text").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard2Text").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
    document.getElementById("SecondLineCard2Bar").classList.remove("gradient-45deg-purple-deep-orange");
    document.getElementById("SecondLineCard2Bar").classList.add("gradient-shadow", "gradient-45deg-blue-indigo");
}
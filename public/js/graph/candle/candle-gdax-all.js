/**
 * Created by MCIAKTE on 2018/6/10.
 */
chartData = []
gdax_btc_usd = "http://localhost:8080/data/candelGDAX_BTC_UTC_API"
gdax_eth_usd = "http://localhost:8080/data/candelGDAX_ETH_UTC_API"
gdax_btc_gbp = "http://localhost:8080/data/candelGDAX_BTC_GBP_API"
gdax_btc_eur = "http://localhost:8080/data/candelGDAX_BTC_EUR_API"
gdax_eth_eur = "http://localhost:8080/data/candelGDAX_ETH_EUR_API"

gdax_btc_usd_refresh = "http://localhost:8080/data/candelGDAX_BTC_UTC_API_Update"
gdax_eth_usd_refresh = "http://localhost:8080/data/candelGDAX_ETH_UTC_API_Update"
gdax_btc_gbp_refresh = "http://localhost:8080/data/candelGDAX_BTC_GBP_API_Update"
gdax_btc_eur_refresh = "http://localhost:8080/data/candelGDAX_BTC_EUR_API_Update"
gdax_eth_eur_refresh = "http://localhost:8080/data/candelGDAX_ETH_EUR_API_Update"

bitfinex_btc_usd = "http://localhost:8080/data/candleBitfinex"
bitfinex_btc_gbp = "http://localhost:8080/data/candleBitfinexGBP"
bitfinex_btc_eur = "http://localhost:8080/data/candleBitfinex_BTC_EUR"
bitfinex_eth_usd = "http://localhost:8080/data/candleBitfinex_ETH_USD"
bitfinex_eth_gbp = "http://localhost:8080/data/candleBitfinex_ETH_GBP"
bitfinex_eth_eur = "http://localhost:8080/data/candleBitfinex_ETH_EUR"

gdaxCandle(gdax_btc_usd, gdax_btc_usd_refresh, "GDAX BTC/USD");

$("#SecondLineCard1Text, #SecondLineCard1Bar").click(function () {
    if ($("#vc").text() === "BTC") {
        if ($("#fc").text() === "USD") {
            gdaxCandle(gdax_btc_usd, gdax_btc_usd_refresh, "GDAX BTC/USD");
        }
        if ($("#fc").text() === "GBP") {
            gdaxCandle(gdax_btc_gbp, gdax_btc_gbp_refresh, "GDAX BTC/GBP");
        }
        if ($("#fc").text() === "EUR") {
            gdaxCandle(gdax_btc_eur, gdax_btc_eur_refresh, "GDAX BTC/EUR");
        }
    }
    if ($("#vc").text() === "ETH") {
        if ($("#fc").text() === "USD") {
            gdaxCandle(gdax_eth_usd, gdax_eth_usd_refresh, "GDAX ETH/USD");
        }
        if ($("#fc").text() === "EUR") {
            gdaxCandle(gdax_eth_eur, gdax_eth_eur_refresh, "GDAX ETH/EUR");
        }
    }
})

$("#SecondLineCard2Text, #SecondLineCard2Bar").click(function () {
    if ($("#vc").text() === "BTC") {
        if ($("#fc").text() === "USD") {
            bitfinexCandle(bitfinex_btc_usd, "BITFINEX BTC/USD");
        }
        if ($("#fc").text() === "GBP") {
            bitfinexCandle(bitfinex_btc_gbp, "BITFINEX BTC/GBP");
        }
        if ($("#fc").text() === "EUR") {
            bitfinexCandle(bitfinex_btc_eur, "BITFINEX BTC/EUR");
        }
    }
    if ($("#vc").text() === "ETH") {
        if ($("#fc").text() === "USD") {
            bitfinexCandle(bitfinex_eth_usd, "BITFINEX ETH/USD");
        }
        if ($("#fc").text() === "EUR") {
            bitfinexCandle(bitfinex_eth_eur, "BITFINEX ETH/EUR");
        }
        if ($("#fc").text() === "GBP") {
            bitfinexCandle(bitfinex_eth_gbp, "BITFINEX ETH/GBP");
        }
    }
})

function gdaxCandle(chart_url, refresh_url, candle_gdax_title) {
    $.ajax({
        type: 'POST',
        url: chart_url,
        data:{},
        success: function (data) {
            var timestamp = data[data.length-1].date;
            for(var i =0 ; i < data.length; i++){
                var time = data[i].date;
                time = time*1000
                time = new Date(time);
                data[i].date = time

            }
            chartData = data
            jsondata = {
                "type": "stock",
                "theme": "light",

                "categoryAxesSettings": {
                    "minPeriod": "mm"
                },
                "glueToTheEnd": true,
                "dataSets": [ {
                    "fieldMappings": [ {
                        "fromField": "open",
                        "toField": "open"
                    }, {
                        "fromField": "close",
                        "toField": "close"
                    }, {
                        "fromField": "high",
                        "toField": "high"
                    }, {
                        "fromField": "low",
                        "toField": "low"
                    }, {
                        "fromField": "volume",
                        "toField": "volume"
                    }, {
                        "fromField": "value",
                        "toField": "value"
                    } ],

                    "color": "#7f8da9",
                    "dataProvider":chartData ,
                    "title": candle_gdax_title,
                    "categoryField": "date"
                }

                ],


                "panels": [ {
                    "title": "Value",
                    "showCategoryAxis": false,
                    "percentHeight": 70,
                    "valueAxes": [ {
                        "dashLength": 5
                    } ],

                    "categoryAxis": {
                        "dashLength": 5
                    },

                    "stockGraphs": [ {
                        "type": "candlestick",
                        "id": "g1",
                        "openField": "open",
                        "closeField": "close",
                        "highField": "high",
                        "lowField": "low",
                        "valueField": "close",
                        "lineColor": "#7f8da9",
                        "fillColors": "#7f8da9",
                        "negativeLineColor": "#db4c3c",
                        "negativeFillColors": "#db4c3c",
                        "fillAlphas": 1,
                        "useDataSetColors": false,
                        "comparable": true,
                        "compareField": "value",
                        "showBalloon": false
                    } ],

                    "stockLegend": {
                        "valueTextRegular": undefined,
                        "periodValueTextComparing": "[[percents.value.average]]%"
                    }
                },

                    {
                        "title": "Volume",
                        "percentHeight": 30,
                        "marginTop": 1,
                        "showCategoryAxis": true,
                        "valueAxes": [ {
                            "dashLength": 5
                        } ],

                        "categoryAxis": {
                            "dashLength": 5
                        },

                        "stockGraphs": [ {
                            "valueField": "volume",
                            "type": "column",
                            "showBalloon": false,
                            "fillAlphas": 1
                        } ],

                        "stockLegend": {
                            "markerType": "none",
                            "markerSize": 0,
                            "labelText": "",
                            "periodValueTextRegular": "[[value.close]]"
                        }
                    }
                ],

                "chartScrollbarSettings": {
                    "graph": "g1",
                    "graphType": "line",
                    "usePeriod": "hh"
                },

                "periodSelector": {
                    "position": "top",
                    "dateFormat": "YYYY-MM-DD JJ:NN",
                    "inputFieldWidth": 150,
                    "inputFieldsEnabled": false,
                    "periods": [ {
                        "period": "hh",
                        "count": 1,
                        "label": "1 hour",
                        "selected": true

                    }, {
                        "period": "hh",
                        "count": 2,
                        "label": "2 hours"
                    }, {
                        "period": "hh",
                        "count": 5,
                        "label": "5 hour"
                    }, {
                        "period": "hh",
                        "count": 12,
                        "label": "12 hours"
                    }, {
                        "period": "MAX",
                        "label": "MAX"
                    } ]
                }
            }

            var chart = AmCharts.makeChart( "candle-api", jsondata);
            var timedata = {
                time:timestamp
            }

            var gdaxRefresh = setInterval( function() {
                $.ajax({
                    // async : false,
                    type: 'POST',
                    url: refresh_url,
                    data: timedata,
                    success: function (data) {
                        for(var i =0 ; i < data.length; i++){
                            var time = data[i].date;
                            time = time*1000
                            time = new Date(time);
                            data[i].date = time
                        }
                        data = data[0];
                        chart.dataSets[ 0 ].dataProvider.push(data);
                        chart.validateData();
                        timedata.time = timedata.time+60;
                    }, error: function () {
                        alert("wrong");
                    }
                })
            },60000)

        }, error: function () {
            alert("wrong");
        }
    })

}

function bitfinexCandle(chart_url, candle_bitfinex_title) {
    $.ajax({
        type: 'POST',
        url: chart_url,
        data: {},
        success: function (data) {

            for (var i = 0; i < data.length; i++) {
                var time = data[i].date;
                time = new Date(time);
                data[i].date = time
            }
            // console.log(data)
            chartData = data;
            var chart = AmCharts.makeChart("candle-api", {
                "type": "stock",
                "theme": "light",

                "categoryAxesSettings": {
                    "minPeriod": "hh"
                },

                "glueToTheEnd": true,

                "dataSets": [{
                    "fieldMappings": [{
                        "fromField": "open",
                        "toField": "open"
                    }, {
                        "fromField": "close",
                        "toField": "close"
                    }, {
                        "fromField": "high",
                        "toField": "high"
                    }, {
                        "fromField": "low",
                        "toField": "low"
                    }, {
                        "fromField": "volume",
                        "toField": "volume"
                    }, {
                        "fromField": "value",
                        "toField": "value"
                    }],

                    "color": "#7f8da9",
                    "dataProvider": chartData,
                    "title": candle_bitfinex_title,
                    "categoryField": "date"
                }],

                "panels": [{
                    "title": "Value",
                    "showCategoryAxis": false,
                    "percentHeight": 70,
                    "valueAxes": [{
                        "dashLength": 5
                    }],

                    "categoryAxis": {
                        "dashLength": 5
                    },

                    "stockGraphs": [{
                        "type": "candlestick",
                        "id": "g1",
                        "openField": "open",
                        "closeField": "close",
                        "highField": "high",
                        "lowField": "low",
                        "valueField": "close",
                        "lineColor": "#7f8da9",
                        "fillColors": "#7f8da9",
                        "negativeLineColor": "#db4c3c",
                        "negativeFillColors": "#db4c3c",
                        "fillAlphas": 1,
                        "useDataSetColors": false,
                        "comparable": true,
                        "compareField": "value",
                        "showBalloon": false
                    }],

                    "stockLegend": {
                        "valueTextRegular": undefined,
                        "periodValueTextComparing": "[[percents.value.average]]%"
                    }
                },

                    {
                        "title": "Volume",
                        "percentHeight": 30,
                        "marginTop": 1,
                        "showCategoryAxis": true,
                        "valueAxes": [{
                            "dashLength": 5
                        }],

                        "categoryAxis": {
                            "dashLength": 5
                        },

                        "stockGraphs": [{
                            "valueField": "volume",
                            "type": "column",
                            "showBalloon": false,
                            "fillAlphas": 1
                        }],

                        "stockLegend": {
                            "markerType": "none",
                            "markerSize": 0,
                            "labelText": "",
                            "periodValueTextRegular": "[[value.close]]"
                        }
                    }
                ],

                "chartScrollbarSettings": {
                    "graph": "g1",
                    "graphType": "line",
                    "usePeriod": "hh"
                },

                "periodSelector": {
                    "position": "top",
                    "dateFormat": "YYYY-MM-DD JJ:NN",
                    "inputFieldWidth": 150,
                    "inputFieldsEnabled": false,
                    "periods": [{
                        "period": "hh",
                        "count": 24,
                        "label": "24 Hours",
                        "selected": true

                    }, {
                        "period": "hh",
                        "count": 48,
                        "label": "48 Hours"
                    }, {
                        "period": "DD",
                        "count": 30,
                        "label": "1 Month"
                    }, {
                        "period": "DD",
                        "count": 60,
                        "label": "2 Months"
                    }, {
                        "period": "MAX",
                        "label": "MAX"
                    }]
                }
            });

        }, error: function () {
            alert("wrong in candle2");
        }
    })
}
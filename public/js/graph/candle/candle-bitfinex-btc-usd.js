chartData = []

function realtimecandel() {
    $.ajax({
        type: 'POST',
        url: "http://localhost:8080/data/candleBitfinex",
        data: {},
        success: function (data) {

            for (var i = 0; i < data.length; i++) {
                var time = data[i].date;
                time = new Date(time);
                data[i].date = time
            }
            // console.log(data)
            chartData = data;
            var chart = AmCharts.makeChart("candle-bitfinex-btc-usd", {
                "type": "stock",
                "theme": "light",

                "categoryAxesSettings": {
                    "minPeriod": "hh"
                },

                "glueToTheEnd": true,

                "dataSets": [{
                    "fieldMappings": [{
                        "fromField": "open",
                        "toField": "open"
                    }, {
                        "fromField": "close",
                        "toField": "close"
                    }, {
                        "fromField": "high",
                        "toField": "high"
                    }, {
                        "fromField": "low",
                        "toField": "low"
                    }, {
                        "fromField": "volume",
                        "toField": "volume"
                    }, {
                        "fromField": "value",
                        "toField": "value"
                    }],

                    "color": "#7f8da9",
                    "dataProvider": chartData,
                    "title": "BITFINEX BTC/USD",
                    "categoryField": "date"
                }],

                "panels": [{
                    "title": "Value",
                    "showCategoryAxis": false,
                    "percentHeight": 70,
                    "valueAxes": [{
                        "dashLength": 5
                    }],

                    "categoryAxis": {
                        "dashLength": 5
                    },

                    "stockGraphs": [{
                        "type": "candlestick",
                        "id": "g1",
                        "openField": "open",
                        "closeField": "close",
                        "highField": "high",
                        "lowField": "low",
                        "valueField": "close",
                        "lineColor": "#7f8da9",
                        "fillColors": "#7f8da9",
                        "negativeLineColor": "#db4c3c",
                        "negativeFillColors": "#db4c3c",
                        "fillAlphas": 1,
                        "useDataSetColors": false,
                        "comparable": true,
                        "compareField": "value",
                        "showBalloon": false
                    }],

                    "stockLegend": {
                        "valueTextRegular": undefined,
                        "periodValueTextComparing": "[[percents.value.average]]%"
                    }
                },

                    {
                        "title": "Volume",
                        "percentHeight": 30,
                        "marginTop": 1,
                        "showCategoryAxis": true,
                        "valueAxes": [{
                            "dashLength": 5
                        }],

                        "categoryAxis": {
                            "dashLength": 5
                        },

                        "stockGraphs": [{
                            "valueField": "volume",
                            "type": "column",
                            "showBalloon": false,
                            "fillAlphas": 1
                        }],

                        "stockLegend": {
                            "markerType": "none",
                            "markerSize": 0,
                            "labelText": "",
                            "periodValueTextRegular": "[[value.close]]"
                        }
                    }
                ],

                "chartScrollbarSettings": {
                    "graph": "g1",
                    "graphType": "line",
                    "usePeriod": "hh"
                },

                "periodSelector": {
                    "position": "top",
                    "dateFormat": "YYYY-MM-DD JJ:NN",
                    "inputFieldWidth": 150,
                    "inputFieldsEnabled": false,
                    "periods": [{
                        "period": "hh",
                        "count": 24,
                        "label": "24 Hours",
                        "selected": true

                    }, {
                        "period": "hh",
                        "count": 48,
                        "label": "48 Hours"
                    }, {
                        "period": "DD",
                        "count": 30,
                        "label": "1 Month"
                    }, {
                        "period": "DD",
                        "count": 60,
                        "label": "2 Months"
                    }, {
                        "period": "MAX",
                        "label": "MAX"
                    }]
                }
            });

        }, error: function () {
            alert("wrong in candle2");
        }
    })
}
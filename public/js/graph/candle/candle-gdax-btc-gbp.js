/**
 * Created by MCIAKTE on 2018/6/6.
 */
chartData = []

//realtimecandel();
function realtimecandel() {
    $.ajax({
        type: 'POST',
        url: "http://localhost:8080/data/candelGDAX_GBP",
        data:{},
        success: function (data) {
            var timestamp = data[data.length-1].date;
            for(var i =0 ; i < data.length; i++){
                var time = data[i].date;
                time = time*1000
                time = new Date(time);
                data[i].date = time

            }
            chartData = data

            var chart = AmCharts.makeChart( "candle-gdax-btc-gbp", {
                "type": "stock",
                "theme": "light",

                "categoryAxesSettings": {
                    "minPeriod": "mm"
                },
                "glueToTheEnd": true,
                "dataSets": [ {
                    "fieldMappings": [ {
                        "fromField": "open",
                        "toField": "open"
                    }, {
                        "fromField": "close",
                        "toField": "close"
                    }, {
                        "fromField": "high",
                        "toField": "high"
                    }, {
                        "fromField": "low",
                        "toField": "low"
                    }, {
                        "fromField": "volume",
                        "toField": "volume"
                    }, {
                        "fromField": "value",
                        "toField": "value"
                    } ],

                    "color": "#7f8da9",
                    "dataProvider":chartData ,
                    "title": "GDAX BTC/GBP",
                    "categoryField": "date"
                }

                ],


                "panels": [ {
                    "title": "Value",
                    "showCategoryAxis": false,
                    "percentHeight": 70,
                    "valueAxes": [ {
                        "dashLength": 5
                    } ],

                    "categoryAxis": {
                        "dashLength": 5
                    },

                    "stockGraphs": [ {
                        "type": "candlestick",
                        "id": "g1",
                        "openField": "open",
                        "closeField": "close",
                        "highField": "high",
                        "lowField": "low",
                        "valueField": "close",
                        "lineColor": "#7f8da9",
                        "fillColors": "#7f8da9",
                        "negativeLineColor": "#db4c3c",
                        "negativeFillColors": "#db4c3c",
                        "fillAlphas": 1,
                        "useDataSetColors": false,
                        "comparable": true,
                        "compareField": "value",
                        "showBalloon": false
                    } ],

                    "stockLegend": {
                        "valueTextRegular": undefined,
                        "periodValueTextComparing": "[[percents.value.average]]%"
                    }
                },

                    {
                        "title": "Volume",
                        "percentHeight": 30,
                        "marginTop": 1,
                        "showCategoryAxis": true,
                        "valueAxes": [ {
                            "dashLength": 5
                        } ],

                        "categoryAxis": {
                            "dashLength": 5
                        },

                        "stockGraphs": [ {
                            "valueField": "volume",
                            "type": "column",
                            "showBalloon": false,
                            "fillAlphas": 1
                        } ],

                        "stockLegend": {
                            "markerType": "none",
                            "markerSize": 0,
                            "labelText": "",
                            "periodValueTextRegular": "[[value.close]]"
                        }
                    }
                ],

                "chartScrollbarSettings": {
                    "graph": "g1",
                    "graphType": "line",
                    "usePeriod": "hh"
                },

                "periodSelector": {
                    "position": "top",
                    "dateFormat": "YYYY-MM-DD JJ:NN",
                    "inputFieldWidth": 150,
                    "periods": [ {
                        "period": "hh",
                        "count": 1,
                        "label": "1 hour",
                        "selected": true

                    }, {
                        "period": "hh",
                        "count": 2,
                        "label": "2 hours"
                    }, {
                        "period": "hh",
                        "count": 5,
                        "label": "5 hour"
                    }, {
                        "period": "hh",
                        "count": 12,
                        "label": "12 hours"
                    }, {
                        "period": "MAX",
                        "label": "MAX"
                    } ]
                }
            } );
            var timedata = {
                time:timestamp
            }

            console.log(timedata)
            setInterval( function() {
                $.ajax({
                    // async : false,
                    type: 'POST',
                    url: "http://localhost:8080/data/candelGDAXupdate_GBP",
                    data: timedata,
                    success: function (data) {
                        for(var i =0 ; i < data.length; i++){
                            var time = data[i].date;
                            // console.log(data[i].date);
                            time = time*1000
                            time = new Date(time);
                            // console.log(time);
                            data[i].date = time
                            // console.log(data[i].date)
                        }
                        console.log(data)
                        data = data[0];
                        console.log(data)
                        chart.dataSets[ 0 ].dataProvider.push(data);
                        chart.validateData();
                        timedata.time = timedata.time+60;
                    }, error: function () {
                        alert("wrong");
                    }
                })
            },60000)

        }, error: function () {
            alert("wrong");
        }
    })


}

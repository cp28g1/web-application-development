var value = "";
//alert(1);

function MapData(data){
    var jsonArr = new Array();
    var nodedata = data.nodes;
    var list = [];
    var la;
    var lo;
    var city;
    var countrycode;

    for(var key in nodedata){
        var jsonObj = {};
        list = nodedata[key];
        city = list[6];
        countrycode = list[7];
        la = list[8];
        lo = list[9];
        var title = city+"/"+countrycode;
        jsonObj["zoomLevel"] = 5;
        jsonObj["scale"] = 0.5;
        jsonObj["title"] = title;
        jsonObj["latitude"] = la;
        jsonObj["longitude"] = lo;
        jsonArr.push(jsonObj);
    }
    // console.log(jsonArr)
    return jsonArr;

}

var map = AmCharts.makeChart( "mapdiv", {
    "type": "map",
    "theme": "light",
    "projection": "miller",

    "imagesSettings": {
        "rollOverColor": "#089282",
        "rollOverScale": 3,
        "selectedScale": 3,
        "selectedColor": "#089282",
        "color": "#13564e"
    },

    "areasSettings": {
        "unlistedAreasColor": "#15A892"
    },

    "dataProvider": {
        "map": "worldLow",
        "images":""
    }
} );

//Map real time with setinterval
function drawMap() {
    $.get("https://bitnodes.earn.com/api/v1/snapshots/latest/", function (data, status) {
        value = MapData(data)
        var start = Math.random()*10000;
        value = value.slice(start,start+20)
        map.dataProvider.images = value;
        map.validateData();
        // console.log(value);
    })
}

setInterval(drawMap, 3000);

map.addListener( "positionChanged", updateCustomMarkers );
//Map real time

// this function will take current images on the map and create HTML elements for them
function updateCustomMarkers( event ) {
    // get map object
    var map = event.chart;

    // go through all of the images
    for ( var x in map.dataProvider.images ) {
        // get MapImage object
        var image = map.dataProvider.images[ x ];

        // check if it has corresponding HTML element
        if ( 'undefined' == typeof image.externalElement )
            image.externalElement = createCustomMarker( image );

        // reposition the element accoridng to coordinates
        var xy = map.coordinatesToStageXY( image.longitude, image.latitude );
        image.externalElement.style.top = xy.y + 'px';
        image.externalElement.style.left = xy.x + 'px';
    }
}

// this function creates and returns a new marker element
function createCustomMarker( image ) {
    // create holder
    var holder = document.createElement( 'div' );
    holder.className = 'map-marker';
    holder.title = image.title;
    holder.style.position = 'absolute';

    // maybe add a link to it?
    if ( undefined != image.url ) {
        holder.onclick = function() {
            window.location.href = image.url;
        };
        holder.className += ' map-clickable';
    }

    // create dot
    var dot = document.createElement( 'div' );
    dot.className = 'dot';
    holder.appendChild( dot );

    // create pulse
    var pulse = document.createElement( 'div' );
    pulse.className = 'pulse';
    holder.appendChild( pulse );

    // append the marker to the map container
    image.chart.chartDiv.appendChild( holder );

    return holder;
}

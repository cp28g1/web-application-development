/**
 * Created by MCIAKTE on 2017/5/29.
 */
function drawBarChart(){
    // var v1 = $.get("http://localhost:8080/data/barchartdataanon",function(data,status){
    //         [data.Title,data.Number];
    //
    // var v2 = $.get("http://localhost:8080/data/barchartdatadmin",function(data,status){
    //         [data.Title,data.Number];
    //
    // var v3 = $.get("http://localhost:8080/data/barchartdatabot",function(data,status){
    //         [data.Title,data.Number];
    //
    // var v2 = $.get("http://localhost:8080/data/barchartdatauser",function(data,status){
    //         [data.Title,data.Number];
    //var vt = [v1,v2,v3,v4]
    //         console(v1)




    require.config({
        paths:{
            echarts: './echarts.js'
        }
    });

    require(
        [
            'echarts',
            'echarts/chart/bar'
        ],
        function (ec) {

            var myChart = ec.init(document.getElementById('barchartbox'));

            var option = {
                tooltip: {
                    show: true
                },
                legend: {
                    data:['']
                },
                xAxis : [
                    {
                        type : 'category',
                        data : ["2001","2002","2003","2004","2005","2006"]
                    }
                ],
                yAxis : [
                    {
                        type : 'value'
                    }
                ],
                series : [
                    {
                        "name":"",
                        "type":"bar",
                        "data":[5, 20, 40, 10, 10, 20]
                    }
                ]
            };


            myChart.setOption(option);
        }
);
}
function drawPieChart(){

    require.config({
        paths:{
            echarts: './echarts.js'
        }
    });

    require(
        [
            'echarts',
            'echarts/chart/pie'
        ],

        function (ec) {

            myChart = ec.init(document.getElementById('piechartbox'));

            var option = {
                title : {
                    text: 'PieChart',
                    //subtext: '（Pie Chart）',
                    x:'center'
                },
                tooltip : {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                legend: {
                    orient : 'vertical',
                    x : 'left',
                    data:['part1','part2','part3','part4']
                },
                toolbox: {
                    show : true,
                    feature : {
                        //mark : {show: true},
                        //dataView : {show: true, readOnly: false},
                        restore : {show: true},
                        //saveAsImage : {show: true}
                    }
                },
                calculable : false,
                series : [
                    {
                        name:'Pie Chart',
                        type:'pie',
                        radius : '55%',
                        center: ['50%', '60%'],
                        data:[
                            {value:100, name:'part1'},
                            {value:200, name:'part2'},
                            {value:300, name:'part3'},
                            {value:400, name:'part4'}]
                    }
                ]
            };
            //console(ec)
            myChart.setOption(option);

        }



);

 }
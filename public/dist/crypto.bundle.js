! function(e, t) {
    if ("object" == typeof exports && "object" == typeof module) module.exports = t(require("ReactDOM"), require("ReactHighstock"), require("React"));
    else if ("function" == typeof define && define.amd) define(["ReactDOM", "ReactHighstock", "React"], t);
    else {
        var n = "object" == typeof exports ? t(require("ReactDOM"), require("ReactHighstock"), require("React")) : t(e.ReactDOM, e.ReactHighstock, e.React);
        for (var r in n)("object" == typeof exports ? exports : e)[r] = n[r]
    }
}("undefined" != typeof self ? self : this, function(e, t, n) {
    return function(e) {
        function t(r) {
            if (n[r]) return n[r].exports;
            var o = n[r] = {
                i: r,
                l: !1,
                exports: {}
            };
            return e[r].call(o.exports, o, o.exports, t), o.l = !0, o.exports
        }
        var n = {};
        return t.m = e, t.c = n, t.d = function(e, n, r) {
            t.o(e, n) || Object.defineProperty(e, n, {
                configurable: !1,
                enumerable: !0,
                get: r
            })
        }, t.n = function(e) {
            var n = e && e.__esModule ? function() {
                return e.default
            } : function() {
                return e
            };
            return t.d(n, "a", n), n
        }, t.o = function(e, t) {
            return Object.prototype.hasOwnProperty.call(e, t)
        }, t.p = "", t(t.s = 42)
    }([function(e, t) {
        var n;
        n = function() {
            return this
        }();
        try {
            n = n || Function("return this")() || (0, eval)("this")
        } catch (e) {
            "object" == typeof window && (n = window)
        }
        e.exports = n
    }, function(e, t, n) {
        "use strict";

        function r(e) {
            return "[object Array]" === E.call(e)
        }

        function o(e) {
            return "[object ArrayBuffer]" === E.call(e)
        }

        function i(e) {
            return "undefined" != typeof FormData && e instanceof FormData
        }

        function a(e) {
            return "undefined" != typeof ArrayBuffer && ArrayBuffer.isView ? ArrayBuffer.isView(e) : e && e.buffer && e.buffer instanceof ArrayBuffer
        }

        function s(e) {
            return "string" == typeof e
        }

        function c(e) {
            return "number" == typeof e
        }

        function u(e) {
            return void 0 === e
        }

        function l(e) {
            return null !== e && "object" == typeof e
        }

        function p(e) {
            return "[object Date]" === E.call(e)
        }

        function f(e) {
            return "[object File]" === E.call(e)
        }

        function h(e) {
            return "[object Blob]" === E.call(e)
        }

        function d(e) {
            return "[object Function]" === E.call(e)
        }

        function m(e) {
            return l(e) && d(e.pipe)
        }

        function y(e) {
            return "undefined" != typeof URLSearchParams && e instanceof URLSearchParams
        }

        function g(e) {
            return e.replace(/^\s*/, "").replace(/\s*$/, "")
        }

        function v() {
            return ("undefined" == typeof navigator || "ReactNative" !== navigator.product) && "undefined" != typeof window && "undefined" != typeof document
        }

        function b(e, t) {
            if (null !== e && void 0 !== e)
                if ("object" != typeof e && (e = [e]), r(e))
                    for (var n = 0, o = e.length; n < o; n++) t.call(null, e[n], n, e);
                else
                    for (var i in e) Object.prototype.hasOwnProperty.call(e, i) && t.call(null, e[i], i, e)
        }

        function w() {
            function e(e, n) {
                "object" == typeof t[n] && "object" == typeof e ? t[n] = w(t[n], e) : t[n] = e
            }
            for (var t = {}, n = 0, r = arguments.length; n < r; n++) b(arguments[n], e);
            return t
        }

        function C(e, t, n) {
            return b(t, function(t, r) {
                e[r] = n && "function" == typeof t ? x(t, n) : t
            }), e
        }
        var x = n(22),
            k = n(62),
            E = Object.prototype.toString;
        e.exports = {
            isArray: r,
            isArrayBuffer: o,
            isBuffer: k,
            isFormData: i,
            isArrayBufferView: a,
            isString: s,
            isNumber: c,
            isObject: l,
            isUndefined: u,
            isDate: p,
            isFile: f,
            isBlob: h,
            isFunction: d,
            isStream: m,
            isURLSearchParams: y,
            isStandardBrowserEnv: v,
            forEach: b,
            merge: w,
            extend: C,
            trim: g
        }
    }, function(e, t) {
        e.exports = n
    }, function(e, t, n) {
        e.exports = n(46)()
    }, function(e, t, n) {
        function r(e) {
            if (e) return o(e)
        }

        function o(e) {
            for (var t in r.prototype) e[t] = r.prototype[t];
            return e
        }
        e.exports = r, r.prototype.on = r.prototype.addEventListener = function(e, t) {
            return this._callbacks = this._callbacks || {}, (this._callbacks["$" + e] = this._callbacks["$" + e] || []).push(t), this
        }, r.prototype.once = function(e, t) {
            function n() {
                this.off(e, n), t.apply(this, arguments)
            }
            return n.fn = t, this.on(e, n), this
        }, r.prototype.off = r.prototype.removeListener = r.prototype.removeAllListeners = r.prototype.removeEventListener = function(e, t) {
            if (this._callbacks = this._callbacks || {}, 0 == arguments.length) return this._callbacks = {}, this;
            var n = this._callbacks["$" + e];
            if (!n) return this;
            if (1 == arguments.length) return delete this._callbacks["$" + e], this;
            for (var r, o = 0; o < n.length; o++)
                if ((r = n[o]) === t || r.fn === t) {
                    n.splice(o, 1);
                    break
                }
            return this
        }, r.prototype.emit = function(e) {
            this._callbacks = this._callbacks || {};
            var t = [].slice.call(arguments, 1),
                n = this._callbacks["$" + e];
            if (n) {
                n = n.slice(0);
                for (var r = 0, o = n.length; r < o; ++r) n[r].apply(this, t)
            }
            return this
        }, r.prototype.listeners = function(e) {
            return this._callbacks = this._callbacks || {}, this._callbacks["$" + e] || []
        }, r.prototype.hasListeners = function(e) {
            return !!this.listeners(e).length
        }
    }, function(e, t, n) {
        (function(e) {
            function r(e, n) {
                return n("b" + t.packets[e.type] + e.data.data)
            }

            function o(e, n, r) {
                if (!n) return t.encodeBase64Packet(e, r);
                var o = e.data,
                    i = new Uint8Array(o),
                    a = new Uint8Array(1 + o.byteLength);
                a[0] = v[e.type];
                for (var s = 0; s < i.length; s++) a[s + 1] = i[s];
                return r(a.buffer)
            }

            function i(e, n, r) {
                if (!n) return t.encodeBase64Packet(e, r);
                var o = new FileReader;
                return o.onload = function() {
                    e.data = o.result, t.encodePacket(e, n, !0, r)
                }, o.readAsArrayBuffer(e.data)
            }

            function a(e, n, r) {
                if (!n) return t.encodeBase64Packet(e, r);
                if (g) return i(e, n, r);
                var o = new Uint8Array(1);
                return o[0] = v[e.type], r(new C([o.buffer, e.data]))
            }

            function s(e) {
                try {
                    e = d.decode(e, {
                        strict: !1
                    })
                } catch (e) {
                    return !1
                }
                return e
            }

            function c(e, t, n) {
                for (var r = new Array(e.length), o = h(e.length, n), i = 0; i < e.length; i++) ! function(e, n, o) {
                    t(n, function(t, n) {
                        r[e] = n, o(t, r)
                    })
                }(i, e[i], o)
            }
            var u, l = n(93),
                p = n(29),
                f = n(94),
                h = n(95),
                d = n(96);
            e && e.ArrayBuffer && (u = n(98));
            var m = "undefined" != typeof navigator && /Android/i.test(navigator.userAgent),
                y = "undefined" != typeof navigator && /PhantomJS/i.test(navigator.userAgent),
                g = m || y;
            t.protocol = 3;
            var v = t.packets = {
                    open: 0,
                    close: 1,
                    ping: 2,
                    pong: 3,
                    message: 4,
                    upgrade: 5,
                    noop: 6
                },
                b = l(v),
                w = {
                    type: "error",
                    data: "parser error"
                },
                C = n(99);
            t.encodePacket = function(t, n, i, s) {
                "function" == typeof n && (s = n, n = !1), "function" == typeof i && (s = i, i = null);
                var c = void 0 === t.data ? void 0 : t.data.buffer || t.data;
                if (e.ArrayBuffer && c instanceof ArrayBuffer) return o(t, n, s);
                if (C && c instanceof e.Blob) return a(t, n, s);
                if (c && c.base64) return r(t, s);
                var u = v[t.type];
                return void 0 !== t.data && (u += i ? d.encode(String(t.data), {
                    strict: !1
                }) : String(t.data)), s("" + u)
            }, t.encodeBase64Packet = function(n, r) {
                var o = "b" + t.packets[n.type];
                if (C && n.data instanceof e.Blob) {
                    var i = new FileReader;
                    return i.onload = function() {
                        var e = i.result.split(",")[1];
                        r(o + e)
                    }, i.readAsDataURL(n.data)
                }
                var a;
                try {
                    a = String.fromCharCode.apply(null, new Uint8Array(n.data))
                } catch (e) {
                    for (var s = new Uint8Array(n.data), c = new Array(s.length), u = 0; u < s.length; u++) c[u] = s[u];
                    a = String.fromCharCode.apply(null, c)
                }
                return o += e.btoa(a), r(o)
            }, t.decodePacket = function(e, n, r) {
                if (void 0 === e) return w;
                if ("string" == typeof e) {
                    if ("b" === e.charAt(0)) return t.decodeBase64Packet(e.substr(1), n);
                    if (r && !1 === (e = s(e))) return w;
                    var o = e.charAt(0);
                    return Number(o) == o && b[o] ? e.length > 1 ? {
                        type: b[o],
                        data: e.substring(1)
                    } : {
                        type: b[o]
                    } : w
                }
                var i = new Uint8Array(e),
                    o = i[0],
                    a = f(e, 1);
                return C && "blob" === n && (a = new C([a])), {
                    type: b[o],
                    data: a
                }
            }, t.decodeBase64Packet = function(e, t) {
                var n = b[e.charAt(0)];
                if (!u) return {
                    type: n,
                    data: {
                        base64: !0,
                        data: e.substr(1)
                    }
                };
                var r = u.decode(e.substr(1));
                return "blob" === t && C && (r = new C([r])), {
                    type: n,
                    data: r
                }
            }, t.encodePayload = function(e, n, r) {
                function o(e) {
                    return e.length + ":" + e
                }

                function i(e, r) {
                    t.encodePacket(e, !!a && n, !1, function(e) {
                        r(null, o(e))
                    })
                }
                "function" == typeof n && (r = n, n = null);
                var a = p(e);
                return n && a ? C && !g ? t.encodePayloadAsBlob(e, r) : t.encodePayloadAsArrayBuffer(e, r) : e.length ? void c(e, i, function(e, t) {
                    return r(t.join(""))
                }) : r("0:")
            }, t.decodePayload = function(e, n, r) {
                if ("string" != typeof e) return t.decodePayloadAsBinary(e, n, r);
                "function" == typeof n && (r = n, n = null);
                var o;
                if ("" === e) return r(w, 0, 1);
                for (var i, a, s = "", c = 0, u = e.length; c < u; c++) {
                    var l = e.charAt(c);
                    if (":" === l) {
                        if ("" === s || s != (i = Number(s))) return r(w, 0, 1);
                        if (a = e.substr(c + 1, i), s != a.length) return r(w, 0, 1);
                        if (a.length) {
                            if (o = t.decodePacket(a, n, !1), w.type === o.type && w.data === o.data) return r(w, 0, 1);
                            if (!1 === r(o, c + i, u)) return
                        }
                        c += i, s = ""
                    } else s += l
                }
                return "" !== s ? r(w, 0, 1) : void 0
            }, t.encodePayloadAsArrayBuffer = function(e, n) {
                function r(e, n) {
                    t.encodePacket(e, !0, !0, function(e) {
                        return n(null, e)
                    })
                }
                if (!e.length) return n(new ArrayBuffer(0));
                c(e, r, function(e, t) {
                    var r = t.reduce(function(e, t) {
                            var n;
                            return n = "string" == typeof t ? t.length : t.byteLength, e + n.toString().length + n + 2
                        }, 0),
                        o = new Uint8Array(r),
                        i = 0;
                    return t.forEach(function(e) {
                        var t = "string" == typeof e,
                            n = e;
                        if (t) {
                            for (var r = new Uint8Array(e.length), a = 0; a < e.length; a++) r[a] = e.charCodeAt(a);
                            n = r.buffer
                        }
                        o[i++] = t ? 0 : 1;
                        for (var s = n.byteLength.toString(), a = 0; a < s.length; a++) o[i++] = parseInt(s[a]);
                        o[i++] = 255;
                        for (var r = new Uint8Array(n), a = 0; a < r.length; a++) o[i++] = r[a]
                    }), n(o.buffer)
                })
            }, t.encodePayloadAsBlob = function(e, n) {
                function r(e, n) {
                    t.encodePacket(e, !0, !0, function(e) {
                        var t = new Uint8Array(1);
                        if (t[0] = 1, "string" == typeof e) {
                            for (var r = new Uint8Array(e.length), o = 0; o < e.length; o++) r[o] = e.charCodeAt(o);
                            e = r.buffer, t[0] = 0
                        }
                        for (var i = e instanceof ArrayBuffer ? e.byteLength : e.size, a = i.toString(), s = new Uint8Array(a.length + 1), o = 0; o < a.length; o++) s[o] = parseInt(a[o]);
                        if (s[a.length] = 255, C) {
                            var c = new C([t.buffer, s.buffer, e]);
                            n(null, c)
                        }
                    })
                }
                c(e, r, function(e, t) {
                    return n(new C(t))
                })
            }, t.decodePayloadAsBinary = function(e, n, r) {
                "function" == typeof n && (r = n, n = null);
                for (var o = e, i = []; o.byteLength > 0;) {
                    for (var a = new Uint8Array(o), s = 0 === a[0], c = "", u = 1; 255 !== a[u]; u++) {
                        if (c.length > 310) return r(w, 0, 1);
                        c += a[u]
                    }
                    o = f(o, 2 + c.length), c = parseInt(c);
                    var l = f(o, 0, c);
                    if (s) try {
                        l = String.fromCharCode.apply(null, new Uint8Array(l))
                    } catch (e) {
                        var p = new Uint8Array(l);
                        l = "";
                        for (var u = 0; u < p.length; u++) l += String.fromCharCode(p[u])
                    }
                    i.push(l), o = f(o, c)
                }
                var h = i.length;
                i.forEach(function(e, o) {
                    r(t.decodePacket(e, n, !0), o, h)
                })
            }
        }).call(t, n(0))
    }, function(e, t) {
        function n() {
            throw new Error("setTimeout has not been defined")
        }

        function r() {
            throw new Error("clearTimeout has not been defined")
        }

        function o(e) {
            if (l === setTimeout) return setTimeout(e, 0);
            if ((l === n || !l) && setTimeout) return l = setTimeout, setTimeout(e, 0);
            try {
                return l(e, 0)
            } catch (t) {
                try {
                    return l.call(null, e, 0)
                } catch (t) {
                    return l.call(this, e, 0)
                }
            }
        }

        function i(e) {
            if (p === clearTimeout) return clearTimeout(e);
            if ((p === r || !p) && clearTimeout) return p = clearTimeout, clearTimeout(e);
            try {
                return p(e)
            } catch (t) {
                try {
                    return p.call(null, e)
                } catch (t) {
                    return p.call(this, e)
                }
            }
        }

        function a() {
            m && h && (m = !1, h.length ? d = h.concat(d) : y = -1, d.length && s())
        }

        function s() {
            if (!m) {
                var e = o(a);
                m = !0;
                for (var t = d.length; t;) {
                    for (h = d, d = []; ++y < t;) h && h[y].run();
                    y = -1, t = d.length
                }
                h = null, m = !1, i(e)
            }
        }

        function c(e, t) {
            this.fun = e, this.array = t
        }

        function u() {}
        var l, p, f = e.exports = {};
        ! function() {
            try {
                l = "function" == typeof setTimeout ? setTimeout : n
            } catch (e) {
                l = n
            }
            try {
                p = "function" == typeof clearTimeout ? clearTimeout : r
            } catch (e) {
                p = r
            }
        }();
        var h, d = [],
            m = !1,
            y = -1;
        f.nextTick = function(e) {
            var t = new Array(arguments.length - 1);
            if (arguments.length > 1)
                for (var n = 1; n < arguments.length; n++) t[n - 1] = arguments[n];
            d.push(new c(e, t)), 1 !== d.length || m || o(s)
        }, c.prototype.run = function() {
            this.fun.apply(null, this.array)
        }, f.title = "browser", f.browser = !0, f.env = {}, f.argv = [], f.version = "", f.versions = {}, f.on = u, f.addListener = u, f.once = u, f.off = u, f.removeListener = u, f.removeAllListeners = u, f.emit = u, f.prependListener = u, f.prependOnceListener = u, f.listeners = function(e) {
            return []
        }, f.binding = function(e) {
            throw new Error("process.binding is not supported")
        }, f.cwd = function() {
            return "/"
        }, f.chdir = function(e) {
            throw new Error("process.chdir is not supported")
        }, f.umask = function() {
            return 0
        }
    }, function(e, t, n) {
        (function(r) {
            function o() {
                return !("undefined" == typeof window || !window.process || "renderer" !== window.process.type) || "undefined" != typeof document && document.documentElement && document.documentElement.style && document.documentElement.style.WebkitAppearance || "undefined" != typeof window && window.console && (window.console.firebug || window.console.exception && window.console.table) || "undefined" != typeof navigator && navigator.userAgent && navigator.userAgent.toLowerCase().match(/firefox\/(\d+)/) && parseInt(RegExp.$1, 10) >= 31 || "undefined" != typeof navigator && navigator.userAgent && navigator.userAgent.toLowerCase().match(/applewebkit\/(\d+)/)
            }

            function i(e) {
                var n = this.useColors;
                if (e[0] = (n ? "%c" : "") + this.namespace + (n ? " %c" : " ") + e[0] + (n ? "%c " : " ") + "+" + t.humanize(this.diff), n) {
                    var r = "color: " + this.color;
                    e.splice(1, 0, r, "color: inherit");
                    var o = 0,
                        i = 0;
                    e[0].replace(/%[a-zA-Z%]/g, function(e) {
                        "%%" !== e && (o++, "%c" === e && (i = o))
                    }), e.splice(i, 0, r)
                }
            }

            function a() {
                return "object" == typeof console && console.log && Function.prototype.apply.call(console.log, console, arguments)
            }

            function s(e) {
                try {
                    null == e ? t.storage.removeItem("debug") : t.storage.debug = e
                } catch (e) {}
            }

            function c() {
                var e;
                try {
                    e = t.storage.debug
                } catch (e) {}
                return !e && void 0 !== r && "env" in r && (e = r.env.DEBUG), e
            }
            t = e.exports = n(84), t.log = a, t.formatArgs = i, t.save = s, t.load = c, t.useColors = o, t.storage = "undefined" != typeof chrome && void 0 !== chrome.storage ? chrome.storage.local : function() {
                try {
                    return window.localStorage
                } catch (e) {}
            }(), t.colors = ["lightseagreen", "forestgreen", "goldenrod", "dodgerblue", "darkorchid", "crimson"], t.formatters.j = function(e) {
                try {
                    return JSON.stringify(e)
                } catch (e) {
                    return "[UnexpectedJSONParseError]: " + e.message
                }
            }, t.enable(c())
        }).call(t, n(6))
    }, function(e, t) {
        t.encode = function(e) {
            var t = "";
            for (var n in e) e.hasOwnProperty(n) && (t.length && (t += "&"), t += encodeURIComponent(n) + "=" + encodeURIComponent(e[n]));
            return t
        }, t.decode = function(e) {
            for (var t = {}, n = e.split("&"), r = 0, o = n.length; r < o; r++) {
                var i = n[r].split("=");
                t[decodeURIComponent(i[0])] = decodeURIComponent(i[1])
            }
            return t
        }
    }, function(e, t) {
        e.exports = function(e, t) {
            var n = function() {};
            n.prototype = t.prototype, e.prototype = new n, e.prototype.constructor = e
        }
    }, function(e, t, n) {
        (function(r) {
            function o() {
                return !("undefined" == typeof window || !window.process || "renderer" !== window.process.type) || ("undefined" == typeof navigator || !navigator.userAgent || !navigator.userAgent.toLowerCase().match(/(edge|trident)\/(\d+)/)) && ("undefined" != typeof document && document.documentElement && document.documentElement.style && document.documentElement.style.WebkitAppearance || "undefined" != typeof window && window.console && (window.console.firebug || window.console.exception && window.console.table) || "undefined" != typeof navigator && navigator.userAgent && navigator.userAgent.toLowerCase().match(/firefox\/(\d+)/) && parseInt(RegExp.$1, 10) >= 31 || "undefined" != typeof navigator && navigator.userAgent && navigator.userAgent.toLowerCase().match(/applewebkit\/(\d+)/))
            }

            function i(e) {
                var n = this.useColors;
                if (e[0] = (n ? "%c" : "") + this.namespace + (n ? " %c" : " ") + e[0] + (n ? "%c " : " ") + "+" + t.humanize(this.diff), n) {
                    var r = "color: " + this.color;
                    e.splice(1, 0, r, "color: inherit");
                    var o = 0,
                        i = 0;
                    e[0].replace(/%[a-zA-Z%]/g, function(e) {
                        "%%" !== e && (o++, "%c" === e && (i = o))
                    }), e.splice(i, 0, r)
                }
            }

            function a() {
                return "object" == typeof console && console.log && Function.prototype.apply.call(console.log, console, arguments)
            }

            function s(e) {
                try {
                    null == e ? t.storage.removeItem("debug") : t.storage.debug = e
                } catch (e) {}
            }

            function c() {
                var e;
                try {
                    e = t.storage.debug
                } catch (e) {}
                return !e && void 0 !== r && "env" in r && (e = r.env.DEBUG), e
            }
            t = e.exports = n(100), t.log = a, t.formatArgs = i, t.save = s, t.load = c, t.useColors = o, t.storage = "undefined" != typeof chrome && void 0 !== chrome.storage ? chrome.storage.local : function() {
                try {
                    return window.localStorage
                } catch (e) {}
            }(), t.colors = ["#0000CC", "#0000FF", "#0033CC", "#0033FF", "#0066CC", "#0066FF", "#0099CC", "#0099FF", "#00CC00", "#00CC33", "#00CC66", "#00CC99", "#00CCCC", "#00CCFF", "#3300CC", "#3300FF", "#3333CC", "#3333FF", "#3366CC", "#3366FF", "#3399CC", "#3399FF", "#33CC00", "#33CC33", "#33CC66", "#33CC99", "#33CCCC", "#33CCFF", "#6600CC", "#6600FF", "#6633CC", "#6633FF", "#66CC00", "#66CC33", "#9900CC", "#9900FF", "#9933CC", "#9933FF", "#99CC00", "#99CC33", "#CC0000", "#CC0033", "#CC0066", "#CC0099", "#CC00CC", "#CC00FF", "#CC3300", "#CC3333", "#CC3366", "#CC3399", "#CC33CC", "#CC33FF", "#CC6600", "#CC6633", "#CC9900", "#CC9933", "#CCCC00", "#CCCC33", "#FF0000", "#FF0033", "#FF0066", "#FF0099", "#FF00CC", "#FF00FF", "#FF3300", "#FF3333", "#FF3366", "#FF3399", "#FF33CC", "#FF33FF", "#FF6600", "#FF6633", "#FF9900", "#FF9933", "#FFCC00", "#FFCC33"], t.formatters.j = function(e) {
                try {
                    return JSON.stringify(e)
                } catch (e) {
                    return "[UnexpectedJSONParseError]: " + e.message
                }
            }, t.enable(c())
        }).call(t, n(6))
    }, function(t, n) {
        t.exports = e
    }, function(e, t, n) {
        "use strict";
        var r = n(60),
            o = n.n(r);
        t.a = o.a.create({
            baseURL: "https://coincap.io/"
        })
    }, function(e, t, n) {
        "use strict";
        (function(t) {
            function r(e, t) {
                !o.isUndefined(e) && o.isUndefined(e["Content-Type"]) && (e["Content-Type"] = t)
            }
            var o = n(1),
                i = n(64),
                a = {
                    "Content-Type": "application/x-www-form-urlencoded"
                },
                s = {
                    adapter: function() {
                        var e;
                        return "undefined" != typeof XMLHttpRequest ? e = n(23) : void 0 !== t && (e = n(23)), e
                    }(),
                    transformRequest: [function(e, t) {
                        return i(t, "Content-Type"), o.isFormData(e) || o.isArrayBuffer(e) || o.isBuffer(e) || o.isStream(e) || o.isFile(e) || o.isBlob(e) ? e : o.isArrayBufferView(e) ? e.buffer : o.isURLSearchParams(e) ? (r(t, "application/x-www-form-urlencoded;charset=utf-8"), e.toString()) : o.isObject(e) ? (r(t, "application/json;charset=utf-8"), JSON.stringify(e)) : e
                    }],
                    transformResponse: [function(e) {
                        if ("string" == typeof e) try {
                            e = JSON.parse(e)
                        } catch (e) {}
                        return e
                    }],
                    timeout: 0,
                    xsrfCookieName: "XSRF-TOKEN",
                    xsrfHeaderName: "X-XSRF-TOKEN",
                    maxContentLength: -1,
                    validateStatus: function(e) {
                        return e >= 200 && e < 300
                    }
                };
            s.headers = {
                common: {
                    Accept: "application/json, text/plain, */*"
                }
            }, o.forEach(["delete", "get", "head"], function(e) {
                s.headers[e] = {}
            }), o.forEach(["post", "put", "patch"], function(e) {
                s.headers[e] = o.merge(a)
            }), e.exports = s
        }).call(t, n(6))
    }, function(e, t, n) {
        "use strict";
        n.d(t, "c", function() {
            return s
        }), n.d(t, "b", function() {
            return c
        });
        var r = n(79),
            o = n.n(r),
            i = n(12),
            a = n(80);
        o.a.fetch = function() {
            return Object(i.a)("exchange_rates").then(function(e) {
                var t = e.data;
                return o.a.base = t.base, o.a.settings.from = t.base, o.a.rates = t.rates, o.a.symbols = t.symbols, t.rates
            })
        };
        var s = function(e) {
                for (var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 3, n = ["k", "M", "G", "T", "P", "E", "Z", "Y"], r = void 0, o = n.length - 1; o >= 0; o--)
                    if (r = Math.pow(1e3, o + 1), e <= -r || e >= r) return a.a.toFixed(e / r, t) + n[o];
                return e
            },
            c = function(e) {
                var t = arguments.length > 1 && void 0 !== arguments[1] ? arguments[1] : 3,
                    n = arguments[2];
                return a.a.formatMoney(o()(e).to(n), o.a.symbols[n] && {
                    format: "%s %v",
                    precision: t,
                    symbol: o.a.symbols[n]
                } || n, t)
            };
        a.a.formatNumber, a.a.formatColumn, t.a = o.a
    }, function(e, t) {
        function n(e) {
            if (e = String(e), !(e.length > 100)) {
                var t = /^((?:\d+)?\.?\d+) *(milliseconds?|msecs?|ms|seconds?|secs?|s|minutes?|mins?|m|hours?|hrs?|h|days?|d|years?|yrs?|y)?$/i.exec(e);
                if (t) {
                    var n = parseFloat(t[1]);
                    switch ((t[2] || "ms").toLowerCase()) {
                        case "years":
                        case "year":
                        case "yrs":
                        case "yr":
                        case "y":
                            return n * l;
                        case "days":
                        case "day":
                        case "d":
                            return n * u;
                        case "hours":
                        case "hour":
                        case "hrs":
                        case "hr":
                        case "h":
                            return n * c;
                        case "minutes":
                        case "minute":
                        case "mins":
                        case "min":
                        case "m":
                            return n * s;
                        case "seconds":
                        case "second":
                        case "secs":
                        case "sec":
                        case "s":
                            return n * a;
                        case "milliseconds":
                        case "millisecond":
                        case "msecs":
                        case "msec":
                        case "ms":
                            return n;
                        default:
                            return
                    }
                }
            }
        }

        function r(e) {
            return e >= u ? Math.round(e / u) + "d" : e >= c ? Math.round(e / c) + "h" : e >= s ? Math.round(e / s) + "m" : e >= a ? Math.round(e / a) + "s" : e + "ms"
        }

        function o(e) {
            return i(e, u, "day") || i(e, c, "hour") || i(e, s, "minute") || i(e, a, "second") || e + " ms"
        }

        function i(e, t, n) {
            if (!(e < t)) return e < 1.5 * t ? Math.floor(e / t) + " " + n : Math.ceil(e / t) + " " + n + "s"
        }
        var a = 1e3,
            s = 60 * a,
            c = 60 * s,
            u = 24 * c,
            l = 365.25 * u;
        e.exports = function(e, t) {
            t = t || {};
            var i = typeof e;
            if ("string" === i && e.length > 0) return n(e);
            if ("number" === i && !1 === isNaN(e)) return t.long ? o(e) : r(e);
            throw new Error("val is not a non-empty string or a valid number. val=" + JSON.stringify(e))
        }
    }, function(e, t, n) {
        function r() {}

        function o(e) {
            var n = "" + e.type;
            return t.BINARY_EVENT !== e.type && t.BINARY_ACK !== e.type || (n += e.attachments + "-"), e.nsp && "/" !== e.nsp && (n += e.nsp + ","), null != e.id && (n += e.id), null != e.data && (n += JSON.stringify(e.data)), p("encoded %j as %s", e, n), n
        }

        function i(e, t) {
            function n(e) {
                var n = d.deconstructPacket(e),
                    r = o(n.packet),
                    i = n.buffers;
                i.unshift(r), t(i)
            }
            d.removeBlobs(e, n)
        }

        function a() {
            this.reconstructor = null
        }

        function s(e) {
            var n = 0,
                r = {
                    type: Number(e.charAt(0))
                };
            if (null == t.types[r.type]) return l("unknown packet type " + r.type);
            if (t.BINARY_EVENT === r.type || t.BINARY_ACK === r.type) {
                for (var o = "";
                     "-" !== e.charAt(++n) && (o += e.charAt(n), n != e.length););
                if (o != Number(o) || "-" !== e.charAt(n)) throw new Error("Illegal attachments");
                r.attachments = Number(o)
            }
            if ("/" === e.charAt(n + 1))
                for (r.nsp = ""; ++n;) {
                    var i = e.charAt(n);
                    if ("," === i) break;
                    if (r.nsp += i, n === e.length) break
                } else r.nsp = "/";
            var a = e.charAt(n + 1);
            if ("" !== a && Number(a) == a) {
                for (r.id = ""; ++n;) {
                    var i = e.charAt(n);
                    if (null == i || Number(i) != i) {
                        --n;
                        break
                    }
                    if (r.id += e.charAt(n), n === e.length) break
                }
                r.id = Number(r.id)
            }
            if (e.charAt(++n)) {
                var s = c(e.substr(n));
                if (!1 === s || r.type !== t.ERROR && !m(s)) return l("invalid payload");
                r.data = s
            }
            return p("decoded %s as %j", e, r), r
        }

        function c(e) {
            try {
                return JSON.parse(e)
            } catch (e) {
                return !1
            }
        }

        function u(e) {
            this.reconPack = e, this.buffers = []
        }

        function l(e) {
            return {
                type: t.ERROR,
                data: "parser error: " + e
            }
        }
        var p = n(85)("socket.io-parser"),
            f = n(4),
            h = n(29),
            d = n(88),
            m = n(30),
            y = n(31);
        t.protocol = 4, t.types = ["CONNECT", "DISCONNECT", "EVENT", "ACK", "ERROR", "BINARY_EVENT", "BINARY_ACK"], t.CONNECT = 0, t.DISCONNECT = 1, t.EVENT = 2, t.ACK = 3, t.ERROR = 4, t.BINARY_EVENT = 5, t.BINARY_ACK = 6, t.Encoder = r, t.Decoder = a, r.prototype.encode = function(e, n) {
            e.type !== t.EVENT && e.type !== t.ACK || !h(e.data) || (e.type = e.type === t.EVENT ? t.BINARY_EVENT : t.BINARY_ACK), p("encoding packet %j", e), t.BINARY_EVENT === e.type || t.BINARY_ACK === e.type ? i(e, n) : n([o(e)])
        }, f(a.prototype), a.prototype.add = function(e) {
            var n;
            if ("string" == typeof e) n = s(e), t.BINARY_EVENT === n.type || t.BINARY_ACK === n.type ? (this.reconstructor = new u(n), 0 === this.reconstructor.reconPack.attachments && this.emit("decoded", n)) : this.emit("decoded", n);
            else {
                if (!y(e) && !e.base64) throw new Error("Unknown type: " + e);
                if (!this.reconstructor) throw new Error("got binary data when not reconstructing a packet");
                (n = this.reconstructor.takeBinaryData(e)) && (this.reconstructor = null, this.emit("decoded", n))
            }
        }, a.prototype.destroy = function() {
            this.reconstructor && this.reconstructor.finishedReconstruction()
        }, u.prototype.takeBinaryData = function(e) {
            if (this.buffers.push(e), this.buffers.length === this.reconPack.attachments) {
                var t = d.reconstructPacket(this.reconPack, this.buffers);
                return this.finishedReconstruction(), t
            }
            return null
        }, u.prototype.finishedReconstruction = function() {
            this.reconPack = null, this.buffers = []
        }
    }, function(e, t, n) {
        (function(t) {
            var r = n(91);
            e.exports = function(e) {
                var n = e.xdomain,
                    o = e.xscheme,
                    i = e.enablesXDR;
                try {
                    if ("undefined" != typeof XMLHttpRequest && (!n || r)) return new XMLHttpRequest
                } catch (e) {}
                try {
                    if ("undefined" != typeof XDomainRequest && !o && i) return new XDomainRequest
                } catch (e) {}
                if (!n) try {
                    return new(t[["Active"].concat("Object").join("X")])("Microsoft.XMLHTTP")
                } catch (e) {}
            }
        }).call(t, n(0))
    }, function(e, t, n) {
        function r(e) {
            this.path = e.path, this.hostname = e.hostname, this.port = e.port, this.secure = e.secure, this.query = e.query, this.timestampParam = e.timestampParam, this.timestampRequests = e.timestampRequests, this.readyState = "", this.agent = e.agent || !1, this.socket = e.socket, this.enablesXDR = e.enablesXDR, this.pfx = e.pfx, this.key = e.key, this.passphrase = e.passphrase, this.cert = e.cert, this.ca = e.ca, this.ciphers = e.ciphers, this.rejectUnauthorized = e.rejectUnauthorized, this.forceNode = e.forceNode, this.extraHeaders = e.extraHeaders, this.localAddress = e.localAddress
        }
        var o = n(5),
            i = n(4);
        e.exports = r, i(r.prototype), r.prototype.onError = function(e, t) {
            var n = new Error(e);
            return n.type = "TransportError", n.description = t, this.emit("error", n), this
        }, r.prototype.open = function() {
            return "closed" !== this.readyState && "" !== this.readyState || (this.readyState = "opening", this.doOpen()), this
        }, r.prototype.close = function() {
            return "opening" !== this.readyState && "open" !== this.readyState || (this.doClose(), this.onClose()), this
        }, r.prototype.send = function(e) {
            if ("open" !== this.readyState) throw new Error("Transport not open");
            this.write(e)
        }, r.prototype.onOpen = function() {
            this.readyState = "open", this.writable = !0, this.emit("open")
        }, r.prototype.onData = function(e) {
            var t = o.decodePacket(e, this.socket.binaryType);
            this.onPacket(t)
        }, r.prototype.onPacket = function(e) {
            this.emit("packet", e)
        }, r.prototype.onClose = function() {
            this.readyState = "closed", this.emit("close")
        }
    }, function(e, t) {
        function n(e) {
            var t = typeof e;
            return null != e && ("object" == t || "function" == t)
        }
        e.exports = n
    }, function(e, t, n) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }), t.default = !("undefined" == typeof window || !window.document || !window.document.createElement), e.exports = t.default
    }, function(e, t, n) {
        "use strict";

        function r(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function o(e) {
            var t = "transition" + e + "Timeout",
                n = "transition" + e;
            return function(e) {
                if (e[n]) {
                    if (null == e[t]) return new Error(t + " wasn't supplied to CSSTransitionGroup: this can cause unreliable animations and won't be supported in a future version of React. See https://fb.me/react-animation-transition-group-timeout for more information.");
                    if ("number" != typeof e[t]) return new Error(t + " must be a number (in milliseconds)")
                }
                return null
            }
        }
        t.__esModule = !0, t.nameShape = void 0, t.transitionTimeout = o;
        var i = n(2),
            a = (r(i), n(3)),
            s = r(a);
        t.nameShape = s.default.oneOfType([s.default.string, s.default.shape({
            enter: s.default.string,
            leave: s.default.string,
            active: s.default.string
        }), s.default.shape({
            enter: s.default.string,
            enterActive: s.default.string,
            leave: s.default.string,
            leaveActive: s.default.string,
            appear: s.default.string,
            appearActive: s.default.string
        })])
    }, function(e, t, n) {
        "use strict";
        e.exports = function(e, t) {
            return function() {
                for (var n = new Array(arguments.length), r = 0; r < n.length; r++) n[r] = arguments[r];
                return e.apply(t, n)
            }
        }
    }, function(e, t, n) {
        "use strict";
        var r = n(1),
            o = n(65),
            i = n(67),
            a = n(68),
            s = n(69),
            c = n(24),
            u = "undefined" != typeof window && window.btoa && window.btoa.bind(window) || n(70);
        e.exports = function(e) {
            return new Promise(function(t, l) {
                var p = e.data,
                    f = e.headers;
                r.isFormData(p) && delete f["Content-Type"];
                var h = new XMLHttpRequest,
                    d = "onreadystatechange",
                    m = !1;
                if ("undefined" == typeof window || !window.XDomainRequest || "withCredentials" in h || s(e.url) || (h = new window.XDomainRequest, d = "onload", m = !0, h.onprogress = function() {}, h.ontimeout = function() {}), e.auth) {
                    var y = e.auth.username || "",
                        g = e.auth.password || "";
                    f.Authorization = "Basic " + u(y + ":" + g)
                }
                if (h.open(e.method.toUpperCase(), i(e.url, e.params, e.paramsSerializer), !0), h.timeout = e.timeout, h[d] = function() {
                    if (h && (4 === h.readyState || m) && (0 !== h.status || h.responseURL && 0 === h.responseURL.indexOf("file:"))) {
                        var n = "getAllResponseHeaders" in h ? a(h.getAllResponseHeaders()) : null,
                            r = e.responseType && "text" !== e.responseType ? h.response : h.responseText,
                            i = {
                                data: r,
                                status: 1223 === h.status ? 204 : h.status,
                                statusText: 1223 === h.status ? "No Content" : h.statusText,
                                headers: n,
                                config: e,
                                request: h
                            };
                        o(t, l, i), h = null
                    }
                }, h.onerror = function() {
                    l(c("Network Error", e, null, h)), h = null
                }, h.ontimeout = function() {
                    l(c("timeout of " + e.timeout + "ms exceeded", e, "ECONNABORTED", h)), h = null
                }, r.isStandardBrowserEnv()) {
                    var v = n(71),
                        b = (e.withCredentials || s(e.url)) && e.xsrfCookieName ? v.read(e.xsrfCookieName) : void 0;
                    b && (f[e.xsrfHeaderName] = b)
                }
                if ("setRequestHeader" in h && r.forEach(f, function(e, t) {
                    void 0 === p && "content-type" === t.toLowerCase() ? delete f[t] : h.setRequestHeader(t, e)
                }), e.withCredentials && (h.withCredentials = !0), e.responseType) try {
                    h.responseType = e.responseType
                } catch (t) {
                    if ("json" !== e.responseType) throw t
                }
                "function" == typeof e.onDownloadProgress && h.addEventListener("progress", e.onDownloadProgress), "function" == typeof e.onUploadProgress && h.upload && h.upload.addEventListener("progress", e.onUploadProgress), e.cancelToken && e.cancelToken.promise.then(function(e) {
                    h && (h.abort(), l(e), h = null)
                }), void 0 === p && (p = null), h.send(p)
            })
        }
    }, function(e, t, n) {
        "use strict";
        var r = n(66);
        e.exports = function(e, t, n, o, i) {
            var a = new Error(e);
            return r(a, t, n, o, i)
        }
    }, function(e, t, n) {
        "use strict";
        e.exports = function(e) {
            return !(!e || !e.__CANCEL__)
        }
    }, function(e, t, n) {
        "use strict";

        function r(e) {
            this.message = e
        }
        r.prototype.toString = function() {
            return "Cancel" + (this.message ? ": " + this.message : "")
        }, r.prototype.__CANCEL__ = !0, e.exports = r
    }, function(e, t, n) {
        "use strict";
        n.d(t, "b", function() {
            return s
        }), n.d(t, "c", function() {
            return c
        });
        var r = n(82),
            o = n.n(r),
            i = o()("//coincap.io", {
                autoConnect: !1
            }),
            a = [],
            s = function(e, t) {
                a[e] = t
            },
            c = function(e) {
                delete a[e]
            };
        i.on("trades", function(e) {
            return a[e.coin] && a[e.coin](e)
        }), t.a = i
    }, function(e, t) {
        var n = /^(?:(?![^:@]+:[^:@\/]*@)(http|https|ws|wss):\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?((?:[a-f0-9]{0,4}:){2,7}[a-f0-9]{0,4}|[^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/,
            r = ["source", "protocol", "authority", "userInfo", "user", "password", "host", "port", "relative", "path", "directory", "file", "query", "anchor"];
        e.exports = function(e) {
            var t = e,
                o = e.indexOf("["),
                i = e.indexOf("]"); - 1 != o && -1 != i && (e = e.substring(0, o) + e.substring(o, i).replace(/:/g, ";") + e.substring(i, e.length));
            for (var a = n.exec(e || ""), s = {}, c = 14; c--;) s[r[c]] = a[c] || "";
            return -1 != o && -1 != i && (s.source = t, s.host = s.host.substring(1, s.host.length - 1).replace(/;/g, ":"), s.authority = s.authority.replace("[", "").replace("]", "").replace(/;/g, ":"), s.ipv6uri = !0), s
        }
    }, function(e, t, n) {
        (function(t) {
            function r(e) {
                if (!e || "object" != typeof e) return !1;
                if (o(e)) {
                    for (var n = 0, i = e.length; n < i; n++)
                        if (r(e[n])) return !0;
                    return !1
                }
                if ("function" == typeof t.Buffer && t.Buffer.isBuffer && t.Buffer.isBuffer(e) || "function" == typeof t.ArrayBuffer && e instanceof ArrayBuffer || a && e instanceof Blob || s && e instanceof File) return !0;
                if (e.toJSON && "function" == typeof e.toJSON && 1 === arguments.length) return r(e.toJSON(), !0);
                for (var c in e)
                    if (Object.prototype.hasOwnProperty.call(e, c) && r(e[c])) return !0;
                return !1
            }
            var o = n(87),
                i = Object.prototype.toString,
                a = "function" == typeof t.Blob || "[object BlobConstructor]" === i.call(t.Blob),
                s = "function" == typeof t.File || "[object FileConstructor]" === i.call(t.File);
            e.exports = r
        }).call(t, n(0))
    }, function(e, t) {
        var n = {}.toString;
        e.exports = Array.isArray || function(e) {
            return "[object Array]" == n.call(e)
        }
    }, function(e, t, n) {
        (function(t) {
            function n(e) {
                return t.Buffer && t.Buffer.isBuffer(e) || t.ArrayBuffer && (e instanceof ArrayBuffer || ArrayBuffer.isView(e))
            }
            e.exports = n
        }).call(t, n(0))
    }, function(e, t, n) {
        function r(e, t) {
            if (!(this instanceof r)) return new r(e, t);
            e && "object" == typeof e && (t = e, e = void 0), t = t || {}, t.path = t.path || "/socket.io", this.nsps = {}, this.subs = [], this.opts = t, this.reconnection(!1 !== t.reconnection), this.reconnectionAttempts(t.reconnectionAttempts || 1 / 0), this.reconnectionDelay(t.reconnectionDelay || 1e3), this.reconnectionDelayMax(t.reconnectionDelayMax || 5e3), this.randomizationFactor(t.randomizationFactor || .5), this.backoff = new f({
                min: this.reconnectionDelay(),
                max: this.reconnectionDelayMax(),
                jitter: this.randomizationFactor()
            }), this.timeout(null == t.timeout ? 2e4 : t.timeout), this.readyState = "closed", this.uri = e, this.connecting = [], this.lastPing = null, this.encoding = !1, this.packetBuffer = [];
            var n = t.parser || s;
            this.encoder = new n.Encoder, this.decoder = new n.Decoder, this.autoConnect = !1 !== t.autoConnect, this.autoConnect && this.open()
        }
        var o = n(89),
            i = n(37),
            a = n(4),
            s = n(16),
            c = n(38),
            u = n(39),
            l = n(7)("socket.io-client:manager"),
            p = n(36),
            f = n(105),
            h = Object.prototype.hasOwnProperty;
        e.exports = r, r.prototype.emitAll = function() {
            this.emit.apply(this, arguments);
            for (var e in this.nsps) h.call(this.nsps, e) && this.nsps[e].emit.apply(this.nsps[e], arguments)
        }, r.prototype.updateSocketIds = function() {
            for (var e in this.nsps) h.call(this.nsps, e) && (this.nsps[e].id = this.generateId(e))
        }, r.prototype.generateId = function(e) {
            return ("/" === e ? "" : e + "#") + this.engine.id
        }, a(r.prototype), r.prototype.reconnection = function(e) {
            return arguments.length ? (this._reconnection = !!e, this) : this._reconnection
        }, r.prototype.reconnectionAttempts = function(e) {
            return arguments.length ? (this._reconnectionAttempts = e, this) : this._reconnectionAttempts
        }, r.prototype.reconnectionDelay = function(e) {
            return arguments.length ? (this._reconnectionDelay = e, this.backoff && this.backoff.setMin(e), this) : this._reconnectionDelay
        }, r.prototype.randomizationFactor = function(e) {
            return arguments.length ? (this._randomizationFactor = e, this.backoff && this.backoff.setJitter(e), this) : this._randomizationFactor
        }, r.prototype.reconnectionDelayMax = function(e) {
            return arguments.length ? (this._reconnectionDelayMax = e, this.backoff && this.backoff.setMax(e), this) : this._reconnectionDelayMax
        }, r.prototype.timeout = function(e) {
            return arguments.length ? (this._timeout = e, this) : this._timeout
        }, r.prototype.maybeReconnectOnOpen = function() {
            !this.reconnecting && this._reconnection && 0 === this.backoff.attempts && this.reconnect()
        }, r.prototype.open = r.prototype.connect = function(e, t) {
            if (l("readyState %s", this.readyState), ~this.readyState.indexOf("open")) return this;
            l("opening %s", this.uri), this.engine = o(this.uri, this.opts);
            var n = this.engine,
                r = this;
            this.readyState = "opening", this.skipReconnect = !1;
            var i = c(n, "open", function() {
                    r.onopen(), e && e()
                }),
                a = c(n, "error", function(t) {
                    if (l("connect_error"), r.cleanup(), r.readyState = "closed", r.emitAll("connect_error", t), e) {
                        var n = new Error("Connection error");
                        n.data = t, e(n)
                    } else r.maybeReconnectOnOpen()
                });
            if (!1 !== this._timeout) {
                var s = this._timeout;
                l("connect attempt will timeout after %d", s);
                var u = setTimeout(function() {
                    l("connect attempt timed out after %d", s), i.destroy(), n.close(), n.emit("error", "timeout"), r.emitAll("connect_timeout", s)
                }, s);
                this.subs.push({
                    destroy: function() {
                        clearTimeout(u)
                    }
                })
            }
            return this.subs.push(i), this.subs.push(a), this
        }, r.prototype.onopen = function() {
            l("open"), this.cleanup(), this.readyState = "open", this.emit("open");
            var e = this.engine;
            this.subs.push(c(e, "data", u(this, "ondata"))), this.subs.push(c(e, "ping", u(this, "onping"))), this.subs.push(c(e, "pong", u(this, "onpong"))), this.subs.push(c(e, "error", u(this, "onerror"))), this.subs.push(c(e, "close", u(this, "onclose"))), this.subs.push(c(this.decoder, "decoded", u(this, "ondecoded")))
        }, r.prototype.onping = function() {
            this.lastPing = new Date, this.emitAll("ping")
        }, r.prototype.onpong = function() {
            this.emitAll("pong", new Date - this.lastPing)
        }, r.prototype.ondata = function(e) {
            this.decoder.add(e)
        }, r.prototype.ondecoded = function(e) {
            this.emit("packet", e)
        }, r.prototype.onerror = function(e) {
            l("error", e), this.emitAll("error", e)
        }, r.prototype.socket = function(e, t) {
            function n() {
                ~p(o.connecting, r) || o.connecting.push(r)
            }
            var r = this.nsps[e];
            if (!r) {
                r = new i(this, e, t), this.nsps[e] = r;
                var o = this;
                r.on("connecting", n), r.on("connect", function() {
                    r.id = o.generateId(e)
                }), this.autoConnect && n()
            }
            return r
        }, r.prototype.destroy = function(e) {
            var t = p(this.connecting, e);
            ~t && this.connecting.splice(t, 1), this.connecting.length || this.close()
        }, r.prototype.packet = function(e) {
            l("writing packet %j", e);
            var t = this;
            e.query && 0 === e.type && (e.nsp += "?" + e.query), t.encoding ? t.packetBuffer.push(e) : (t.encoding = !0, this.encoder.encode(e, function(n) {
                for (var r = 0; r < n.length; r++) t.engine.write(n[r], e.options);
                t.encoding = !1, t.processPacketQueue()
            }))
        }, r.prototype.processPacketQueue = function() {
            if (this.packetBuffer.length > 0 && !this.encoding) {
                var e = this.packetBuffer.shift();
                this.packet(e)
            }
        }, r.prototype.cleanup = function() {
            l("cleanup");
            for (var e = this.subs.length, t = 0; t < e; t++) this.subs.shift().destroy();
            this.packetBuffer = [], this.encoding = !1, this.lastPing = null, this.decoder.destroy()
        }, r.prototype.close = r.prototype.disconnect = function() {
            l("disconnect"), this.skipReconnect = !0, this.reconnecting = !1, "opening" === this.readyState && this.cleanup(), this.backoff.reset(), this.readyState = "closed", this.engine && this.engine.close()
        }, r.prototype.onclose = function(e) {
            l("onclose"), this.cleanup(), this.backoff.reset(), this.readyState = "closed", this.emit("close", e), this._reconnection && !this.skipReconnect && this.reconnect()
        }, r.prototype.reconnect = function() {
            if (this.reconnecting || this.skipReconnect) return this;
            var e = this;
            if (this.backoff.attempts >= this._reconnectionAttempts) l("reconnect failed"), this.backoff.reset(), this.emitAll("reconnect_failed"), this.reconnecting = !1;
            else {
                var t = this.backoff.duration();
                l("will wait %dms before reconnect attempt", t), this.reconnecting = !0;
                var n = setTimeout(function() {
                    e.skipReconnect || (l("attempting reconnect"), e.emitAll("reconnect_attempt", e.backoff.attempts), e.emitAll("reconnecting", e.backoff.attempts), e.skipReconnect || e.open(function(t) {
                        t ? (l("reconnect attempt error"), e.reconnecting = !1, e.reconnect(), e.emitAll("reconnect_error", t.data)) : (l("reconnect success"), e.onreconnect())
                    }))
                }, t);
                this.subs.push({
                    destroy: function() {
                        clearTimeout(n)
                    }
                })
            }
        }, r.prototype.onreconnect = function() {
            var e = this.backoff.attempts;
            this.reconnecting = !1, this.backoff.reset(), this.updateSocketIds(), this.emitAll("reconnect", e)
        }
    }, function(e, t, n) {
        (function(e) {
            function r(t) {
                var n = !1,
                    r = !1,
                    s = !1 !== t.jsonp;
                if (e.location) {
                    var c = "https:" === location.protocol,
                        u = location.port;
                    u || (u = c ? 443 : 80), n = t.hostname !== location.hostname || u !== t.port, r = t.secure !== c
                }
                if (t.xdomain = n, t.xscheme = r, "open" in new o(t) && !t.forceJSONP) return new i(t);
                if (!s) throw new Error("JSONP disabled");
                return new a(t)
            }
            var o = n(17),
                i = n(92),
                a = n(101),
                s = n(102);
            t.polling = r, t.websocket = s
        }).call(t, n(0))
    }, function(e, t, n) {
        function r(e) {
            var t = e && e.forceBase64;
            l && !t || (this.supportsBinary = !1), o.call(this, e)
        }
        var o = n(18),
            i = n(8),
            a = n(5),
            s = n(9),
            c = n(35),
            u = n(10)("engine.io-client:polling");
        e.exports = r;
        var l = function() {
            return null != new(n(17))({
                xdomain: !1
            }).responseType
        }();
        s(r, o), r.prototype.name = "polling", r.prototype.doOpen = function() {
            this.poll()
        }, r.prototype.pause = function(e) {
            function t() {
                u("paused"), n.readyState = "paused", e()
            }
            var n = this;
            if (this.readyState = "pausing", this.polling || !this.writable) {
                var r = 0;
                this.polling && (u("we are currently polling - waiting to pause"), r++, this.once("pollComplete", function() {
                    u("pre-pause polling complete"), --r || t()
                })), this.writable || (u("we are currently writing - waiting to pause"), r++, this.once("drain", function() {
                    u("pre-pause writing complete"), --r || t()
                }))
            } else t()
        }, r.prototype.poll = function() {
            u("polling"), this.polling = !0, this.doPoll(), this.emit("poll")
        }, r.prototype.onData = function(e) {
            var t = this;
            u("polling got data %s", e);
            var n = function(e, n, r) {
                if ("opening" === t.readyState && t.onOpen(), "close" === e.type) return t.onClose(), !1;
                t.onPacket(e)
            };
            a.decodePayload(e, this.socket.binaryType, n), "closed" !== this.readyState && (this.polling = !1, this.emit("pollComplete"), "open" === this.readyState ? this.poll() : u('ignoring poll - transport state "%s"', this.readyState))
        }, r.prototype.doClose = function() {
            function e() {
                u("writing close packet"), t.write([{
                    type: "close"
                }])
            }
            var t = this;
            "open" === this.readyState ? (u("transport open - closing"), e()) : (u("transport not open - deferring close"), this.once("open", e))
        }, r.prototype.write = function(e) {
            var t = this;
            this.writable = !1;
            var n = function() {
                t.writable = !0, t.emit("drain")
            };
            a.encodePayload(e, this.supportsBinary, function(e) {
                t.doWrite(e, n)
            })
        }, r.prototype.uri = function() {
            var e = this.query || {},
                t = this.secure ? "https" : "http",
                n = "";
            return !1 !== this.timestampRequests && (e[this.timestampParam] = c()), this.supportsBinary || e.sid || (e.b64 = 1), e = i.encode(e), this.port && ("https" === t && 443 !== Number(this.port) || "http" === t && 80 !== Number(this.port)) && (n = ":" + this.port), e.length && (e = "?" + e), t + "://" + (-1 !== this.hostname.indexOf(":") ? "[" + this.hostname + "]" : this.hostname) + n + this.path + e
        }
    }, function(e, t, n) {
        "use strict";

        function r(e) {
            var t = "";
            do {
                t = s[e % c] + t, e = Math.floor(e / c)
            } while (e > 0);
            return t
        }

        function o(e) {
            var t = 0;
            for (p = 0; p < e.length; p++) t = t * c + u[e.charAt(p)];
            return t
        }

        function i() {
            var e = r(+new Date);
            return e !== a ? (l = 0, a = e) : e + "." + r(l++)
        }
        for (var a, s = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_".split(""), c = 64, u = {}, l = 0, p = 0; p < c; p++) u[s[p]] = p;
        i.encode = r, i.decode = o, e.exports = i
    }, function(e, t) {
        var n = [].indexOf;
        e.exports = function(e, t) {
            if (n) return e.indexOf(t);
            for (var r = 0; r < e.length; ++r)
                if (e[r] === t) return r;
            return -1
        }
    }, function(e, t, n) {
        function r(e, t, n) {
            this.io = e, this.nsp = t, this.json = this, this.ids = 0, this.acks = {}, this.receiveBuffer = [], this.sendBuffer = [], this.connected = !1, this.disconnected = !0, n && n.query && (this.query = n.query), this.io.autoConnect && this.open()
        }
        var o = n(16),
            i = n(4),
            a = n(104),
            s = n(38),
            c = n(39),
            u = n(7)("socket.io-client:socket"),
            l = n(8);
        e.exports = r;
        var p = {
                connect: 1,
                connect_error: 1,
                connect_timeout: 1,
                connecting: 1,
                disconnect: 1,
                error: 1,
                reconnect: 1,
                reconnect_attempt: 1,
                reconnect_failed: 1,
                reconnect_error: 1,
                reconnecting: 1,
                ping: 1,
                pong: 1
            },
            f = i.prototype.emit;
        i(r.prototype), r.prototype.subEvents = function() {
            if (!this.subs) {
                var e = this.io;
                this.subs = [s(e, "open", c(this, "onopen")), s(e, "packet", c(this, "onpacket")), s(e, "close", c(this, "onclose"))]
            }
        }, r.prototype.open = r.prototype.connect = function() {
            return this.connected ? this : (this.subEvents(), this.io.open(), "open" === this.io.readyState && this.onopen(), this.emit("connecting"), this)
        }, r.prototype.send = function() {
            var e = a(arguments);
            return e.unshift("message"), this.emit.apply(this, e), this
        }, r.prototype.emit = function(e) {
            if (p.hasOwnProperty(e)) return f.apply(this, arguments), this;
            var t = a(arguments),
                n = {
                    type: o.EVENT,
                    data: t
                };
            return n.options = {}, n.options.compress = !this.flags || !1 !== this.flags.compress, "function" == typeof t[t.length - 1] && (u("emitting packet with ack id %d", this.ids), this.acks[this.ids] = t.pop(), n.id = this.ids++), this.connected ? this.packet(n) : this.sendBuffer.push(n), delete this.flags, this
        }, r.prototype.packet = function(e) {
            e.nsp = this.nsp, this.io.packet(e)
        }, r.prototype.onopen = function() {
            if (u("transport is open - connecting"), "/" !== this.nsp)
                if (this.query) {
                    var e = "object" == typeof this.query ? l.encode(this.query) : this.query;
                    u("sending connect packet with query %s", e), this.packet({
                        type: o.CONNECT,
                        query: e
                    })
                } else this.packet({
                    type: o.CONNECT
                })
        }, r.prototype.onclose = function(e) {
            u("close (%s)", e), this.connected = !1, this.disconnected = !0, delete this.id, this.emit("disconnect", e)
        }, r.prototype.onpacket = function(e) {
            if (e.nsp === this.nsp) switch (e.type) {
                case o.CONNECT:
                    this.onconnect();
                    break;
                case o.EVENT:
                case o.BINARY_EVENT:
                    this.onevent(e);
                    break;
                case o.ACK:
                case o.BINARY_ACK:
                    this.onack(e);
                    break;
                case o.DISCONNECT:
                    this.ondisconnect();
                    break;
                case o.ERROR:
                    this.emit("error", e.data)
            }
        }, r.prototype.onevent = function(e) {
            var t = e.data || [];
            u("emitting event %j", t), null != e.id && (u("attaching ack callback to event"), t.push(this.ack(e.id))), this.connected ? f.apply(this, t) : this.receiveBuffer.push(t)
        }, r.prototype.ack = function(e) {
            var t = this,
                n = !1;
            return function() {
                if (!n) {
                    n = !0;
                    var r = a(arguments);
                    u("sending ack %j", r), t.packet({
                        type: o.ACK,
                        id: e,
                        data: r
                    })
                }
            }
        }, r.prototype.onack = function(e) {
            var t = this.acks[e.id];
            "function" == typeof t ? (u("calling ack %s with %j", e.id, e.data), t.apply(this, e.data), delete this.acks[e.id]) : u("bad ack %s", e.id)
        }, r.prototype.onconnect = function() {
            this.connected = !0, this.disconnected = !1, this.emit("connect"), this.emitBuffered()
        }, r.prototype.emitBuffered = function() {
            var e;
            for (e = 0; e < this.receiveBuffer.length; e++) f.apply(this, this.receiveBuffer[e]);
            for (this.receiveBuffer = [], e = 0; e < this.sendBuffer.length; e++) this.packet(this.sendBuffer[e]);
            this.sendBuffer = []
        }, r.prototype.ondisconnect = function() {
            u("server disconnect (%s)", this.nsp), this.destroy(), this.onclose("io server disconnect")
        }, r.prototype.destroy = function() {
            if (this.subs) {
                for (var e = 0; e < this.subs.length; e++) this.subs[e].destroy();
                this.subs = null
            }
            this.io.destroy(this)
        }, r.prototype.close = r.prototype.disconnect = function() {
            return this.connected && (u("performing disconnect (%s)", this.nsp), this.packet({
                type: o.DISCONNECT
            })), this.destroy(), this.connected && this.onclose("io client disconnect"), this
        }, r.prototype.compress = function(e) {
            return this.flags = this.flags || {}, this.flags.compress = e, this
        }
    }, function(e, t) {
        function n(e, t, n) {
            return e.on(t, n), {
                destroy: function() {
                    e.removeListener(t, n)
                }
            }
        }
        e.exports = n
    }, function(e, t) {
        var n = [].slice;
        e.exports = function(e, t) {
            if ("string" == typeof t && (t = e[t]), "function" != typeof t) throw new Error("bind() requires a function");
            var r = n.call(arguments, 2);
            return function() {
                return t.apply(e, r.concat(n.call(arguments)))
            }
        }
    }, function(e, t, n) {
        var r = n(114),
            o = "object" == typeof self && self && self.Object === Object && self,
            i = r || o || Function("return this")();
        e.exports = i
    }, function(e, t, n) {
        var r = n(40),
            o = r.Symbol;
        e.exports = o
    }, function(e, t, n) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var r = n(2),
            o = n.n(r),
            i = n(11),
            a = (n.n(i), n(43));
        Object(i.render)(o.a.createElement(a.a, null), document.querySelector("[coins-container]")), n(122).install()
    }, function(e, t, n) {
        "use strict";

        function r(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function o(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function i(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        var a = n(2),
            s = n.n(a),
            c = n(11),
            u = (n.n(c), n(44)),
            l = n.n(u),
            p = n(12),
            f = n(14),
            h = n(27),
            d = n(106),
            m = n(108),
            y = n(109),
            g = n(121),
            v = (n.n(g), function() {
                function e(e, t) {
                    var n = [],
                        r = !0,
                        o = !1,
                        i = void 0;
                    try {
                        for (var a, s = e[Symbol.iterator](); !(r = (a = s.next()).done) && (n.push(a.value), !t || n.length !== t); r = !0);
                    } catch (e) {
                        o = !0, i = e
                    } finally {
                        try {
                            !r && s.return && s.return()
                        } finally {
                            if (o) throw i
                        }
                    }
                    return n
                }
                return function(t, n) {
                    if (Array.isArray(t)) return t;
                    if (Symbol.iterator in Object(t)) return e(t, n);
                    throw new TypeError("Invalid attempt to destructure non-iterable instance")
                }
            }()),
            b = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var r = t[n];
                        r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
                    }
                }
                return function(t, n, r) {
                    return n && e(t.prototype, n), r && e(t, r), t
                }
            }(),
            w = function() {
                return {
                    __html: "Real-time cryptocurrency prices, trades, volumes and history."
                }
            },
            C = function(e) {
                function t() {
                    var e, n, i, a;
                    r(this, t);
                    for (var s = arguments.length, c = Array(s), u = 0; u < s; u++) c[u] = arguments[u];
                    return n = i = o(this, (e = t.__proto__ || Object.getPrototypeOf(t)).call.apply(e, [this].concat(c))), i.state = {
                        global: null,
                        front: null,
                        darkMode: 1 == localStorage.getItem("darkMode"),
                        currentCurrency: !!localStorage.getItem("curr") && localStorage.getItem("curr") || "USD"
                    }, a = n, o(i, a)
                }
                return i(t, e), b(t, [{
                    key: "searchCoins",
                    value: function(e) {
                        e.target.value.length ? this.setState({
                            showCoins: this.state.front.filter(function(t) {
                                return new RegExp(e.target.value, "gi").test(t.long)
                            })
                        }) : this.setState({
                            showCoins: this.state.front.slice(0, 30)
                        })
                    }
                }, {
                    key: "updateShowenCoins",
                    value: function() {
                        window.innerHeight + window.scrollY >= document.body.offsetHeight && this.setState({
                            showCoins: this.state.front.slice(0, this.state.showCoins.length + 30)
                        })
                    }
                }, {
                    key: "toggleDarkTheme",
                    value: function(e) {
                        this.setState({
                            darkMode: e.target.checked
                        }), localStorage.setItem("darkMode", Number(e.target.checked))
                    }
                }, {
                    key: "changeCurrency",
                    value: function(e) {
                        this.setState({
                            currentCurrency: e
                        }), localStorage.setItem("curr", e)
                    }
                }, {
                    key: "componentWillMount",
                    value: function() {
                        var e = this;
                        this.mountHeaderClass = document.querySelector("[coins-header]"), window.Promise.all([Object(p.a)("https://api.coinmarketcap.com/v1/global/").then(function(e) {
                            return e.data
                        }), Object(p.a)("front").then(function(e) {
                            return e.data
                        }), f.a.fetch()]).then(function(t) {
                            var n = v(t, 2),
                                r = n[0],
                                o = n[1];
                            e.setState({
                                global: r,
                                front: o,
                                showCoins: o.slice(0, 30)
                            }), h.a.open(), window.addEventListener("scroll", function() {
                                return e.updateShowenCoins()
                            })
                        })
                    }
                }, {
                    key: "componentWillUnmount",
                    value: function() {
                        h.a.close(), window.removeEventListener("scroll", function() {})
                    }
                }, {
                    key: "render",
                    value: function() {
                        var e = this;
                        return s.a.createElement(l.a, {
                            transitionName: "fade",
                            transitionEnterTimeout: 500,
                            transitionLeaveTimeout: 500
                        }, this.state.front && this.state.global ? s.a.createElement(a.Fragment, {
                            key: "loaded"
                        }, this.mountHeaderClass && Object(c.createPortal)(s.a.createElement(d.a, {
                            darkMode: this.state.darkMode,
                            global: this.state.global,
                            options: this.mountHeaderClass.dataset.options
                        }), this.mountHeaderClass), s.a.createElement("h1", {
                            className: "h3 text-center",
                            dangerouslySetInnerHTML: w()
                        }), s.a.createElement(m.a, {
                            rates: Object.keys(f.a.rates),
                            darkMode: this.state.darkMode,
                            currentCurrency: this.state.currentCurrency,
                            toggleDarkTheme: function(t) {
                                return e.toggleDarkTheme(t)
                            },
                            searchCoins: function(t) {
                                return e.searchCoins(t)
                            },
                            changeCurrency: function(t) {
                                return e.changeCurrency(t)
                            }
                        }), s.a.createElement("div", {
                            className: this.state.darkMode ? "table-responsive bg-secondary" : "table-responsive bg-light"
                        }, s.a.createElement("table", {
                            className: this.state.darkMode ? "table table-bordered table-hover table-dark" : "table table-bordered table-hover table-light"
                        }, s.a.createElement("thead", null, s.a.createElement("tr", null, s.a.createElement("th", {
                            scope: "col"
                        }, "Name"), s.a.createElement("th", {
                            scope: "col"
                        }, "Marketcap"), s.a.createElement("th", {
                            scope: "col"
                        }, "Price"), s.a.createElement("th", {
                            scope: "col"
                        }, "24hour VWAP"), s.a.createElement("th", {
                            scope: "col"
                        }, "Supply"), s.a.createElement("th", {
                            scope: "col"
                        }, "24h V."), s.a.createElement("th", {
                            scope: "col"
                        }, "24h. %"))), s.a.createElement("tbody", null, this.state.showCoins.slice(0, 5).map(function(t) {
                            return s.a.createElement(y.a, {
                                currentCurrency: e.state.currentCurrency,
                                key: t.short,
                                coin: t
                            })
                        }))))) : s.a.createElement("div", {
                            key: "loading",
                            className: "loading d-flex justify-content-center align-items-center bg-dark"
                        }, s.a.createElement("svg", {
                            width: "105",
                            height: "105",
                            viewBox: "0 0 105 105",
                            xmlns: "http://www.w3.org/2000/svg",
                            fill: "#fff"
                        }, s.a.createElement("circle", {
                            cx: "12.5",
                            cy: "12.5",
                            r: "12.5"
                        }, s.a.createElement("animate", {
                            attributeName: "fill-opacity",
                            begin: "0s",
                            dur: "1s",
                            values: "1;.2;1",
                            calcMode: "linear",
                            repeatCount: "indefinite"
                        })), s.a.createElement("circle", {
                            cx: "12.5",
                            cy: "52.5",
                            r: "12.5",
                            fillOpacity: ".5"
                        }, s.a.createElement("animate", {
                            attributeName: "fill-opacity",
                            begin: "100ms",
                            dur: "1s",
                            values: "1;.2;1",
                            calcMode: "linear",
                            repeatCount: "indefinite"
                        })), s.a.createElement("circle", {
                            cx: "52.5",
                            cy: "12.5",
                            r: "12.5"
                        }, s.a.createElement("animate", {
                            attributeName: "fill-opacity",
                            begin: "300ms",
                            dur: "1s",
                            values: "1;.2;1",
                            calcMode: "linear",
                            repeatCount: "indefinite"
                        })), s.a.createElement("circle", {
                            cx: "52.5",
                            cy: "52.5",
                            r: "12.5"
                        }, s.a.createElement("animate", {
                            attributeName: "fill-opacity",
                            begin: "600ms",
                            dur: "1s",
                            values: "1;.2;1",
                            calcMode: "linear",
                            repeatCount: "indefinite"
                        })), s.a.createElement("circle", {
                            cx: "92.5",
                            cy: "12.5",
                            r: "12.5"
                        }, s.a.createElement("animate", {
                            attributeName: "fill-opacity",
                            begin: "800ms",
                            dur: "1s",
                            values: "1;.2;1",
                            calcMode: "linear",
                            repeatCount: "indefinite"
                        })), s.a.createElement("circle", {
                            cx: "92.5",
                            cy: "52.5",
                            r: "12.5"
                        }, s.a.createElement("animate", {
                            attributeName: "fill-opacity",
                            begin: "400ms",
                            dur: "1s",
                            values: "1;.2;1",
                            calcMode: "linear",
                            repeatCount: "indefinite"
                        })), s.a.createElement("circle", {
                            cx: "12.5",
                            cy: "92.5",
                            r: "12.5"
                        }, s.a.createElement("animate", {
                            attributeName: "fill-opacity",
                            begin: "700ms",
                            dur: "1s",
                            values: "1;.2;1",
                            calcMode: "linear",
                            repeatCount: "indefinite"
                        })), s.a.createElement("circle", {
                            cx: "52.5",
                            cy: "92.5",
                            r: "12.5"
                        }, s.a.createElement("animate", {
                            attributeName: "fill-opacity",
                            begin: "500ms",
                            dur: "1s",
                            values: "1;.2;1",
                            calcMode: "linear",
                            repeatCount: "indefinite"
                        })), s.a.createElement("circle", {
                            cx: "92.5",
                            cy: "92.5",
                            r: "12.5"
                        }, s.a.createElement("animate", {
                            attributeName: "fill-opacity",
                            begin: "200ms",
                            dur: "1s",
                            values: "1;.2;1",
                            calcMode: "linear",
                            repeatCount: "indefinite"
                        })))))
                    }
                }]), t
            }(a.Component);
        t.a = C
    }, function(e, t, n) {
        "use strict";
        e.exports = n(45)
    }, function(e, t, n) {
        "use strict";

        function r(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function o(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function i(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function a(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        t.__esModule = !0;
        var s = Object.assign || function(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = arguments[t];
                    for (var r in n) Object.prototype.hasOwnProperty.call(n, r) && (e[r] = n[r])
                }
                return e
            },
            c = n(2),
            u = r(c),
            l = n(3),
            p = r(l),
            f = n(50),
            h = r(f),
            d = n(54),
            m = r(d),
            y = n(21),
            g = (y.nameShape.isRequired, p.default.bool, p.default.bool, p.default.bool, (0, y.transitionTimeout)("Appear"), (0, y.transitionTimeout)("Enter"), (0, y.transitionTimeout)("Leave"), {
                transitionAppear: !1,
                transitionEnter: !0,
                transitionLeave: !0
            }),
            v = function(e) {
                function t() {
                    var n, r, a;
                    o(this, t);
                    for (var s = arguments.length, c = Array(s), l = 0; l < s; l++) c[l] = arguments[l];
                    return n = r = i(this, e.call.apply(e, [this].concat(c))), r._wrapChild = function(e) {
                        return u.default.createElement(m.default, {
                            name: r.props.transitionName,
                            appear: r.props.transitionAppear,
                            enter: r.props.transitionEnter,
                            leave: r.props.transitionLeave,
                            appearTimeout: r.props.transitionAppearTimeout,
                            enterTimeout: r.props.transitionEnterTimeout,
                            leaveTimeout: r.props.transitionLeaveTimeout
                        }, e)
                    }, a = n, i(r, a)
                }
                return a(t, e), t.prototype.render = function() {
                    return u.default.createElement(h.default, s({}, this.props, {
                        childFactory: this._wrapChild
                    }))
                }, t
            }(u.default.Component);
        v.displayName = "CSSTransitionGroup", v.propTypes = {}, v.defaultProps = g, t.default = v, e.exports = t.default
    }, function(e, t, n) {
        "use strict";
        var r = n(47),
            o = n(48),
            i = n(49);
        e.exports = function() {
            function e(e, t, n, r, a, s) {
                s !== i && o(!1, "Calling PropTypes validators directly is not supported by the `prop-types` package. Use PropTypes.checkPropTypes() to call them. Read more at http://fb.me/use-check-prop-types")
            }

            function t() {
                return e
            }
            e.isRequired = e;
            var n = {
                array: e,
                bool: e,
                func: e,
                number: e,
                object: e,
                string: e,
                symbol: e,
                any: e,
                arrayOf: t,
                element: e,
                instanceOf: t,
                node: e,
                objectOf: t,
                oneOf: t,
                oneOfType: t,
                shape: t,
                exact: t
            };
            return n.checkPropTypes = r, n.PropTypes = n, n
        }
    }, function(e, t, n) {
        "use strict";

        function r(e) {
            return function() {
                return e
            }
        }
        var o = function() {};
        o.thatReturns = r, o.thatReturnsFalse = r(!1), o.thatReturnsTrue = r(!0), o.thatReturnsNull = r(null), o.thatReturnsThis = function() {
            return this
        }, o.thatReturnsArgument = function(e) {
            return e
        }, e.exports = o
    }, function(e, t, n) {
        "use strict";

        function r(e, t, n, r, i, a, s, c) {
            if (o(t), !e) {
                var u;
                if (void 0 === t) u = new Error("Minified exception occurred; use the non-minified dev environment for the full error message and additional helpful warnings.");
                else {
                    var l = [n, r, i, a, s, c],
                        p = 0;
                    u = new Error(t.replace(/%s/g, function() {
                        return l[p++]
                    })), u.name = "Invariant Violation"
                }
                throw u.framesToPop = 1, u
            }
        }
        var o = function(e) {};
        e.exports = r
    }, function(e, t, n) {
        "use strict";
        e.exports = "SECRET_DO_NOT_PASS_THIS_OR_YOU_WILL_BE_FIRED"
    }, function(e, t, n) {
        "use strict";

        function r(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function o(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function i(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function a(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        t.__esModule = !0;
        var s = Object.assign || function(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = arguments[t];
                    for (var r in n) Object.prototype.hasOwnProperty.call(n, r) && (e[r] = n[r])
                }
                return e
            },
            c = n(51),
            u = r(c),
            l = n(2),
            p = r(l),
            f = n(3),
            h = r(f),
            d = n(52),
            m = (r(d), n(53)),
            y = (h.default.any, h.default.func, h.default.node, {
                component: "span",
                childFactory: function(e) {
                    return e
                }
            }),
            g = function(e) {
                function t(n, r) {
                    o(this, t);
                    var a = i(this, e.call(this, n, r));
                    return a.performAppear = function(e, t) {
                        a.currentlyTransitioningKeys[e] = !0, t.componentWillAppear ? t.componentWillAppear(a._handleDoneAppearing.bind(a, e, t)) : a._handleDoneAppearing(e, t)
                    }, a._handleDoneAppearing = function(e, t) {
                        t.componentDidAppear && t.componentDidAppear(), delete a.currentlyTransitioningKeys[e];
                        var n = (0, m.getChildMapping)(a.props.children);
                        n && n.hasOwnProperty(e) || a.performLeave(e, t)
                    }, a.performEnter = function(e, t) {
                        a.currentlyTransitioningKeys[e] = !0, t.componentWillEnter ? t.componentWillEnter(a._handleDoneEntering.bind(a, e, t)) : a._handleDoneEntering(e, t)
                    }, a._handleDoneEntering = function(e, t) {
                        t.componentDidEnter && t.componentDidEnter(), delete a.currentlyTransitioningKeys[e];
                        var n = (0, m.getChildMapping)(a.props.children);
                        n && n.hasOwnProperty(e) || a.performLeave(e, t)
                    }, a.performLeave = function(e, t) {
                        a.currentlyTransitioningKeys[e] = !0, t.componentWillLeave ? t.componentWillLeave(a._handleDoneLeaving.bind(a, e, t)) : a._handleDoneLeaving(e, t)
                    }, a._handleDoneLeaving = function(e, t) {
                        t.componentDidLeave && t.componentDidLeave(), delete a.currentlyTransitioningKeys[e];
                        var n = (0, m.getChildMapping)(a.props.children);
                        n && n.hasOwnProperty(e) ? a.keysToEnter.push(e) : a.setState(function(t) {
                            var n = s({}, t.children);
                            return delete n[e], {
                                children: n
                            }
                        })
                    }, a.childRefs = Object.create(null), a.state = {
                        children: (0, m.getChildMapping)(n.children)
                    }, a
                }
                return a(t, e), t.prototype.componentWillMount = function() {
                    this.currentlyTransitioningKeys = {}, this.keysToEnter = [], this.keysToLeave = []
                }, t.prototype.componentDidMount = function() {
                    var e = this.state.children;
                    for (var t in e) e[t] && this.performAppear(t, this.childRefs[t])
                }, t.prototype.componentWillReceiveProps = function(e) {
                    var t = (0, m.getChildMapping)(e.children),
                        n = this.state.children;
                    this.setState({
                        children: (0, m.mergeChildMappings)(n, t)
                    });
                    for (var r in t) {
                        var o = n && n.hasOwnProperty(r);
                        !t[r] || o || this.currentlyTransitioningKeys[r] || this.keysToEnter.push(r)
                    }
                    for (var i in n) {
                        var a = t && t.hasOwnProperty(i);
                        !n[i] || a || this.currentlyTransitioningKeys[i] || this.keysToLeave.push(i)
                    }
                }, t.prototype.componentDidUpdate = function() {
                    var e = this,
                        t = this.keysToEnter;
                    this.keysToEnter = [], t.forEach(function(t) {
                        return e.performEnter(t, e.childRefs[t])
                    });
                    var n = this.keysToLeave;
                    this.keysToLeave = [], n.forEach(function(t) {
                        return e.performLeave(t, e.childRefs[t])
                    })
                }, t.prototype.render = function() {
                    var e = this,
                        t = [];
                    for (var n in this.state.children) ! function(n) {
                        var r = e.state.children[n];
                        if (r) {
                            var o = "string" != typeof r.ref,
                                i = e.props.childFactory(r),
                                a = function(t) {
                                    e.childRefs[n] = t
                                };
                            i === r && o && (a = (0, u.default)(r.ref, a)), t.push(p.default.cloneElement(i, {
                                key: n,
                                ref: a
                            }))
                        }
                    }(n);
                    var r = s({}, this.props);
                    return delete r.transitionLeave, delete r.transitionName, delete r.transitionAppear, delete r.transitionEnter, delete r.childFactory, delete r.transitionLeaveTimeout, delete r.transitionEnterTimeout, delete r.transitionAppearTimeout, delete r.component, p.default.createElement(this.props.component, r, t)
                }, t
            }(p.default.Component);
        g.displayName = "TransitionGroup", g.propTypes = {}, g.defaultProps = y, t.default = g, e.exports = t.default
    }, function(e, t) {
        e.exports = function() {
            for (var e = arguments.length, t = [], n = 0; n < e; n++) t[n] = arguments[n];
            if (t = t.filter(function(e) {
                return null != e
            }), 0 !== t.length) return 1 === t.length ? t[0] : t.reduce(function(e, t) {
                return function() {
                    e.apply(this, arguments), t.apply(this, arguments)
                }
            })
        }
    }, function(e, t, n) {
        "use strict";
        var r = function() {};
        e.exports = r
    }, function(e, t, n) {
        "use strict";

        function r(e) {
            if (!e) return e;
            var t = {};
            return i.Children.map(e, function(e) {
                return e
            }).forEach(function(e) {
                t[e.key] = e
            }), t
        }

        function o(e, t) {
            function n(n) {
                return t.hasOwnProperty(n) ? t[n] : e[n]
            }
            e = e || {}, t = t || {};
            var r = {},
                o = [];
            for (var i in e) t.hasOwnProperty(i) ? o.length && (r[i] = o, o = []) : o.push(i);
            var a = void 0,
                s = {};
            for (var c in t) {
                if (r.hasOwnProperty(c))
                    for (a = 0; a < r[c].length; a++) {
                        var u = r[c][a];
                        s[r[c][a]] = n(u)
                    }
                s[c] = n(c)
            }
            for (a = 0; a < o.length; a++) s[o[a]] = n(o[a]);
            return s
        }
        t.__esModule = !0, t.getChildMapping = r, t.mergeChildMappings = o;
        var i = n(2)
    }, function(e, t, n) {
        "use strict";

        function r(e) {
            return e && e.__esModule ? e : {
                default: e
            }
        }

        function o(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function i(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function a(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }

        function s(e, t) {
            return x.length ? x.forEach(function(n) {
                return e.addEventListener(n, t, !1)
            }) : setTimeout(t, 0),
                function() {
                    x.length && x.forEach(function(n) {
                        return e.removeEventListener(n, t, !1)
                    })
                }
        }
        t.__esModule = !0;
        var c = Object.assign || function(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = arguments[t];
                    for (var r in n) Object.prototype.hasOwnProperty.call(n, r) && (e[r] = n[r])
                }
                return e
            },
            u = n(55),
            l = r(u),
            p = n(57),
            f = r(p),
            h = n(58),
            d = r(h),
            m = n(59),
            y = n(2),
            g = r(y),
            v = n(3),
            b = r(v),
            w = n(11),
            C = n(21),
            x = [];
        m.transitionEnd && x.push(m.transitionEnd), m.animationEnd && x.push(m.animationEnd);
        var k = (b.default.node, C.nameShape.isRequired, b.default.bool, b.default.bool, b.default.bool, b.default.number, b.default.number, b.default.number, function(e) {
            function t() {
                var n, r, a;
                o(this, t);
                for (var s = arguments.length, c = Array(s), u = 0; u < s; u++) c[u] = arguments[u];
                return n = r = i(this, e.call.apply(e, [this].concat(c))), r.componentWillAppear = function(e) {
                    r.props.appear ? r.transition("appear", e, r.props.appearTimeout) : e()
                }, r.componentWillEnter = function(e) {
                    r.props.enter ? r.transition("enter", e, r.props.enterTimeout) : e()
                }, r.componentWillLeave = function(e) {
                    r.props.leave ? r.transition("leave", e, r.props.leaveTimeout) : e()
                }, a = n, i(r, a)
            }
            return a(t, e), t.prototype.componentWillMount = function() {
                this.classNameAndNodeQueue = [], this.transitionTimeouts = []
            }, t.prototype.componentWillUnmount = function() {
                this.unmounted = !0, this.timeout && clearTimeout(this.timeout), this.transitionTimeouts.forEach(function(e) {
                    clearTimeout(e)
                }), this.classNameAndNodeQueue.length = 0
            }, t.prototype.transition = function(e, t, n) {
                var r = (0, w.findDOMNode)(this);
                if (!r) return void(t && t());
                var o = this.props.name[e] || this.props.name + "-" + e,
                    i = this.props.name[e + "Active"] || o + "-active",
                    a = null,
                    c = void 0;
                (0, l.default)(r, o), this.queueClassAndNode(i, r);
                var u = function(e) {
                    e && e.target !== r || (clearTimeout(a), c && c(), (0, f.default)(r, o), (0, f.default)(r, i), c && c(), t && t())
                };
                n ? (a = setTimeout(u, n), this.transitionTimeouts.push(a)) : m.transitionEnd && (c = s(r, u))
            }, t.prototype.queueClassAndNode = function(e, t) {
                var n = this;
                this.classNameAndNodeQueue.push({
                    className: e,
                    node: t
                }), this.rafHandle || (this.rafHandle = (0, d.default)(function() {
                    return n.flushClassNameAndNodeQueue()
                }))
            }, t.prototype.flushClassNameAndNodeQueue = function() {
                this.unmounted || this.classNameAndNodeQueue.forEach(function(e) {
                    e.node.scrollTop, (0, l.default)(e.node, e.className)
                }), this.classNameAndNodeQueue.length = 0, this.rafHandle = null
            }, t.prototype.render = function() {
                var e = c({}, this.props);
                return delete e.name, delete e.appear, delete e.enter, delete e.leave, delete e.appearTimeout, delete e.enterTimeout, delete e.leaveTimeout, delete e.children, g.default.cloneElement(g.default.Children.only(this.props.children), e)
            }, t
        }(g.default.Component));
        k.displayName = "CSSTransitionGroupChild", k.propTypes = {}, t.default = k, e.exports = t.default
    }, function(e, t, n) {
        "use strict";

        function r(e, t) {
            e.classList ? e.classList.add(t) : (0, i.default)(e, t) || ("string" == typeof e.className ? e.className = e.className + " " + t : e.setAttribute("class", (e.className && e.className.baseVal || "") + " " + t))
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        }), t.default = r;
        var o = n(56),
            i = function(e) {
                return e && e.__esModule ? e : {
                    default: e
                }
            }(o);
        e.exports = t.default
    }, function(e, t, n) {
        "use strict";

        function r(e, t) {
            return e.classList ? !!t && e.classList.contains(t) : -1 !== (" " + (e.className.baseVal || e.className) + " ").indexOf(" " + t + " ")
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        }), t.default = r, e.exports = t.default
    }, function(e, t, n) {
        "use strict";

        function r(e, t) {
            return e.replace(new RegExp("(^|\\s)" + t + "(?:\\s|$)", "g"), "$1").replace(/\s+/g, " ").replace(/^\s*|\s*$/g, "")
        }
        e.exports = function(e, t) {
            e.classList ? e.classList.remove(t) : "string" == typeof e.className ? e.className = r(e.className, t) : e.setAttribute("class", r(e.className && e.className.baseVal || "", t))
        }
    }, function(e, t, n) {
        "use strict";

        function r(e) {
            var t = (new Date).getTime(),
                n = Math.max(0, 16 - (t - p)),
                r = setTimeout(e, n);
            return p = t, r
        }
        Object.defineProperty(t, "__esModule", {
            value: !0
        });
        var o = n(20),
            i = function(e) {
                return e && e.__esModule ? e : {
                    default: e
                }
            }(o),
            a = ["", "webkit", "moz", "o", "ms"],
            s = "clearTimeout",
            c = r,
            u = void 0,
            l = function(e, t) {
                return e + (e ? t[0].toUpperCase() + t.substr(1) : t) + "AnimationFrame"
            };
        i.default && a.some(function(e) {
            var t = l(e, "request");
            if (t in window) return s = l(e, "cancel"), c = function(e) {
                return window[t](e)
            }
        });
        var p = (new Date).getTime();
        u = function(e) {
            return c(e)
        }, u.cancel = function(e) {
            window[s] && "function" == typeof window[s] && window[s](e)
        }, t.default = u, e.exports = t.default
    }, function(e, t, n) {
        "use strict";
        Object.defineProperty(t, "__esModule", {
            value: !0
        }), t.animationEnd = t.animationDelay = t.animationTiming = t.animationDuration = t.animationName = t.transitionEnd = t.transitionDuration = t.transitionDelay = t.transitionTiming = t.transitionProperty = t.transform = void 0;
        var r = n(20),
            o = function(e) {
                return e && e.__esModule ? e : {
                    default: e
                }
            }(r),
            i = "transform",
            a = void 0,
            s = void 0,
            c = void 0,
            u = void 0,
            l = void 0,
            p = void 0,
            f = void 0,
            h = void 0,
            d = void 0,
            m = void 0,
            y = void 0;
        if (o.default) {
            var g = function() {
                for (var e = document.createElement("div").style, t = {
                    O: function(e) {
                        return "o" + e.toLowerCase()
                    },
                    Moz: function(e) {
                        return e.toLowerCase()
                    },
                    Webkit: function(e) {
                        return "webkit" + e
                    },
                    ms: function(e) {
                        return "MS" + e
                    }
                }, n = Object.keys(t), r = void 0, o = void 0, i = "", a = 0; a < n.length; a++) {
                    var s = n[a];
                    if (s + "TransitionProperty" in e) {
                        i = "-" + s.toLowerCase(), r = t[s]("TransitionEnd"), o = t[s]("AnimationEnd");
                        break
                    }
                }
                return !r && "transitionProperty" in e && (r = "transitionend"), !o && "animationName" in e && (o = "animationend"), e = null, {
                    animationEnd: o,
                    transitionEnd: r,
                    prefix: i
                }
            }();
            a = g.prefix, t.transitionEnd = s = g.transitionEnd, t.animationEnd = c = g.animationEnd, t.transform = i = a + "-" + i, t.transitionProperty = u = a + "-transition-property", t.transitionDuration = l = a + "-transition-duration", t.transitionDelay = f = a + "-transition-delay", t.transitionTiming = p = a + "-transition-timing-function", t.animationName = h = a + "-animation-name", t.animationDuration = d = a + "-animation-duration", t.animationTiming = m = a + "-animation-delay", t.animationDelay = y = a + "-animation-timing-function"
        }
        t.transform = i, t.transitionProperty = u, t.transitionTiming = p, t.transitionDelay = f, t.transitionDuration = l, t.transitionEnd = s, t.animationName = h, t.animationDuration = d, t.animationTiming = m, t.animationDelay = y, t.animationEnd = c, t.default = {
            transform: i,
            end: s,
            property: u,
            timing: p,
            delay: f,
            duration: l
        }
    }, function(e, t, n) {
        e.exports = n(61)
    }, function(e, t, n) {
        "use strict";

        function r(e) {
            var t = new a(e),
                n = i(a.prototype.request, t);
            return o.extend(n, a.prototype, t), o.extend(n, t), n
        }
        var o = n(1),
            i = n(22),
            a = n(63),
            s = n(13),
            c = r(s);
        c.Axios = a, c.create = function(e) {
            return r(o.merge(s, e))
        }, c.Cancel = n(26), c.CancelToken = n(77), c.isCancel = n(25), c.all = function(e) {
            return Promise.all(e)
        }, c.spread = n(78), e.exports = c, e.exports.default = c
    }, function(e, t) {
        function n(e) {
            return !!e.constructor && "function" == typeof e.constructor.isBuffer && e.constructor.isBuffer(e)
        }

        function r(e) {
            return "function" == typeof e.readFloatLE && "function" == typeof e.slice && n(e.slice(0, 0))
        }
        /*!
         * Determine if an object is a Buffer
         *
         * @author   Feross Aboukhadijeh <https://feross.org>
         * @license  MIT
         */
        e.exports = function(e) {
            return null != e && (n(e) || r(e) || !!e._isBuffer)
        }
    }, function(e, t, n) {
        "use strict";

        function r(e) {
            this.defaults = e, this.interceptors = {
                request: new a,
                response: new a
            }
        }
        var o = n(13),
            i = n(1),
            a = n(72),
            s = n(73);
        r.prototype.request = function(e) {
            "string" == typeof e && (e = i.merge({
                url: arguments[0]
            }, arguments[1])), e = i.merge(o, {
                method: "get"
            }, this.defaults, e), e.method = e.method.toLowerCase();
            var t = [s, void 0],
                n = Promise.resolve(e);
            for (this.interceptors.request.forEach(function(e) {
                t.unshift(e.fulfilled, e.rejected)
            }), this.interceptors.response.forEach(function(e) {
                t.push(e.fulfilled, e.rejected)
            }); t.length;) n = n.then(t.shift(), t.shift());
            return n
        }, i.forEach(["delete", "get", "head", "options"], function(e) {
            r.prototype[e] = function(t, n) {
                return this.request(i.merge(n || {}, {
                    method: e,
                    url: t
                }))
            }
        }), i.forEach(["post", "put", "patch"], function(e) {
            r.prototype[e] = function(t, n, r) {
                return this.request(i.merge(r || {}, {
                    method: e,
                    url: t,
                    data: n
                }))
            }
        }), e.exports = r
    }, function(e, t, n) {
        "use strict";
        var r = n(1);
        e.exports = function(e, t) {
            r.forEach(e, function(n, r) {
                r !== t && r.toUpperCase() === t.toUpperCase() && (e[t] = n, delete e[r])
            })
        }
    }, function(e, t, n) {
        "use strict";
        var r = n(24);
        e.exports = function(e, t, n) {
            var o = n.config.validateStatus;
            n.status && o && !o(n.status) ? t(r("Request failed with status code " + n.status, n.config, null, n.request, n)) : e(n)
        }
    }, function(e, t, n) {
        "use strict";
        e.exports = function(e, t, n, r, o) {
            return e.config = t, n && (e.code = n), e.request = r, e.response = o, e
        }
    }, function(e, t, n) {
        "use strict";

        function r(e) {
            return encodeURIComponent(e).replace(/%40/gi, "@").replace(/%3A/gi, ":").replace(/%24/g, "$").replace(/%2C/gi, ",").replace(/%20/g, "+").replace(/%5B/gi, "[").replace(/%5D/gi, "]")
        }
        var o = n(1);
        e.exports = function(e, t, n) {
            if (!t) return e;
            var i;
            if (n) i = n(t);
            else if (o.isURLSearchParams(t)) i = t.toString();
            else {
                var a = [];
                o.forEach(t, function(e, t) {
                    null !== e && void 0 !== e && (o.isArray(e) ? t += "[]" : e = [e], o.forEach(e, function(e) {
                        o.isDate(e) ? e = e.toISOString() : o.isObject(e) && (e = JSON.stringify(e)), a.push(r(t) + "=" + r(e))
                    }))
                }), i = a.join("&")
            }
            return i && (e += (-1 === e.indexOf("?") ? "?" : "&") + i), e
        }
    }, function(e, t, n) {
        "use strict";
        var r = n(1),
            o = ["age", "authorization", "content-length", "content-type", "etag", "expires", "from", "host", "if-modified-since", "if-unmodified-since", "last-modified", "location", "max-forwards", "proxy-authorization", "referer", "retry-after", "user-agent"];
        e.exports = function(e) {
            var t, n, i, a = {};
            return e ? (r.forEach(e.split("\n"), function(e) {
                if (i = e.indexOf(":"), t = r.trim(e.substr(0, i)).toLowerCase(), n = r.trim(e.substr(i + 1)), t) {
                    if (a[t] && o.indexOf(t) >= 0) return;
                    a[t] = "set-cookie" === t ? (a[t] ? a[t] : []).concat([n]) : a[t] ? a[t] + ", " + n : n
                }
            }), a) : a
        }
    }, function(e, t, n) {
        "use strict";
        var r = n(1);
        e.exports = r.isStandardBrowserEnv() ? function() {
            function e(e) {
                var t = e;
                return n && (o.setAttribute("href", t), t = o.href), o.setAttribute("href", t), {
                    href: o.href,
                    protocol: o.protocol ? o.protocol.replace(/:$/, "") : "",
                    host: o.host,
                    search: o.search ? o.search.replace(/^\?/, "") : "",
                    hash: o.hash ? o.hash.replace(/^#/, "") : "",
                    hostname: o.hostname,
                    port: o.port,
                    pathname: "/" === o.pathname.charAt(0) ? o.pathname : "/" + o.pathname
                }
            }
            var t, n = /(msie|trident)/i.test(navigator.userAgent),
                o = document.createElement("a");
            return t = e(window.location.href),
                function(n) {
                    var o = r.isString(n) ? e(n) : n;
                    return o.protocol === t.protocol && o.host === t.host
                }
        }() : function() {
            return function() {
                return !0
            }
        }()
    }, function(e, t, n) {
        "use strict";

        function r() {
            this.message = "String contains an invalid character"
        }

        function o(e) {
            for (var t, n, o = String(e), a = "", s = 0, c = i; o.charAt(0 | s) || (c = "=", s % 1); a += c.charAt(63 & t >> 8 - s % 1 * 8)) {
                if ((n = o.charCodeAt(s += .75)) > 255) throw new r;
                t = t << 8 | n
            }
            return a
        }
        var i = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
        r.prototype = new Error, r.prototype.code = 5, r.prototype.name = "InvalidCharacterError", e.exports = o
    }, function(e, t, n) {
        "use strict";
        var r = n(1);
        e.exports = r.isStandardBrowserEnv() ? function() {
            return {
                write: function(e, t, n, o, i, a) {
                    var s = [];
                    s.push(e + "=" + encodeURIComponent(t)), r.isNumber(n) && s.push("expires=" + new Date(n).toGMTString()), r.isString(o) && s.push("path=" + o), r.isString(i) && s.push("domain=" + i), !0 === a && s.push("secure"), document.cookie = s.join("; ")
                },
                read: function(e) {
                    var t = document.cookie.match(new RegExp("(^|;\\s*)(" + e + ")=([^;]*)"));
                    return t ? decodeURIComponent(t[3]) : null
                },
                remove: function(e) {
                    this.write(e, "", Date.now() - 864e5)
                }
            }
        }() : function() {
            return {
                write: function() {},
                read: function() {
                    return null
                },
                remove: function() {}
            }
        }()
    }, function(e, t, n) {
        "use strict";

        function r() {
            this.handlers = []
        }
        var o = n(1);
        r.prototype.use = function(e, t) {
            return this.handlers.push({
                fulfilled: e,
                rejected: t
            }), this.handlers.length - 1
        }, r.prototype.eject = function(e) {
            this.handlers[e] && (this.handlers[e] = null)
        }, r.prototype.forEach = function(e) {
            o.forEach(this.handlers, function(t) {
                null !== t && e(t)
            })
        }, e.exports = r
    }, function(e, t, n) {
        "use strict";

        function r(e) {
            e.cancelToken && e.cancelToken.throwIfRequested()
        }
        var o = n(1),
            i = n(74),
            a = n(25),
            s = n(13),
            c = n(75),
            u = n(76);
        e.exports = function(e) {
            return r(e), e.baseURL && !c(e.url) && (e.url = u(e.baseURL, e.url)), e.headers = e.headers || {}, e.data = i(e.data, e.headers, e.transformRequest), e.headers = o.merge(e.headers.common || {}, e.headers[e.method] || {}, e.headers || {}), o.forEach(["delete", "get", "head", "post", "put", "patch", "common"], function(t) {
                delete e.headers[t]
            }), (e.adapter || s.adapter)(e).then(function(t) {
                return r(e), t.data = i(t.data, t.headers, e.transformResponse), t
            }, function(t) {
                return a(t) || (r(e), t && t.response && (t.response.data = i(t.response.data, t.response.headers, e.transformResponse))), Promise.reject(t)
            })
        }
    }, function(e, t, n) {
        "use strict";
        var r = n(1);
        e.exports = function(e, t, n) {
            return r.forEach(n, function(n) {
                e = n(e, t)
            }), e
        }
    }, function(e, t, n) {
        "use strict";
        e.exports = function(e) {
            return /^([a-z][a-z\d\+\-\.]*:)?\/\//i.test(e)
        }
    }, function(e, t, n) {
        "use strict";
        e.exports = function(e, t) {
            return t ? e.replace(/\/+$/, "") + "/" + t.replace(/^\/+/, "") : e
        }
    }, function(e, t, n) {
        "use strict";

        function r(e) {
            if ("function" != typeof e) throw new TypeError("executor must be a function.");
            var t;
            this.promise = new Promise(function(e) {
                t = e
            });
            var n = this;
            e(function(e) {
                n.reason || (n.reason = new o(e), t(n.reason))
            })
        }
        var o = n(26);
        r.prototype.throwIfRequested = function() {
            if (this.reason) throw this.reason
        }, r.source = function() {
            var e;
            return {
                token: new r(function(t) {
                    e = t
                }),
                cancel: e
            }
        }, e.exports = r
    }, function(e, t, n) {
        "use strict";
        e.exports = function(e) {
            return function(t) {
                return e.apply(null, t)
            }
        }
    }, function(e, t, n) {
        /*!
         * money.js / fx() v0.2
         * Copyright 2014 Open Exchange Rates
         *
         * JavaScript library for realtime currency conversion and exchange rate calculation.
         *
         * Freely distributable under the MIT license.
         * Portions of money.js are inspired by or borrowed from underscore.js
         *
         * For details, examples and documentation:
         * http://openexchangerates.github.io/money.js/
         */
        ! function(n, r) {
            var o = function(e) {
                return new c(e)
            };
            o.version = "0.2";
            var i = n.fxSetup || {
                rates: {},
                base: ""
            };
            o.rates = i.rates, o.base = i.base, o.settings = {
                from: i.from || o.base,
                to: i.to || o.base
            };
            var a = o.convert = function(e, t) {
                    if ("object" == typeof e && e.length) {
                        for (var n = 0; n < e.length; n++) e[n] = a(e[n], t);
                        return e
                    }
                    return t = t || {}, t.from || (t.from = o.settings.from), t.to || (t.to = o.settings.to), e * s(t.to, t.from)
                },
                s = function(e, t) {
                    var n = o.rates;
                    if (n[o.base] = 1, !n[e] || !n[t]) throw "fx error";
                    return t === o.base ? n[e] : e === o.base ? 1 / n[t] : n[e] * (1 / n[t])
                },
                c = function(e) {
                    "string" == typeof e ? (this._v = parseFloat(e.replace(/[^0-9-.]/g, "")), this._fx = e.replace(/([^A-Za-z])/g, "")) : this._v = e
                },
                u = o.prototype = c.prototype;
            u.convert = function() {
                var e = Array.prototype.slice.call(arguments);
                return e.unshift(this._v), a.apply(o, e)
            }, u.from = function(e) {
                var t = o(a(this._v, {
                    from: e,
                    to: o.base
                }));
                return t._fx = o.base, t
            }, u.to = function(e) {
                return a(this._v, {
                    from: this._fx ? this._fx : o.settings.from,
                    to: e
                })
            }, void 0 !== e && e.exports && (t = e.exports = o), t.fx = o
        }(this)
    }, function(e, t, n) {
        "use strict";
        var r = n(81),
            o = n.n(r);
        o.a.settings = {
            currency: {
                symbol: "$",
                format: "%v %s",
                decimal: ".",
                thousand: ",",
                precision: 0
            },
            number: {
                precision: 0,
                thousand: ",",
                decimal: "."
            }
        }, t.a = o.a
    }, function(e, t, n) {
        /*!
         * accounting.js v0.4.1
         * Copyright 2014 Open Exchange Rates
         *
         * Freely distributable under the MIT license.
         * Portions of accounting.js are inspired or borrowed from underscore.js
         *
         * Full details and documentation:
         * http://openexchangerates.github.io/accounting.js/
         */
        ! function(n, r) {
            function o(e) {
                return !!("" === e || e && e.charCodeAt && e.substr)
            }

            function i(e) {
                return h ? h(e) : "[object Array]" === d.call(e)
            }

            function a(e) {
                return e && "[object Object]" === d.call(e)
            }

            function s(e, t) {
                var n;
                e = e || {}, t = t || {};
                for (n in t) t.hasOwnProperty(n) && null == e[n] && (e[n] = t[n]);
                return e
            }

            function c(e, t, n) {
                var r, o, i = [];
                if (!e) return i;
                if (f && e.map === f) return e.map(t, n);
                for (r = 0, o = e.length; r < o; r++) i[r] = t.call(n, e[r], r, e);
                return i
            }

            function u(e, t) {
                return e = Math.round(Math.abs(e)), isNaN(e) ? t : e
            }

            function l(e) {
                var t = p.settings.currency.format;
                return "function" == typeof e && (e = e()), o(e) && e.match("%v") ? {
                    pos: e,
                    neg: e.replace("-", "").replace("%v", "-%v"),
                    zero: e
                } : e && e.pos && e.pos.match("%v") ? e : o(t) ? p.settings.currency.format = {
                    pos: t,
                    neg: t.replace("%v", "-%v"),
                    zero: t
                } : t
            }
            var p = {};
            p.version = "0.4.1", p.settings = {
                currency: {
                    symbol: "$",
                    format: "%s%v",
                    decimal: ".",
                    thousand: ",",
                    precision: 2,
                    grouping: 3
                },
                number: {
                    precision: 0,
                    grouping: 3,
                    thousand: ",",
                    decimal: "."
                }
            };
            var f = Array.prototype.map,
                h = Array.isArray,
                d = Object.prototype.toString,
                m = p.unformat = p.parse = function(e, t) {
                    if (i(e)) return c(e, function(e) {
                        return m(e, t)
                    });
                    if ("number" == typeof(e = e || 0)) return e;
                    t = t || p.settings.number.decimal;
                    var n = new RegExp("[^0-9-" + t + "]", ["g"]),
                        r = parseFloat(("" + e).replace(/\((.*)\)/, "-$1").replace(n, "").replace(t, "."));
                    return isNaN(r) ? 0 : r
                },
                y = p.toFixed = function(e, t) {
                    t = u(t, p.settings.number.precision);
                    var n = Math.pow(10, t);
                    return (Math.round(p.unformat(e) * n) / n).toFixed(t)
                },
                g = p.formatNumber = p.format = function(e, t, n, r) {
                    if (i(e)) return c(e, function(e) {
                        return g(e, t, n, r)
                    });
                    e = m(e);
                    var o = s(a(t) ? t : {
                            precision: t,
                            thousand: n,
                            decimal: r
                        }, p.settings.number),
                        l = u(o.precision),
                        f = e < 0 ? "-" : "",
                        h = parseInt(y(Math.abs(e || 0), l), 10) + "",
                        d = h.length > 3 ? h.length % 3 : 0;
                    return f + (d ? h.substr(0, d) + o.thousand : "") + h.substr(d).replace(/(\d{3})(?=\d)/g, "$1" + o.thousand) + (l ? o.decimal + y(Math.abs(e), l).split(".")[1] : "")
                },
                v = p.formatMoney = function(e, t, n, r, o, f) {
                    if (i(e)) return c(e, function(e) {
                        return v(e, t, n, r, o, f)
                    });
                    e = m(e);
                    var h = s(a(t) ? t : {
                            symbol: t,
                            precision: n,
                            thousand: r,
                            decimal: o,
                            format: f
                        }, p.settings.currency),
                        d = l(h.format);
                    return (e > 0 ? d.pos : e < 0 ? d.neg : d.zero).replace("%s", h.symbol).replace("%v", g(Math.abs(e), u(h.precision), h.thousand, h.decimal))
                };
            p.formatColumn = function(e, t, n, r, f, h) {
                if (!e) return [];
                var d = s(a(t) ? t : {
                        symbol: t,
                        precision: n,
                        thousand: r,
                        decimal: f,
                        format: h
                    }, p.settings.currency),
                    y = l(d.format),
                    v = y.pos.indexOf("%s") < y.pos.indexOf("%v"),
                    b = 0;
                return c(c(e, function(e, t) {
                    if (i(e)) return p.formatColumn(e, d);
                    e = m(e);
                    var n = e > 0 ? y.pos : e < 0 ? y.neg : y.zero,
                        r = n.replace("%s", d.symbol).replace("%v", g(Math.abs(e), u(d.precision), d.thousand, d.decimal));
                    return r.length > b && (b = r.length), r
                }), function(e, t) {
                    return o(e) && e.length < b ? v ? e.replace(d.symbol, d.symbol + new Array(b - e.length + 1).join(" ")) : new Array(b - e.length + 1).join(" ") + e : e
                })
            }, void 0 !== e && e.exports && (t = e.exports = p), t.accounting = p
        }()
    }, function(e, t, n) {
        function r(e, t) {
            "object" == typeof e && (t = e, e = void 0), t = t || {};
            var n, r = o(e),
                i = r.source,
                u = r.id,
                l = r.path,
                p = c[u] && l in c[u].nsps;
            return t.forceNew || t["force new connection"] || !1 === t.multiplex || p ? (s("ignoring socket cache for %s", i), n = a(i, t)) : (c[u] || (s("new io instance for %s", i), c[u] = a(i, t)), n = c[u]), r.query && !t.query && (t.query = r.query), n.socket(r.path, t)
        }
        var o = n(83),
            i = n(16),
            a = n(32),
            s = n(7)("socket.io-client");
        e.exports = t = r;
        var c = t.managers = {};
        t.protocol = i.protocol, t.connect = r, t.Manager = n(32), t.Socket = n(37)
    }, function(e, t, n) {
        (function(t) {
            function r(e, n) {
                var r = e;
                n = n || t.location, null == e && (e = n.protocol + "//" + n.host), "string" == typeof e && ("/" === e.charAt(0) && (e = "/" === e.charAt(1) ? n.protocol + e : n.host + e), /^(https?|wss?):\/\//.test(e) || (i("protocol-less url %s", e), e = void 0 !== n ? n.protocol + "//" + e : "https://" + e), i("parse %s", e), r = o(e)), r.port || (/^(http|ws)$/.test(r.protocol) ? r.port = "80" : /^(http|ws)s$/.test(r.protocol) && (r.port = "443")), r.path = r.path || "/";
                var a = -1 !== r.host.indexOf(":"),
                    s = a ? "[" + r.host + "]" : r.host;
                return r.id = r.protocol + "://" + s + ":" + r.port, r.href = r.protocol + "://" + s + (n && n.port === r.port ? "" : ":" + r.port), r
            }
            var o = n(28),
                i = n(7)("socket.io-client:url");
            e.exports = r
        }).call(t, n(0))
    }, function(e, t, n) {
        function r(e) {
            var n, r = 0;
            for (n in e) r = (r << 5) - r + e.charCodeAt(n), r |= 0;
            return t.colors[Math.abs(r) % t.colors.length]
        }

        function o(e) {
            function n() {
                if (n.enabled) {
                    var e = n,
                        r = +new Date,
                        o = r - (u || r);
                    e.diff = o, e.prev = u, e.curr = r, u = r;
                    for (var i = new Array(arguments.length), a = 0; a < i.length; a++) i[a] = arguments[a];
                    i[0] = t.coerce(i[0]), "string" != typeof i[0] && i.unshift("%O");
                    var s = 0;
                    i[0] = i[0].replace(/%([a-zA-Z%])/g, function(n, r) {
                        if ("%%" === n) return n;
                        s++;
                        var o = t.formatters[r];
                        if ("function" == typeof o) {
                            var a = i[s];
                            n = o.call(e, a), i.splice(s, 1), s--
                        }
                        return n
                    }), t.formatArgs.call(e, i), (n.log || t.log || void 0).apply(e, i)
                }
            }
            return n.namespace = e, n.enabled = t.enabled(e), n.useColors = t.useColors(), n.color = r(e), "function" == typeof t.init && t.init(n), n
        }

        function i(e) {
            t.save(e), t.names = [], t.skips = [];
            for (var n = ("string" == typeof e ? e : "").split(/[\s,]+/), r = n.length, o = 0; o < r; o++) n[o] && (e = n[o].replace(/\*/g, ".*?"), "-" === e[0] ? t.skips.push(new RegExp("^" + e.substr(1) + "$")) : t.names.push(new RegExp("^" + e + "$")))
        }

        function a() {
            t.enable("")
        }

        function s(e) {
            var n, r;
            for (n = 0, r = t.skips.length; n < r; n++)
                if (t.skips[n].test(e)) return !1;
            for (n = 0, r = t.names.length; n < r; n++)
                if (t.names[n].test(e)) return !0;
            return !1
        }

        function c(e) {
            return e instanceof Error ? e.stack || e.message : e
        }
        t = e.exports = o.debug = o.default = o, t.coerce = c, t.disable = a, t.enable = i, t.enabled = s, t.humanize = n(15), t.names = [], t.skips = [], t.formatters = {};
        var u
    }, function(e, t, n) {
        (function(r) {
            function o() {
                return !("undefined" == typeof window || !window.process || "renderer" !== window.process.type) || ("undefined" == typeof navigator || !navigator.userAgent || !navigator.userAgent.toLowerCase().match(/(edge|trident)\/(\d+)/)) && ("undefined" != typeof document && document.documentElement && document.documentElement.style && document.documentElement.style.WebkitAppearance || "undefined" != typeof window && window.console && (window.console.firebug || window.console.exception && window.console.table) || "undefined" != typeof navigator && navigator.userAgent && navigator.userAgent.toLowerCase().match(/firefox\/(\d+)/) && parseInt(RegExp.$1, 10) >= 31 || "undefined" != typeof navigator && navigator.userAgent && navigator.userAgent.toLowerCase().match(/applewebkit\/(\d+)/))
            }

            function i(e) {
                var n = this.useColors;
                if (e[0] = (n ? "%c" : "") + this.namespace + (n ? " %c" : " ") + e[0] + (n ? "%c " : " ") + "+" + t.humanize(this.diff), n) {
                    var r = "color: " + this.color;
                    e.splice(1, 0, r, "color: inherit");
                    var o = 0,
                        i = 0;
                    e[0].replace(/%[a-zA-Z%]/g, function(e) {
                        "%%" !== e && (o++, "%c" === e && (i = o))
                    }), e.splice(i, 0, r)
                }
            }

            function a() {
                return "object" == typeof console && console.log && Function.prototype.apply.call(console.log, console, arguments)
            }

            function s(e) {
                try {
                    null == e ? t.storage.removeItem("debug") : t.storage.debug = e
                } catch (e) {}
            }

            function c() {
                var e;
                try {
                    e = t.storage.debug
                } catch (e) {}
                return !e && void 0 !== r && "env" in r && (e = r.env.DEBUG), e
            }
            t = e.exports = n(86), t.log = a, t.formatArgs = i, t.save = s, t.load = c, t.useColors = o, t.storage = "undefined" != typeof chrome && void 0 !== chrome.storage ? chrome.storage.local : function() {
                try {
                    return window.localStorage
                } catch (e) {}
            }(), t.colors = ["#0000CC", "#0000FF", "#0033CC", "#0033FF", "#0066CC", "#0066FF", "#0099CC", "#0099FF", "#00CC00", "#00CC33", "#00CC66", "#00CC99", "#00CCCC", "#00CCFF", "#3300CC", "#3300FF", "#3333CC", "#3333FF", "#3366CC", "#3366FF", "#3399CC", "#3399FF", "#33CC00", "#33CC33", "#33CC66", "#33CC99", "#33CCCC", "#33CCFF", "#6600CC", "#6600FF", "#6633CC", "#6633FF", "#66CC00", "#66CC33", "#9900CC", "#9900FF", "#9933CC", "#9933FF", "#99CC00", "#99CC33", "#CC0000", "#CC0033", "#CC0066", "#CC0099", "#CC00CC", "#CC00FF", "#CC3300", "#CC3333", "#CC3366", "#CC3399", "#CC33CC", "#CC33FF", "#CC6600", "#CC6633", "#CC9900", "#CC9933", "#CCCC00", "#CCCC33", "#FF0000", "#FF0033", "#FF0066", "#FF0099", "#FF00CC", "#FF00FF", "#FF3300", "#FF3333", "#FF3366", "#FF3399", "#FF33CC", "#FF33FF", "#FF6600", "#FF6633", "#FF9900", "#FF9933", "#FFCC00", "#FFCC33"], t.formatters.j = function(e) {
                try {
                    return JSON.stringify(e)
                } catch (e) {
                    return "[UnexpectedJSONParseError]: " + e.message
                }
            }, t.enable(c())
        }).call(t, n(6))
    }, function(e, t, n) {
        function r(e) {
            var n, r = 0;
            for (n in e) r = (r << 5) - r + e.charCodeAt(n), r |= 0;
            return t.colors[Math.abs(r) % t.colors.length]
        }

        function o(e) {
            function n() {
                if (n.enabled) {
                    var e = n,
                        r = +new Date,
                        i = r - (o || r);
                    e.diff = i, e.prev = o, e.curr = r, o = r;
                    for (var a = new Array(arguments.length), s = 0; s < a.length; s++) a[s] = arguments[s];
                    a[0] = t.coerce(a[0]), "string" != typeof a[0] && a.unshift("%O");
                    var c = 0;
                    a[0] = a[0].replace(/%([a-zA-Z%])/g, function(n, r) {
                        if ("%%" === n) return n;
                        c++;
                        var o = t.formatters[r];
                        if ("function" == typeof o) {
                            var i = a[c];
                            n = o.call(e, i), a.splice(c, 1), c--
                        }
                        return n
                    }), t.formatArgs.call(e, a), (n.log || t.log || void 0).apply(e, a)
                }
            }
            var o;
            return n.namespace = e, n.enabled = t.enabled(e), n.useColors = t.useColors(), n.color = r(e), n.destroy = i, "function" == typeof t.init && t.init(n), t.instances.push(n), n
        }

        function i() {
            var e = t.instances.indexOf(this);
            return -1 !== e && (t.instances.splice(e, 1), !0)
        }

        function a(e) {
            t.save(e), t.names = [], t.skips = [];
            var n, r = ("string" == typeof e ? e : "").split(/[\s,]+/),
                o = r.length;
            for (n = 0; n < o; n++) r[n] && (e = r[n].replace(/\*/g, ".*?"), "-" === e[0] ? t.skips.push(new RegExp("^" + e.substr(1) + "$")) : t.names.push(new RegExp("^" + e + "$")));
            for (n = 0; n < t.instances.length; n++) {
                var i = t.instances[n];
                i.enabled = t.enabled(i.namespace)
            }
        }

        function s() {
            t.enable("")
        }

        function c(e) {
            if ("*" === e[e.length - 1]) return !0;
            var n, r;
            for (n = 0, r = t.skips.length; n < r; n++)
                if (t.skips[n].test(e)) return !1;
            for (n = 0, r = t.names.length; n < r; n++)
                if (t.names[n].test(e)) return !0;
            return !1
        }

        function u(e) {
            return e instanceof Error ? e.stack || e.message : e
        }
        t = e.exports = o.debug = o.default = o, t.coerce = u, t.disable = s, t.enable = a, t.enabled = c, t.humanize = n(15), t.instances = [], t.names = [], t.skips = [], t.formatters = {}
    }, function(e, t) {
        var n = {}.toString;
        e.exports = Array.isArray || function(e) {
            return "[object Array]" == n.call(e)
        }
    }, function(e, t, n) {
        (function(e) {
            function r(e, t) {
                if (!e) return e;
                if (a(e)) {
                    var n = {
                        _placeholder: !0,
                        num: t.length
                    };
                    return t.push(e), n
                }
                if (i(e)) {
                    for (var o = new Array(e.length), s = 0; s < e.length; s++) o[s] = r(e[s], t);
                    return o
                }
                if ("object" == typeof e && !(e instanceof Date)) {
                    var o = {};
                    for (var c in e) o[c] = r(e[c], t);
                    return o
                }
                return e
            }

            function o(e, t) {
                if (!e) return e;
                if (e && e._placeholder) return t[e.num];
                if (i(e))
                    for (var n = 0; n < e.length; n++) e[n] = o(e[n], t);
                else if ("object" == typeof e)
                    for (var r in e) e[r] = o(e[r], t);
                return e
            }
            var i = n(30),
                a = n(31),
                s = Object.prototype.toString,
                c = "function" == typeof e.Blob || "[object BlobConstructor]" === s.call(e.Blob),
                u = "function" == typeof e.File || "[object FileConstructor]" === s.call(e.File);
            t.deconstructPacket = function(e) {
                var t = [],
                    n = e.data,
                    o = e;
                return o.data = r(n, t), o.attachments = t.length, {
                    packet: o,
                    buffers: t
                }
            }, t.reconstructPacket = function(e, t) {
                return e.data = o(e.data, t), e.attachments = void 0, e
            }, t.removeBlobs = function(e, t) {
                function n(e, s, l) {
                    if (!e) return e;
                    if (c && e instanceof Blob || u && e instanceof File) {
                        r++;
                        var p = new FileReader;
                        p.onload = function() {
                            l ? l[s] = this.result : o = this.result, --r || t(o)
                        }, p.readAsArrayBuffer(e)
                    } else if (i(e))
                        for (var f = 0; f < e.length; f++) n(e[f], f, e);
                    else if ("object" == typeof e && !a(e))
                        for (var h in e) n(e[h], h, e)
                }
                var r = 0,
                    o = e;
                n(o), r || t(o)
            }
        }).call(t, n(0))
    }, function(e, t, n) {
        e.exports = n(90), e.exports.parser = n(5)
    }, function(e, t, n) {
        (function(t) {
            function r(e, n) {
                if (!(this instanceof r)) return new r(e, n);
                n = n || {}, e && "object" == typeof e && (n = e, e = null), e ? (e = l(e), n.hostname = e.host, n.secure = "https" === e.protocol || "wss" === e.protocol, n.port = e.port, e.query && (n.query = e.query)) : n.host && (n.hostname = l(n.host).host), this.secure = null != n.secure ? n.secure : t.location && "https:" === location.protocol, n.hostname && !n.port && (n.port = this.secure ? "443" : "80"), this.agent = n.agent || !1, this.hostname = n.hostname || (t.location ? location.hostname : "localhost"), this.port = n.port || (t.location && location.port ? location.port : this.secure ? 443 : 80), this.query = n.query || {}, "string" == typeof this.query && (this.query = p.decode(this.query)), this.upgrade = !1 !== n.upgrade, this.path = (n.path || "/engine.io").replace(/\/$/, "") + "/", this.forceJSONP = !!n.forceJSONP, this.jsonp = !1 !== n.jsonp, this.forceBase64 = !!n.forceBase64, this.enablesXDR = !!n.enablesXDR, this.timestampParam = n.timestampParam || "t", this.timestampRequests = n.timestampRequests, this.transports = n.transports || ["polling", "websocket"], this.transportOptions = n.transportOptions || {}, this.readyState = "", this.writeBuffer = [], this.prevBufferLen = 0, this.policyPort = n.policyPort || 843, this.rememberUpgrade = n.rememberUpgrade || !1, this.binaryType = null, this.onlyBinaryUpgrades = n.onlyBinaryUpgrades, this.perMessageDeflate = !1 !== n.perMessageDeflate && (n.perMessageDeflate || {}), !0 === this.perMessageDeflate && (this.perMessageDeflate = {}), this.perMessageDeflate && null == this.perMessageDeflate.threshold && (this.perMessageDeflate.threshold = 1024), this.pfx = n.pfx || null, this.key = n.key || null, this.passphrase = n.passphrase || null, this.cert = n.cert || null, this.ca = n.ca || null, this.ciphers = n.ciphers || null, this.rejectUnauthorized = void 0 === n.rejectUnauthorized || n.rejectUnauthorized, this.forceNode = !!n.forceNode;
                var o = "object" == typeof t && t;
                o.global === o && (n.extraHeaders && Object.keys(n.extraHeaders).length > 0 && (this.extraHeaders = n.extraHeaders), n.localAddress && (this.localAddress = n.localAddress)), this.id = null, this.upgrades = null, this.pingInterval = null, this.pingTimeout = null, this.pingIntervalTimer = null, this.pingTimeoutTimer = null, this.open()
            }

            function o(e) {
                var t = {};
                for (var n in e) e.hasOwnProperty(n) && (t[n] = e[n]);
                return t
            }
            var i = n(33),
                a = n(4),
                s = n(10)("engine.io-client:socket"),
                c = n(36),
                u = n(5),
                l = n(28),
                p = n(8);
            e.exports = r, r.priorWebsocketSuccess = !1, a(r.prototype), r.protocol = u.protocol, r.Socket = r, r.Transport = n(18), r.transports = n(33), r.parser = n(5), r.prototype.createTransport = function(e) {
                s('creating transport "%s"', e);
                var t = o(this.query);
                t.EIO = u.protocol, t.transport = e;
                var n = this.transportOptions[e] || {};
                return this.id && (t.sid = this.id), new i[e]({
                    query: t,
                    socket: this,
                    agent: n.agent || this.agent,
                    hostname: n.hostname || this.hostname,
                    port: n.port || this.port,
                    secure: n.secure || this.secure,
                    path: n.path || this.path,
                    forceJSONP: n.forceJSONP || this.forceJSONP,
                    jsonp: n.jsonp || this.jsonp,
                    forceBase64: n.forceBase64 || this.forceBase64,
                    enablesXDR: n.enablesXDR || this.enablesXDR,
                    timestampRequests: n.timestampRequests || this.timestampRequests,
                    timestampParam: n.timestampParam || this.timestampParam,
                    policyPort: n.policyPort || this.policyPort,
                    pfx: n.pfx || this.pfx,
                    key: n.key || this.key,
                    passphrase: n.passphrase || this.passphrase,
                    cert: n.cert || this.cert,
                    ca: n.ca || this.ca,
                    ciphers: n.ciphers || this.ciphers,
                    rejectUnauthorized: n.rejectUnauthorized || this.rejectUnauthorized,
                    perMessageDeflate: n.perMessageDeflate || this.perMessageDeflate,
                    extraHeaders: n.extraHeaders || this.extraHeaders,
                    forceNode: n.forceNode || this.forceNode,
                    localAddress: n.localAddress || this.localAddress,
                    requestTimeout: n.requestTimeout || this.requestTimeout,
                    protocols: n.protocols || void 0
                })
            }, r.prototype.open = function() {
                var e;
                if (this.rememberUpgrade && r.priorWebsocketSuccess && -1 !== this.transports.indexOf("websocket")) e = "websocket";
                else {
                    if (0 === this.transports.length) {
                        var t = this;
                        return void setTimeout(function() {
                            t.emit("error", "No transports available")
                        }, 0)
                    }
                    e = this.transports[0]
                }
                this.readyState = "opening";
                try {
                    e = this.createTransport(e)
                } catch (e) {
                    return this.transports.shift(), void this.open()
                }
                e.open(), this.setTransport(e)
            }, r.prototype.setTransport = function(e) {
                s("setting transport %s", e.name);
                var t = this;
                this.transport && (s("clearing existing transport %s", this.transport.name), this.transport.removeAllListeners()), this.transport = e, e.on("drain", function() {
                    t.onDrain()
                }).on("packet", function(e) {
                    t.onPacket(e)
                }).on("error", function(e) {
                    t.onError(e)
                }).on("close", function() {
                    t.onClose("transport close")
                })
            }, r.prototype.probe = function(e) {
                function t() {
                    if (f.onlyBinaryUpgrades) {
                        var t = !this.supportsBinary && f.transport.supportsBinary;
                        p = p || t
                    }
                    p || (s('probe transport "%s" opened', e), l.send([{
                        type: "ping",
                        data: "probe"
                    }]), l.once("packet", function(t) {
                        if (!p)
                            if ("pong" === t.type && "probe" === t.data) {
                                if (s('probe transport "%s" pong', e), f.upgrading = !0, f.emit("upgrading", l), !l) return;
                                r.priorWebsocketSuccess = "websocket" === l.name, s('pausing current transport "%s"', f.transport.name), f.transport.pause(function() {
                                    p || "closed" !== f.readyState && (s("changing transport and sending upgrade packet"), u(), f.setTransport(l), l.send([{
                                        type: "upgrade"
                                    }]), f.emit("upgrade", l), l = null, f.upgrading = !1, f.flush())
                                })
                            } else {
                                s('probe transport "%s" failed', e);
                                var n = new Error("probe error");
                                n.transport = l.name, f.emit("upgradeError", n)
                            }
                    }))
                }

                function n() {
                    p || (p = !0, u(), l.close(), l = null)
                }

                function o(t) {
                    var r = new Error("probe error: " + t);
                    r.transport = l.name, n(), s('probe transport "%s" failed because of error: %s', e, t), f.emit("upgradeError", r)
                }

                function i() {
                    o("transport closed")
                }

                function a() {
                    o("socket closed")
                }

                function c(e) {
                    l && e.name !== l.name && (s('"%s" works - aborting "%s"', e.name, l.name), n())
                }

                function u() {
                    l.removeListener("open", t), l.removeListener("error", o), l.removeListener("close", i), f.removeListener("close", a), f.removeListener("upgrading", c)
                }
                s('probing transport "%s"', e);
                var l = this.createTransport(e, {
                        probe: 1
                    }),
                    p = !1,
                    f = this;
                r.priorWebsocketSuccess = !1, l.once("open", t), l.once("error", o), l.once("close", i), this.once("close", a), this.once("upgrading", c), l.open()
            }, r.prototype.onOpen = function() {
                if (s("socket open"), this.readyState = "open", r.priorWebsocketSuccess = "websocket" === this.transport.name, this.emit("open"), this.flush(), "open" === this.readyState && this.upgrade && this.transport.pause) {
                    s("starting upgrade probes");
                    for (var e = 0, t = this.upgrades.length; e < t; e++) this.probe(this.upgrades[e])
                }
            }, r.prototype.onPacket = function(e) {
                if ("opening" === this.readyState || "open" === this.readyState || "closing" === this.readyState) switch (s('socket receive: type "%s", data "%s"', e.type, e.data), this.emit("packet", e), this.emit("heartbeat"), e.type) {
                    case "open":
                        this.onHandshake(JSON.parse(e.data));
                        break;
                    case "pong":
                        this.setPing(), this.emit("pong");
                        break;
                    case "error":
                        var t = new Error("server error");
                        t.code = e.data, this.onError(t);
                        break;
                    case "message":
                        this.emit("data", e.data), this.emit("message", e.data)
                } else s('packet received with socket readyState "%s"', this.readyState)
            }, r.prototype.onHandshake = function(e) {
                this.emit("handshake", e), this.id = e.sid, this.transport.query.sid = e.sid, this.upgrades = this.filterUpgrades(e.upgrades), this.pingInterval = e.pingInterval, this.pingTimeout = e.pingTimeout, this.onOpen(), "closed" !== this.readyState && (this.setPing(), this.removeListener("heartbeat", this.onHeartbeat), this.on("heartbeat", this.onHeartbeat))
            }, r.prototype.onHeartbeat = function(e) {
                clearTimeout(this.pingTimeoutTimer);
                var t = this;
                t.pingTimeoutTimer = setTimeout(function() {
                    "closed" !== t.readyState && t.onClose("ping timeout")
                }, e || t.pingInterval + t.pingTimeout)
            }, r.prototype.setPing = function() {
                var e = this;
                clearTimeout(e.pingIntervalTimer), e.pingIntervalTimer = setTimeout(function() {
                    s("writing ping packet - expecting pong within %sms", e.pingTimeout), e.ping(), e.onHeartbeat(e.pingTimeout)
                }, e.pingInterval)
            }, r.prototype.ping = function() {
                var e = this;
                this.sendPacket("ping", function() {
                    e.emit("ping")
                })
            }, r.prototype.onDrain = function() {
                this.writeBuffer.splice(0, this.prevBufferLen), this.prevBufferLen = 0, 0 === this.writeBuffer.length ? this.emit("drain") : this.flush()
            }, r.prototype.flush = function() {
                "closed" !== this.readyState && this.transport.writable && !this.upgrading && this.writeBuffer.length && (s("flushing %d packets in socket", this.writeBuffer.length), this.transport.send(this.writeBuffer), this.prevBufferLen = this.writeBuffer.length, this.emit("flush"))
            }, r.prototype.write = r.prototype.send = function(e, t, n) {
                return this.sendPacket("message", e, t, n), this
            }, r.prototype.sendPacket = function(e, t, n, r) {
                if ("function" == typeof t && (r = t, t = void 0), "function" == typeof n && (r = n, n = null), "closing" !== this.readyState && "closed" !== this.readyState) {
                    n = n || {}, n.compress = !1 !== n.compress;
                    var o = {
                        type: e,
                        data: t,
                        options: n
                    };
                    this.emit("packetCreate", o), this.writeBuffer.push(o), r && this.once("flush", r), this.flush()
                }
            }, r.prototype.close = function() {
                function e() {
                    r.onClose("forced close"), s("socket closing - telling transport to close"), r.transport.close()
                }

                function t() {
                    r.removeListener("upgrade", t), r.removeListener("upgradeError", t), e()
                }

                function n() {
                    r.once("upgrade", t), r.once("upgradeError", t)
                }
                if ("opening" === this.readyState || "open" === this.readyState) {
                    this.readyState = "closing";
                    var r = this;
                    this.writeBuffer.length ? this.once("drain", function() {
                        this.upgrading ? n() : e()
                    }) : this.upgrading ? n() : e()
                }
                return this
            }, r.prototype.onError = function(e) {
                s("socket error %j", e), r.priorWebsocketSuccess = !1, this.emit("error", e), this.onClose("transport error", e)
            }, r.prototype.onClose = function(e, t) {
                if ("opening" === this.readyState || "open" === this.readyState || "closing" === this.readyState) {
                    s('socket close with reason: "%s"', e);
                    var n = this;
                    clearTimeout(this.pingIntervalTimer), clearTimeout(this.pingTimeoutTimer), this.transport.removeAllListeners("close"), this.transport.close(), this.transport.removeAllListeners(), this.readyState = "closed", this.id = null, this.emit("close", e, t), n.writeBuffer = [], n.prevBufferLen = 0
                }
            }, r.prototype.filterUpgrades = function(e) {
                for (var t = [], n = 0, r = e.length; n < r; n++) ~c(this.transports, e[n]) && t.push(e[n]);
                return t
            }
        }).call(t, n(0))
    }, function(e, t) {
        try {
            e.exports = "undefined" != typeof XMLHttpRequest && "withCredentials" in new XMLHttpRequest
        } catch (t) {
            e.exports = !1
        }
    }, function(e, t, n) {
        (function(t) {
            function r() {}

            function o(e) {
                if (c.call(this, e), this.requestTimeout = e.requestTimeout, this.extraHeaders = e.extraHeaders, t.location) {
                    var n = "https:" === location.protocol,
                        r = location.port;
                    r || (r = n ? 443 : 80), this.xd = e.hostname !== t.location.hostname || r !== e.port, this.xs = e.secure !== n
                }
            }

            function i(e) {
                this.method = e.method || "GET", this.uri = e.uri, this.xd = !!e.xd, this.xs = !!e.xs, this.async = !1 !== e.async, this.data = void 0 !== e.data ? e.data : null, this.agent = e.agent, this.isBinary = e.isBinary, this.supportsBinary = e.supportsBinary, this.enablesXDR = e.enablesXDR, this.requestTimeout = e.requestTimeout, this.pfx = e.pfx, this.key = e.key, this.passphrase = e.passphrase, this.cert = e.cert, this.ca = e.ca, this.ciphers = e.ciphers, this.rejectUnauthorized = e.rejectUnauthorized, this.extraHeaders = e.extraHeaders, this.create()
            }

            function a() {
                for (var e in i.requests) i.requests.hasOwnProperty(e) && i.requests[e].abort()
            }
            var s = n(17),
                c = n(34),
                u = n(4),
                l = n(9),
                p = n(10)("engine.io-client:polling-xhr");
            e.exports = o, e.exports.Request = i, l(o, c), o.prototype.supportsBinary = !0, o.prototype.request = function(e) {
                return e = e || {}, e.uri = this.uri(), e.xd = this.xd, e.xs = this.xs, e.agent = this.agent || !1, e.supportsBinary = this.supportsBinary, e.enablesXDR = this.enablesXDR, e.pfx = this.pfx, e.key = this.key, e.passphrase = this.passphrase, e.cert = this.cert, e.ca = this.ca, e.ciphers = this.ciphers, e.rejectUnauthorized = this.rejectUnauthorized, e.requestTimeout = this.requestTimeout, e.extraHeaders = this.extraHeaders, new i(e)
            }, o.prototype.doWrite = function(e, t) {
                var n = "string" != typeof e && void 0 !== e,
                    r = this.request({
                        method: "POST",
                        data: e,
                        isBinary: n
                    }),
                    o = this;
                r.on("success", t), r.on("error", function(e) {
                    o.onError("xhr post error", e)
                }), this.sendXhr = r
            }, o.prototype.doPoll = function() {
                p("xhr poll");
                var e = this.request(),
                    t = this;
                e.on("data", function(e) {
                    t.onData(e)
                }), e.on("error", function(e) {
                    t.onError("xhr poll error", e)
                }), this.pollXhr = e
            }, u(i.prototype), i.prototype.create = function() {
                var e = {
                    agent: this.agent,
                    xdomain: this.xd,
                    xscheme: this.xs,
                    enablesXDR: this.enablesXDR
                };
                e.pfx = this.pfx, e.key = this.key, e.passphrase = this.passphrase, e.cert = this.cert, e.ca = this.ca, e.ciphers = this.ciphers, e.rejectUnauthorized = this.rejectUnauthorized;
                var n = this.xhr = new s(e),
                    r = this;
                try {
                    p("xhr open %s: %s", this.method, this.uri), n.open(this.method, this.uri, this.async);
                    try {
                        if (this.extraHeaders) {
                            n.setDisableHeaderCheck && n.setDisableHeaderCheck(!0);
                            for (var o in this.extraHeaders) this.extraHeaders.hasOwnProperty(o) && n.setRequestHeader(o, this.extraHeaders[o])
                        }
                    } catch (e) {}
                    if ("POST" === this.method) try {
                        this.isBinary ? n.setRequestHeader("Content-type", "application/octet-stream") : n.setRequestHeader("Content-type", "text/plain;charset=UTF-8")
                    } catch (e) {}
                    try {
                        n.setRequestHeader("Accept", "*/*")
                    } catch (e) {}
                    this.supportsBinary && (n.responseType = "arraybuffer"), "withCredentials" in n && (n.withCredentials = !0), this.requestTimeout && (n.timeout = this.requestTimeout), this.hasXDR() ? (n.onload = function() {
                        r.onLoad()
                    }, n.onerror = function() {
                        r.onError(n.responseText)
                    }) : n.onreadystatechange = function() {
                        if (2 === n.readyState) try {
                            "application/octet-stream" !== n.getResponseHeader("Content-Type") && (n.responseType = "text")
                        } catch (e) {}
                        4 === n.readyState && (200 === n.status || 1223 === n.status ? r.onLoad() : setTimeout(function() {
                            r.onError(n.status)
                        }, 0))
                    }, p("xhr data %s", this.data), n.send(this.data)
                } catch (e) {
                    return void setTimeout(function() {
                        r.onError(e)
                    }, 0)
                }
                t.document && (this.index = i.requestsCount++, i.requests[this.index] = this)
            }, i.prototype.onSuccess = function() {
                this.emit("success"), this.cleanup()
            }, i.prototype.onData = function(e) {
                this.emit("data", e), this.onSuccess()
            }, i.prototype.onError = function(e) {
                this.emit("error", e), this.cleanup(!0)
            }, i.prototype.cleanup = function(e) {
                if (void 0 !== this.xhr && null !== this.xhr) {
                    if (this.hasXDR() ? this.xhr.onload = this.xhr.onerror = r : this.xhr.onreadystatechange = r, e) try {
                        this.xhr.abort()
                    } catch (e) {}
                    t.document && delete i.requests[this.index], this.xhr = null
                }
            }, i.prototype.onLoad = function() {
                var e;
                try {
                    var t;
                    try {
                        t = this.xhr.getResponseHeader("Content-Type")
                    } catch (e) {}
                    e = "application/octet-stream" === t ? "arraybuffer" === this.xhr.responseType ? this.xhr.response || this.xhr.responseText : String.fromCharCode.apply(null, new Uint8Array(this.xhr.response)) : this.xhr.responseText
                } catch (e) {
                    this.onError(e)
                }
                null != e && this.onData(e)
            }, i.prototype.hasXDR = function() {
                return void 0 !== t.XDomainRequest && !this.xs && this.enablesXDR
            }, i.prototype.abort = function() {
                this.cleanup()
            }, i.requestsCount = 0, i.requests = {}, t.document && (t.attachEvent ? t.attachEvent("onunload", a) : t.addEventListener && t.addEventListener("beforeunload", a, !1))
        }).call(t, n(0))
    }, function(e, t) {
        e.exports = Object.keys || function(e) {
            var t = [],
                n = Object.prototype.hasOwnProperty;
            for (var r in e) n.call(e, r) && t.push(r);
            return t
        }
    }, function(e, t) {
        e.exports = function(e, t, n) {
            var r = e.byteLength;
            if (t = t || 0, n = n || r, e.slice) return e.slice(t, n);
            if (t < 0 && (t += r), n < 0 && (n += r), n > r && (n = r), t >= r || t >= n || 0 === r) return new ArrayBuffer(0);
            for (var o = new Uint8Array(e), i = new Uint8Array(n - t), a = t, s = 0; a < n; a++, s++) i[s] = o[a];
            return i.buffer
        }
    }, function(e, t) {
        function n(e, t, n) {
            function o(e, r) {
                if (o.count <= 0) throw new Error("after called too many times");
                --o.count, e ? (i = !0, t(e), t = n) : 0 !== o.count || i || t(null, r)
            }
            var i = !1;
            return n = n || r, o.count = e, 0 === e ? t() : o
        }

        function r() {}
        e.exports = n
    }, function(e, t, n) {
        (function(e, r) {
            var o;
            ! function(r) {
                function i(e) {
                    for (var t, n, r = [], o = 0, i = e.length; o < i;) t = e.charCodeAt(o++), t >= 55296 && t <= 56319 && o < i ? (n = e.charCodeAt(o++), 56320 == (64512 & n) ? r.push(((1023 & t) << 10) + (1023 & n) + 65536) : (r.push(t), o--)) : r.push(t);
                    return r
                }

                function a(e) {
                    for (var t, n = e.length, r = -1, o = ""; ++r < n;) t = e[r], t > 65535 && (t -= 65536, o += g(t >>> 10 & 1023 | 55296), t = 56320 | 1023 & t), o += g(t);
                    return o
                }

                function s(e, t) {
                    if (e >= 55296 && e <= 57343) {
                        if (t) throw Error("Lone surrogate U+" + e.toString(16).toUpperCase() + " is not a scalar value");
                        return !1
                    }
                    return !0
                }

                function c(e, t) {
                    return g(e >> t & 63 | 128)
                }

                function u(e, t) {
                    if (0 == (4294967168 & e)) return g(e);
                    var n = "";
                    return 0 == (4294965248 & e) ? n = g(e >> 6 & 31 | 192) : 0 == (4294901760 & e) ? (s(e, t) || (e = 65533), n = g(e >> 12 & 15 | 224), n += c(e, 6)) : 0 == (4292870144 & e) && (n = g(e >> 18 & 7 | 240), n += c(e, 12), n += c(e, 6)), n += g(63 & e | 128)
                }

                function l(e, t) {
                    t = t || {};
                    for (var n, r = !1 !== t.strict, o = i(e), a = o.length, s = -1, c = ""; ++s < a;) n = o[s], c += u(n, r);
                    return c
                }

                function p() {
                    if (y >= m) throw Error("Invalid byte index");
                    var e = 255 & d[y];
                    if (y++, 128 == (192 & e)) return 63 & e;
                    throw Error("Invalid continuation byte")
                }

                function f(e) {
                    var t, n, r, o, i;
                    if (y > m) throw Error("Invalid byte index");
                    if (y == m) return !1;
                    if (t = 255 & d[y], y++, 0 == (128 & t)) return t;
                    if (192 == (224 & t)) {
                        if (n = p(), (i = (31 & t) << 6 | n) >= 128) return i;
                        throw Error("Invalid continuation byte")
                    }
                    if (224 == (240 & t)) {
                        if (n = p(), r = p(), (i = (15 & t) << 12 | n << 6 | r) >= 2048) return s(i, e) ? i : 65533;
                        throw Error("Invalid continuation byte")
                    }
                    if (240 == (248 & t) && (n = p(), r = p(), o = p(), (i = (7 & t) << 18 | n << 12 | r << 6 | o) >= 65536 && i <= 1114111)) return i;
                    throw Error("Invalid UTF-8 detected")
                }

                function h(e, t) {
                    t = t || {};
                    var n = !1 !== t.strict;
                    d = i(e), m = d.length, y = 0;
                    for (var r, o = []; !1 !== (r = f(n));) o.push(r);
                    return a(o)
                }
                var d, m, y, g = ("object" == typeof e && e && e.exports, String.fromCharCode),
                    v = {
                        version: "2.1.2",
                        encode: l,
                        decode: h
                    };
                void 0 !== (o = function() {
                    return v
                }.call(t, n, t, e)) && (e.exports = o)
            }()
        }).call(t, n(97)(e), n(0))
    }, function(e, t) {
        e.exports = function(e) {
            return e.webpackPolyfill || (e.deprecate = function() {}, e.paths = [], e.children || (e.children = []), Object.defineProperty(e, "loaded", {
                enumerable: !0,
                get: function() {
                    return e.l
                }
            }), Object.defineProperty(e, "id", {
                enumerable: !0,
                get: function() {
                    return e.i
                }
            }), e.webpackPolyfill = 1), e
        }
    }, function(e, t) {
        ! function() {
            "use strict";
            for (var e = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", n = new Uint8Array(256), r = 0; r < e.length; r++) n[e.charCodeAt(r)] = r;
            t.encode = function(t) {
                var n, r = new Uint8Array(t),
                    o = r.length,
                    i = "";
                for (n = 0; n < o; n += 3) i += e[r[n] >> 2], i += e[(3 & r[n]) << 4 | r[n + 1] >> 4], i += e[(15 & r[n + 1]) << 2 | r[n + 2] >> 6], i += e[63 & r[n + 2]];
                return o % 3 == 2 ? i = i.substring(0, i.length - 1) + "=" : o % 3 == 1 && (i = i.substring(0, i.length - 2) + "=="), i
            }, t.decode = function(e) {
                var t, r, o, i, a, s = .75 * e.length,
                    c = e.length,
                    u = 0;
                "=" === e[e.length - 1] && (s--, "=" === e[e.length - 2] && s--);
                var l = new ArrayBuffer(s),
                    p = new Uint8Array(l);
                for (t = 0; t < c; t += 4) r = n[e.charCodeAt(t)], o = n[e.charCodeAt(t + 1)], i = n[e.charCodeAt(t + 2)], a = n[e.charCodeAt(t + 3)], p[u++] = r << 2 | o >> 4, p[u++] = (15 & o) << 4 | i >> 2, p[u++] = (3 & i) << 6 | 63 & a;
                return l
            }
        }()
    }, function(e, t, n) {
        (function(t) {
            function n(e) {
                for (var t = 0; t < e.length; t++) {
                    var n = e[t];
                    if (n.buffer instanceof ArrayBuffer) {
                        var r = n.buffer;
                        if (n.byteLength !== r.byteLength) {
                            var o = new Uint8Array(n.byteLength);
                            o.set(new Uint8Array(r, n.byteOffset, n.byteLength)), r = o.buffer
                        }
                        e[t] = r
                    }
                }
            }

            function r(e, t) {
                t = t || {};
                var r = new i;
                n(e);
                for (var o = 0; o < e.length; o++) r.append(e[o]);
                return t.type ? r.getBlob(t.type) : r.getBlob()
            }

            function o(e, t) {
                return n(e), new Blob(e, t || {})
            }
            var i = t.BlobBuilder || t.WebKitBlobBuilder || t.MSBlobBuilder || t.MozBlobBuilder,
                a = function() {
                    try {
                        return 2 === new Blob(["hi"]).size
                    } catch (e) {
                        return !1
                    }
                }(),
                s = a && function() {
                    try {
                        return 2 === new Blob([new Uint8Array([1, 2])]).size
                    } catch (e) {
                        return !1
                    }
                }(),
                c = i && i.prototype.append && i.prototype.getBlob;
            e.exports = function() {
                return a ? s ? t.Blob : o : c ? r : void 0
            }()
        }).call(t, n(0))
    }, function(e, t, n) {
        function r(e) {
            var n, r = 0;
            for (n in e) r = (r << 5) - r + e.charCodeAt(n), r |= 0;
            return t.colors[Math.abs(r) % t.colors.length]
        }

        function o(e) {
            function n() {
                if (n.enabled) {
                    var e = n,
                        r = +new Date,
                        i = r - (o || r);
                    e.diff = i, e.prev = o, e.curr = r, o = r;
                    for (var a = new Array(arguments.length), s = 0; s < a.length; s++) a[s] = arguments[s];
                    a[0] = t.coerce(a[0]), "string" != typeof a[0] && a.unshift("%O");
                    var c = 0;
                    a[0] = a[0].replace(/%([a-zA-Z%])/g, function(n, r) {
                        if ("%%" === n) return n;
                        c++;
                        var o = t.formatters[r];
                        if ("function" == typeof o) {
                            var i = a[c];
                            n = o.call(e, i), a.splice(c, 1), c--
                        }
                        return n
                    }), t.formatArgs.call(e, a), (n.log || t.log || void 0).apply(e, a)
                }
            }
            var o;
            return n.namespace = e, n.enabled = t.enabled(e), n.useColors = t.useColors(), n.color = r(e), n.destroy = i, "function" == typeof t.init && t.init(n), t.instances.push(n), n
        }

        function i() {
            var e = t.instances.indexOf(this);
            return -1 !== e && (t.instances.splice(e, 1), !0)
        }

        function a(e) {
            t.save(e), t.names = [], t.skips = [];
            var n, r = ("string" == typeof e ? e : "").split(/[\s,]+/),
                o = r.length;
            for (n = 0; n < o; n++) r[n] && (e = r[n].replace(/\*/g, ".*?"), "-" === e[0] ? t.skips.push(new RegExp("^" + e.substr(1) + "$")) : t.names.push(new RegExp("^" + e + "$")));
            for (n = 0; n < t.instances.length; n++) {
                var i = t.instances[n];
                i.enabled = t.enabled(i.namespace)
            }
        }

        function s() {
            t.enable("")
        }

        function c(e) {
            if ("*" === e[e.length - 1]) return !0;
            var n, r;
            for (n = 0, r = t.skips.length; n < r; n++)
                if (t.skips[n].test(e)) return !1;
            for (n = 0, r = t.names.length; n < r; n++)
                if (t.names[n].test(e)) return !0;
            return !1
        }

        function u(e) {
            return e instanceof Error ? e.stack || e.message : e
        }
        t = e.exports = o.debug = o.default = o, t.coerce = u, t.disable = s, t.enable = a, t.enabled = c, t.humanize = n(15), t.instances = [], t.names = [], t.skips = [], t.formatters = {}
    }, function(e, t, n) {
        (function(t) {
            function r() {}

            function o(e) {
                i.call(this, e), this.query = this.query || {}, s || (t.___eio || (t.___eio = []), s = t.___eio), this.index = s.length;
                var n = this;
                s.push(function(e) {
                    n.onData(e)
                }), this.query.j = this.index, t.document && t.addEventListener && t.addEventListener("beforeunload", function() {
                    n.script && (n.script.onerror = r)
                }, !1)
            }
            var i = n(34),
                a = n(9);
            e.exports = o;
            var s, c = /\n/g,
                u = /\\n/g;
            a(o, i), o.prototype.supportsBinary = !1, o.prototype.doClose = function() {
                this.script && (this.script.parentNode.removeChild(this.script), this.script = null), this.form && (this.form.parentNode.removeChild(this.form), this.form = null, this.iframe = null), i.prototype.doClose.call(this)
            }, o.prototype.doPoll = function() {
                var e = this,
                    t = document.createElement("script");
                this.script && (this.script.parentNode.removeChild(this.script), this.script = null), t.async = !0, t.src = this.uri(), t.onerror = function(t) {
                    e.onError("jsonp poll error", t)
                };
                var n = document.getElementsByTagName("script")[0];
                n ? n.parentNode.insertBefore(t, n) : (document.head || document.body).appendChild(t), this.script = t, "undefined" != typeof navigator && /gecko/i.test(navigator.userAgent) && setTimeout(function() {
                    var e = document.createElement("iframe");
                    document.body.appendChild(e), document.body.removeChild(e)
                }, 100)
            }, o.prototype.doWrite = function(e, t) {
                function n() {
                    r(), t()
                }

                function r() {
                    if (o.iframe) try {
                        o.form.removeChild(o.iframe)
                    } catch (e) {
                        o.onError("jsonp polling iframe removal error", e)
                    }
                    try {
                        var e = '<iframe src="javascript:0" name="' + o.iframeId + '">';
                        i = document.createElement(e)
                    } catch (e) {
                        i = document.createElement("iframe"), i.name = o.iframeId, i.src = "javascript:0"
                    }
                    i.id = o.iframeId, o.form.appendChild(i), o.iframe = i
                }
                var o = this;
                if (!this.form) {
                    var i, a = document.createElement("form"),
                        s = document.createElement("textarea"),
                        l = this.iframeId = "eio_iframe_" + this.index;
                    a.className = "socketio", a.style.position = "absolute", a.style.top = "-1000px", a.style.left = "-1000px", a.target = l, a.method = "POST", a.setAttribute("accept-charset", "utf-8"), s.name = "d", a.appendChild(s), document.body.appendChild(a), this.form = a, this.area = s
                }
                this.form.action = this.uri(), r(), e = e.replace(u, "\\\n"), this.area.value = e.replace(c, "\\n");
                try {
                    this.form.submit()
                } catch (e) {}
                this.iframe.attachEvent ? this.iframe.onreadystatechange = function() {
                    "complete" === o.iframe.readyState && n()
                } : this.iframe.onload = n
            }
        }).call(t, n(0))
    }, function(e, t, n) {
        (function(t) {
            function r(e) {
                e && e.forceBase64 && (this.supportsBinary = !1), this.perMessageDeflate = e.perMessageDeflate, this.usingBrowserWebSocket = p && !e.forceNode, this.protocols = e.protocols, this.usingBrowserWebSocket || (f = o), i.call(this, e)
            }
            var o, i = n(18),
                a = n(5),
                s = n(8),
                c = n(9),
                u = n(35),
                l = n(10)("engine.io-client:websocket"),
                p = t.WebSocket || t.MozWebSocket;
            if ("undefined" == typeof window) try {
                o = n(103)
            } catch (e) {}
            var f = p;
            f || "undefined" != typeof window || (f = o), e.exports = r, c(r, i), r.prototype.name = "websocket", r.prototype.supportsBinary = !0, r.prototype.doOpen = function() {
                if (this.check()) {
                    var e = this.uri(),
                        t = this.protocols,
                        n = {
                            agent: this.agent,
                            perMessageDeflate: this.perMessageDeflate
                        };
                    n.pfx = this.pfx, n.key = this.key, n.passphrase = this.passphrase, n.cert = this.cert, n.ca = this.ca, n.ciphers = this.ciphers, n.rejectUnauthorized = this.rejectUnauthorized, this.extraHeaders && (n.headers = this.extraHeaders), this.localAddress && (n.localAddress = this.localAddress);
                    try {
                        this.ws = this.usingBrowserWebSocket ? t ? new f(e, t) : new f(e) : new f(e, t, n)
                    } catch (e) {
                        return this.emit("error", e)
                    }
                    void 0 === this.ws.binaryType && (this.supportsBinary = !1), this.ws.supports && this.ws.supports.binary ? (this.supportsBinary = !0, this.ws.binaryType = "nodebuffer") : this.ws.binaryType = "arraybuffer", this.addEventListeners()
                }
            }, r.prototype.addEventListeners = function() {
                var e = this;
                this.ws.onopen = function() {
                    e.onOpen()
                }, this.ws.onclose = function() {
                    e.onClose()
                }, this.ws.onmessage = function(t) {
                    e.onData(t.data)
                }, this.ws.onerror = function(t) {
                    e.onError("websocket error", t)
                }
            }, r.prototype.write = function(e) {
                function n() {
                    r.emit("flush"), setTimeout(function() {
                        r.writable = !0, r.emit("drain")
                    }, 0)
                }
                var r = this;
                this.writable = !1;
                for (var o = e.length, i = 0, s = o; i < s; i++) ! function(e) {
                    a.encodePacket(e, r.supportsBinary, function(i) {
                        if (!r.usingBrowserWebSocket) {
                            var a = {};
                            e.options && (a.compress = e.options.compress), r.perMessageDeflate && ("string" == typeof i ? t.Buffer.byteLength(i) : i.length) < r.perMessageDeflate.threshold && (a.compress = !1)
                        }
                        try {
                            r.usingBrowserWebSocket ? r.ws.send(i) : r.ws.send(i, a)
                        } catch (e) {
                            l("websocket closed before onclose event")
                        }--o || n()
                    })
                }(e[i])
            }, r.prototype.onClose = function() {
                i.prototype.onClose.call(this)
            }, r.prototype.doClose = function() {
                void 0 !== this.ws && this.ws.close()
            }, r.prototype.uri = function() {
                var e = this.query || {},
                    t = this.secure ? "wss" : "ws",
                    n = "";
                return this.port && ("wss" === t && 443 !== Number(this.port) || "ws" === t && 80 !== Number(this.port)) && (n = ":" + this.port), this.timestampRequests && (e[this.timestampParam] = u()), this.supportsBinary || (e.b64 = 1), e = s.encode(e), e.length && (e = "?" + e), t + "://" + (-1 !== this.hostname.indexOf(":") ? "[" + this.hostname + "]" : this.hostname) + n + this.path + e
            }, r.prototype.check = function() {
                return !(!f || "__initialize" in f && this.name === r.prototype.name)
            }
        }).call(t, n(0))
    }, function(e, t) {}, function(e, t) {
        function n(e, t) {
            var n = [];
            t = t || 0;
            for (var r = t || 0; r < e.length; r++) n[r - t] = e[r];
            return n
        }
        e.exports = n
    }, function(e, t) {
        function n(e) {
            e = e || {}, this.ms = e.min || 100, this.max = e.max || 1e4, this.factor = e.factor || 2, this.jitter = e.jitter > 0 && e.jitter <= 1 ? e.jitter : 0, this.attempts = 0
        }
        e.exports = n, n.prototype.duration = function() {
            var e = this.ms * Math.pow(this.factor, this.attempts++);
            if (this.jitter) {
                var t = Math.random(),
                    n = Math.floor(t * this.jitter * e);
                e = 0 == (1 & Math.floor(10 * t)) ? e - n : e + n
            }
            return 0 | Math.min(e, this.max)
        }, n.prototype.reset = function() {
            this.attempts = 0
        }, n.prototype.setMin = function(e) {
            this.ms = e
        }, n.prototype.setMax = function(e) {
            this.max = e
        }, n.prototype.setJitter = function(e) {
            this.jitter = e
        }
    }, function(e, t, n) {
        "use strict";

        function r(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function o(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function i(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        var a = n(2),
            s = n.n(a),
            c = n(3),
            u = n.n(c),
            l = n(14),
            p = n(107),
            f = (n.n(p), Object.assign || function(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = arguments[t];
                    for (var r in n) Object.prototype.hasOwnProperty.call(n, r) && (e[r] = n[r])
                }
                return e
            }),
            h = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var r = t[n];
                        r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
                    }
                }
                return function(t, n, r) {
                    return n && e(t.prototype, n), r && e(t, r), t
                }
            }(),
            d = function(e) {
                function t() {
                    return r(this, t), o(this, (t.__proto__ || Object.getPrototypeOf(t)).apply(this, arguments))
                }
                return i(t, e), h(t, [{
                    key: "componentWillMount",
                    value: function() {
                        if (this.options = {}, this.props.options) try {
                            this.options = f({}, this.options, JSON.parse(this.props.options))
                        } catch (e) {}
                    }
                }, {
                    key: "render",
                    value: function() {
                        return s.a.createElement(a.Fragment, null, this.props.global ? s.a.createElement("nav", {
                            className: this.props.darkMode ? "navbar navbar-dark bg-dark" : "navbar navbar-light bg-light border-bottom"
                        }, s.a.createElement("div", {
                            className: "container justify-content-start"
                        }, s.a.createElement("div", {
                            className: "col-md-2 col-sm-4 col-6 navbar-text d-flex flex-column"
                        }, s.a.createElement("span", {
                            className: "title"
                        }, "Total Cap."), s.a.createElement("span", {
                            className: "btc-price"
                        }, "$", Object(l.c)(this.props.global.total_market_cap_usd, 2))), s.a.createElement("div", {
                            className: "col-md-2 col-sm-4 col-6 navbar-text d-flex flex-column"
                        }, s.a.createElement("span", {
                            className: "title"
                        }, "Total Volume"), s.a.createElement("span", {
                            className: "btc-price"
                        }, "$", Object(l.c)(this.props.global.total_24h_volume_usd))), s.a.createElement("div", {
                            className: "col-md-2 col-sm-4 col-6 navbar-text d-flex flex-column"
                        }, s.a.createElement("span", {
                            className: "title"
                        }, "Act. Currencies"), s.a.createElement("span", {
                            className: "btc-price"
                        }, Object(l.c)(this.props.global.active_currencies))), s.a.createElement("div", {
                            className: "col-md-2 col-sm-4 col-6 navbar-text d-flex flex-column"
                        }, s.a.createElement("span", {
                            className: "title"
                        }, "Act. Assets"), s.a.createElement("span", {
                            className: "btc-price"
                        }, Object(l.c)(this.props.global.active_assets))), s.a.createElement("div", {
                            className: "col-md-2 col-sm-4 col-6 navbar-text d-flex flex-column"
                        }, s.a.createElement("span", {
                            className: "title"
                        }, "Act. Markets"), s.a.createElement("span", {
                            className: "btc-price"
                        }, Object(l.c)(this.props.global.active_markets))), s.a.createElement("div", {
                            className: "col-md-2 col-sm-4 col-6 navbar-text d-flex flex-column"
                        }, s.a.createElement("span", {
                            className: "title"
                        }, "BTC Market Cap."), s.a.createElement("span", {
                            className: "btc-price"
                        }, this.props.global.bitcoin_percentage_of_market_cap, " %")))) : "Loading ...")
                    }
                }]), t
            }(a.PureComponent);
        d.propTypes = {
            global: u.a.object,
            darkMode: u.a.bool.isRequired,
            options: u.a.string
        }, t.a = d
    }, function(e, t) {}, function(e, t, n) {
        "use strict";

        function r(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function o(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function i(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        var a = n(2),
            s = n.n(a),
            c = n(3),
            u = n.n(c),
            l = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var r = t[n];
                        r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
                    }
                }
                return function(t, n, r) {
                    return n && e(t.prototype, n), r && e(t, r), t
                }
            }(),
            p = function(e) {
                function t() {
                    var e, n, i, a;
                    r(this, t);
                    for (var s = arguments.length, c = Array(s), u = 0; u < s; u++) c[u] = arguments[u];
                    return n = i = o(this, (e = t.__proto__ || Object.getPrototypeOf(t)).call.apply(e, [this].concat(c))), i.state = {
                        showCurrencies: !1
                    }, a = n, o(i, a)
                }
                return i(t, e), l(t, [{
                    key: "toggleDiplayCurrencies",
                    value: function() {
                        this.setState({
                            showCurrencies: !this.state.showCurrencies
                        })
                    }
                }, {
                    key: "render",
                    value: function() {
                        var e = this;
                        return s.a.createElement("form", {
                            className: "row inputs-controlers"
                        }, s.a.createElement("div", {
                            className: "col-12 mb-3"
                        }, s.a.createElement("div", {
                            className: "form-row align-items-center"
                        }, s.a.createElement("div", {
                            className: "col-lg-8 col-md-7 col-sm-12"
                        }, s.a.createElement("label", {
                            className: "sr-only",
                            htmlFor: "searchCurrencies"
                        }, "Search Cryptocoins"), s.a.createElement("input", {
                            type: "search",
                            className: "form-control",
                            id: "searchCurrencies",
                            placeholder: "Search Cryptocoins",
                            onChange: function(t) {
                                return e.props.searchCoins(t)
                            }
                        })), s.a.createElement("div", {
                            className: "col my-1"
                        }, s.a.createElement("button", {
                            type: "button",
                            onClick: function() {
                                return e.toggleDiplayCurrencies()
                            },
                            className: this.state.showCurrencies ? "btn btn-outline-primary btn-block active" : "btn btn-outline-primary btn-block"
                        }, this.state.showCurrencies ? "Hide" : "Change", " Currency")), s.a.createElement("div", {
                            className: "col my-1"
                        }, s.a.createElement("div", {
                            className: "custom-control custom-checkbox mr-sm-2"
                        }, s.a.createElement("input", {
                            type: "checkbox",
                            className: "custom-control-input",
                            id: "darkTheme",
                            checked: this.props.darkMode,
                            onChange: function(t) {
                                return e.props.toggleDarkTheme(t)
                            }
                        }), s.a.createElement("label", {
                            className: "custom-control-label",
                            htmlFor: "darkTheme"
                        }, "Night Mode"))))), this.state.showCurrencies && s.a.createElement("div", {
                            className: "col-12"
                        }, s.a.createElement("div", {
                            className: "row"
                        }, this.props.rates.filter(function(t) {
                            return t !== e.props.currentCurrency
                        }).sort().map(function(t) {
                            return s.a.createElement("div", {
                                className: "col-xl-1 col-md-2 col-sm-3 col-xs-12 mb-1 no-gutters",
                                key: t
                            }, s.a.createElement("button", {
                                type: "button",
                                onClick: function() {
                                    return e.props.changeCurrency(t)
                                },
                                className: "btn btn-block btn-outline-secondary"
                            }, t))
                        }))))
                    }
                }]), t
            }(a.PureComponent);
        p.propTypes = {
            darkMode: u.a.bool.isRequired,
            rates: u.a.object.isRequired,
            currentCurrency: u.a.string,
            searchCoins: u.a.func,
            toggleDarkTheme: u.a.func,
            changeCurrency: u.a.func
        }, t.a = p
    }, function(e, t, n) {
        "use strict";

        function r(e, t) {
            if (!(e instanceof t)) throw new TypeError("Cannot call a class as a function")
        }

        function o(e, t) {
            if (!e) throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
            return !t || "object" != typeof t && "function" != typeof t ? e : t
        }

        function i(e, t) {
            if ("function" != typeof t && null !== t) throw new TypeError("Super expression must either be null or a function, not " + typeof t);
            e.prototype = Object.create(t && t.prototype, {
                constructor: {
                    value: e,
                    enumerable: !1,
                    writable: !0,
                    configurable: !0
                }
            }), t && (Object.setPrototypeOf ? Object.setPrototypeOf(e, t) : e.__proto__ = t)
        }
        var a = n(2),
            s = n.n(a),
            c = n(3),
            u = n.n(c),
            l = n(12),
            p = n(14),
            f = n(110),
            h = n.n(f),
            d = n(27),
            m = n(111),
            y = n.n(m),
            g = Object.assign || function(e) {
                for (var t = 1; t < arguments.length; t++) {
                    var n = arguments[t];
                    for (var r in n) Object.prototype.hasOwnProperty.call(n, r) && (e[r] = n[r])
                }
                return e
            },
            v = function() {
                function e(e, t) {
                    for (var n = 0; n < t.length; n++) {
                        var r = t[n];
                        r.enumerable = r.enumerable || !1, r.configurable = !0, "value" in r && (r.writable = !0), Object.defineProperty(e, r.key, r)
                    }
                }
                return function(t, n, r) {
                    return n && e(t.prototype, n), r && e(t, r), t
                }
            }(),
            b = function(e) {
                function t() {
                    var e, n, i, a;
                    r(this, t);
                    for (var s = arguments.length, c = Array(s), u = 0; u < s; u++) c[u] = arguments[u];
                    return n = i = o(this, (e = t.__proto__ || Object.getPrototypeOf(t)).call.apply(e, [this].concat(c))), i.state = {
                        history: null,
                        historyLoading: !1,
                        showDetailsActive: !1,
                        coin: i.props.coin
                    }, a = n, o(i, a)
                }
                return i(t, e), v(t, [{
                    key: "componentWillUnmount",
                    value: function() {
                        this.timeout && clearTimeout(this.timeout), Object(d.c)(this.state.coin.short)
                    }
                }, {
                    key: "componentDidMount",
                    value: function() {
                        var e = this;
                        Object(d.b)(this.state.coin.short, y()(function(t) {
                            var n = t.message,
                                r = n.coin,
                                o = n.msg;
                            r !== e.state.coin.short || e.state.showDetailsActive || e.setState({
                                coin: g({}, e.state.coin, o, {
                                    class: e.state.coin.price >= 1 * o.price ? "down" : "up",
                                    priceClass: e.state.coin.price >= 1 * o.price ? "text-danger" : "text-success",
                                    vhclass: e.state.coin.vwapData >= o.vwapData ? "text-danger" : "text-success"
                                })
                            }, function() {
                                e.timeout = setTimeout(function() {
                                    e.setState({
                                        coin: g({}, e.state.coin, {
                                            class: null
                                        })
                                    }), clearTimeout(e.timeout)
                                }, 500)
                            })
                        }, 3e3))
                    }
                }, {
                    key: "getCurrentCurrencyValue",
                    value: function(e, t) {
                        return Object(p.b)(e, t, this.props.currentCurrency)
                    }
                }, {
                    key: "showMoreDetails",
                    value: function(e) {
                        var t = this;
                        if ("A" != e.target.tagName) return this.state.history ? void this.setState({
                            showDetailsActive: !this.state.showDetailsActive
                        }) : (this.setState({
                            historyLoading: !0
                        }), Object(l.a)("history/" + this.props.coin.short).then(function(e) {
                            var n = e.data;
                            t.setState({
                                history: n,
                                showDetailsActive: !0,
                                historyLoading: !1
                            })
                        }))
                    }
                }, {
                    key: "createSerieTitle",
                    value: function(e) {
                        return e.replace("_", " ").replace(/\b\w/g, function(e) {
                            return e.toUpperCase()
                        })
                    }
                }, {
                    key: "render",
                    value: function() {
                        var e = this,
                            t = this.state,
                            n = t.historyLoading,
                            r = t.showDetailsActive,
                            o = t.history,
                            i = t.coin,
                            c = this.getCurrentCurrencyValue.bind(this),
                            u = i && o && {
                                credits: {
                                    enabled: !1
                                },
                                legend: {
                                    enabled: !0,
                                    verticalAlign: "top"
                                },
                                rangeSelector: {
                                    allButtonsEnabled: !0,
                                    buttons: [{
                                        type: "day",
                                        count: 1,
                                        text: "1d"
                                    }, {
                                        type: "week",
                                        count: 1,
                                        text: "1w"
                                    }, {
                                        type: "month",
                                        count: 1,
                                        text: "1m"
                                    }, {
                                        type: "month",
                                        count: 3,
                                        text: "3m"
                                    }, {
                                        type: "month",
                                        count: 6,
                                        text: "6m"
                                    }, {
                                        type: "year",
                                        count: 1,
                                        text: "1y"
                                    }],
                                    selected: 2
                                },
                                xAxis: {
                                    minRange: 864e5
                                },
                                yAxis: {
                                    labels: {
                                        formatter: function() {
                                            return c(this.value)
                                        }
                                    }
                                },
                                series: Object.keys(o).map(function(t) {
                                    return {
                                        name: e.createSerieTitle(t),
                                        data: o[t],
                                        type: "area",
                                        tooltip: {
                                            pointFormatter: function() {
                                                return this.series.name + ": " + c(this.y, "price" == t ? 6 : 0)
                                            }
                                        }
                                    }
                                }),
                                time: {
                                    getTimezoneOffset: function(e) {
                                        return new Date(e).getTimezoneOffset()
                                    }
                                }
                            };
                        return s.a.createElement(a.Fragment, null, s.a.createElement("tr", {
                            className: n ? "loading bg-gray " + i.class : i.class,
                            onClick: function(t) {
                                return e.showMoreDetails(t)
                            }
                        }, s.a.createElement("td", null, i.long), s.a.createElement("td", null, this.getCurrentCurrencyValue(i.mktcap, 0)), s.a.createElement("td", {
                            className: i.priceClass
                        }, this.getCurrentCurrencyValue(i.price)), s.a.createElement("td", {
                            className: i.vhclass
                        }, this.getCurrentCurrencyValue(i.vwapData)), s.a.createElement("td", null, Object(p.c)(i.supply)), s.a.createElement("td", null, this.getCurrentCurrencyValue(i.volume, 0)), s.a.createElement("td", {
                            className: i.cap24hrChange < 0 ? "text-danger" : "text-success"
                        }, i.cap24hrChange, " %")), o && r && s.a.createElement("tr", {
                            className: r ? "charts active" : "charts"
                        }, s.a.createElement("td", {
                            colSpan: "8",
                            className: "p-0"
                        }, s.a.createElement(h.a, {
                            config: u
                        }))))
                    }
                }]), t
            }(a.PureComponent);
        b.propTypes = {
            coin: u.a.object.isRequired,
            currentCurrency: u.a.string
        }, t.a = b
    }, function(e, n) {
        e.exports = t
    }, function(e, t, n) {
        function r(e, t, n) {
            var r = !0,
                s = !0;
            if ("function" != typeof e) throw new TypeError(a);
            return i(n) && (r = "leading" in n ? !!n.leading : r, s = "trailing" in n ? !!n.trailing : s), o(e, t, {
                leading: r,
                maxWait: t,
                trailing: s
            })
        }
        var o = n(112),
            i = n(19),
            a = "Expected a function";
        e.exports = r
    }, function(e, t, n) {
        function r(e, t, n) {
            function r(t) {
                var n = v,
                    r = b;
                return v = b = void 0, E = t, C = e.apply(r, n)
            }

            function l(e) {
                return E = e, x = setTimeout(h, t), A ? r(e) : C
            }

            function p(e) {
                var n = e - k,
                    r = e - E,
                    o = t - n;
                return T ? u(o, w - r) : o
            }

            function f(e) {
                var n = e - k,
                    r = e - E;
                return void 0 === k || n >= t || n < 0 || T && r >= w
            }

            function h() {
                var e = i();
                if (f(e)) return d(e);
                x = setTimeout(h, p(e))
            }

            function d(e) {
                return x = void 0, O && v ? r(e) : (v = b = void 0, C)
            }

            function m() {
                void 0 !== x && clearTimeout(x), E = 0, v = k = b = x = void 0
            }

            function y() {
                return void 0 === x ? C : d(i())
            }

            function g() {
                var e = i(),
                    n = f(e);
                if (v = arguments, b = this, k = e, n) {
                    if (void 0 === x) return l(k);
                    if (T) return x = setTimeout(h, t), r(k)
                }
                return void 0 === x && (x = setTimeout(h, t)), C
            }
            var v, b, w, C, x, k, E = 0,
                A = !1,
                T = !1,
                O = !0;
            if ("function" != typeof e) throw new TypeError(s);
            return t = a(t) || 0, o(n) && (A = !!n.leading, T = "maxWait" in n, w = T ? c(a(n.maxWait) || 0, t) : w, O = "trailing" in n ? !!n.trailing : O), g.cancel = m, g.flush = y, g
        }
        var o = n(19),
            i = n(113),
            a = n(115),
            s = "Expected a function",
            c = Math.max,
            u = Math.min;
        e.exports = r
    }, function(e, t, n) {
        var r = n(40),
            o = function() {
                return r.Date.now()
            };
        e.exports = o
    }, function(e, t, n) {
        (function(t) {
            var n = "object" == typeof t && t && t.Object === Object && t;
            e.exports = n
        }).call(t, n(0))
    }, function(e, t, n) {
        function r(e) {
            if ("number" == typeof e) return e;
            if (i(e)) return a;
            if (o(e)) {
                var t = "function" == typeof e.valueOf ? e.valueOf() : e;
                e = o(t) ? t + "" : t
            }
            if ("string" != typeof e) return 0 === e ? e : +e;
            e = e.replace(s, "");
            var n = u.test(e);
            return n || l.test(e) ? p(e.slice(2), n ? 2 : 8) : c.test(e) ? a : +e
        }
        var o = n(19),
            i = n(116),
            a = NaN,
            s = /^\s+|\s+$/g,
            c = /^[-+]0x[0-9a-f]+$/i,
            u = /^0b[01]+$/i,
            l = /^0o[0-7]+$/i,
            p = parseInt;
        e.exports = r
    }, function(e, t, n) {
        function r(e) {
            return "symbol" == typeof e || i(e) && o(e) == a
        }
        var o = n(117),
            i = n(120),
            a = "[object Symbol]";
        e.exports = r
    }, function(e, t, n) {
        function r(e) {
            return null == e ? void 0 === e ? c : s : u && u in Object(e) ? i(e) : a(e)
        }
        var o = n(41),
            i = n(118),
            a = n(119),
            s = "[object Null]",
            c = "[object Undefined]",
            u = o ? o.toStringTag : void 0;
        e.exports = r
    }, function(e, t, n) {
        function r(e) {
            var t = a.call(e, c),
                n = e[c];
            try {
                e[c] = void 0;
                var r = !0
            } catch (e) {}
            var o = s.call(e);
            return r && (t ? e[c] = n : delete e[c]), o
        }
        var o = n(41),
            i = Object.prototype,
            a = i.hasOwnProperty,
            s = i.toString,
            c = o ? o.toStringTag : void 0;
        e.exports = r
    }, function(e, t) {
        function n(e) {
            return o.call(e)
        }
        var r = Object.prototype,
            o = r.toString;
        e.exports = n
    }, function(e, t) {
        function n(e) {
            return null != e && "object" == typeof e
        }
        e.exports = n
    }, function(e, t) {}, function(e, t) {
        function n() {
            return "serviceWorker" in navigator && (window.fetch || "imageRendering" in document.documentElement.style) && ("https:" === window.location.protocol || "localhost" === window.location.hostname || 0 === window.location.hostname.indexOf("127."))
        }

        function r(e) {
            if (e || (e = {}), n()) navigator.serviceWorker.register("sw.js");
            else if (window.applicationCache) {
                var t = function() {
                    var e = document.createElement("iframe");
                    e.src = "appcache/manifest.html", e.style.display = "none", a = e, document.body.appendChild(e)
                };
                return void("complete" === document.readyState ? setTimeout(t) : window.addEventListener("load", t))
            }
        }

        function o(e, t) {}

        function i() {
            if (n() && navigator.serviceWorker.getRegistration().then(function(e) {
                if (e) return e.update()
            }), a) try {
                a.contentWindow.applicationCache.update()
            } catch (e) {}
        }
        var a;
        t.install = r, t.applyUpdate = o, t.update = i
    }])
});
//# sourceMappingURL=crypto.bundle.js.map
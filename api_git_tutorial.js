const GDAX = require("gdax");
const publicClient = new GDAX.PublicClient();
const callback = (error, response, data) => {
    if (error)
        return console.dir(error);
    return console.dir(data);
}
//publicClient.getProducts(callback);

//publicClient.getCurrencies(callback);
 var a = "Mike"
 var b = "Mike2"

const passPhrase = "nicho412";
const apiKey = "3ca587b18c0164f3ddb50f7ffde5f18d";
const base64secret = "PnRtOBmcmOwAaL9z5iEMz8t4yUwNYjSD5M4Rx3xZV0VyPJHHAiQ5RxAwFauh5KuPdB7p2OAwOSZxns+2GFGCkg==";

/*use sandBox API to figure out this problem "Invalid API "*/
const apiURI = "https://api.gdax.com";

const sandboxURI = 'https://api-public.sandbox.gdax.com';
const authenticatedClient = new GDAX.AuthenticatedClient(apiKey, base64secret, passPhrase, sandboxURI);

//Lets see what our accounts actually are:

authenticatedClient.getAccounts(callback);
/*
* Once you know all your account’s IDs you can query each account individually, for more precise controll:
const ACCOUNT_ETH ="some-super-long-account-id";
authenticatedClient.getAccount(ACCOUNT_ETH, callback);
*/
const ACCOUNT_ETH ="22864723-9ddf-46c6-98fd-6ad4684d0919";
authenticatedClient.getAccount(ACCOUNT_ETH, callback);

/*
*Lets pick one product, Etherium as an example:
*
const ETH_USD = ‘ETH-USD’;
const websocket = new GDAX.WebsocketClient([ETH_USD]);
const websocketCallback = (data) => console.dir(data);
websocket.on('message', websocketCallback);
 */
const ETH_USD = "ETH-USD";
const websocket = new GDAX.WebsocketClient([ETH_USD]);
const websocketCallback = (data) => console.dir(data);
websocket.on('message', websocketCallback);

/*
*Modified callback to track current prices:
const websocketCallback = (data) => {
if (!(data.type === ‘done’ && data.reason === ‘filled’))
return;
console.dir(data);
 }
 */

const websocketCallback = (data) => {
    if (!(data.type === "done" && data.reason === "filled"))
    return;
    console.dir(data);
}


/*
*Okay. Lets say you have first version of your algorithm finalized, how do we actually trade?

const websocketCallback = (data) => {
if (!(data.type === 'done' && data.reason === 'filled'))
return;
const analytics = someBrilliantAnalyticalMethod(data);
if (analytics.buy)
return placeBuyOrder(analytics.buyDetails)
if (analytics.sell)
return placeSellOrder(analytics.sellDetails)
 }
 */

/*
*How do we place buy and sell orders?
*Very simple, back to our authenticatedClient:
*
function placeBuyOrder(buyDetails) {
    const buyParams = {
    ‘price’: buyDetails.price,
    ‘size’: buyDetails.size,
    ‘product_id’: ETH_USD,
    };
    this.buyOrderId = authenticatedClient.buy(buyParams, callback);
}
function placeSellOrder(buyDetails) {
    const sellParams = {
    ‘price’: buyDetails.price,
    ‘size’: buyDetails.size,
    ‘product_id’: ETH_USD,
    };
    this.sellOrderId = authenticatedClient.sell(sellParams,  callback);
}
 */

/*
 *To check all placed orders:
 *
authenticatedClient.getOrders(callback);
*/

/*
 *To check status of an individual order:
 *
authenticatedClient.getOrder(orderID, callback);
 */